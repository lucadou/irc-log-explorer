# Changelog
All notable changes to this project will be documented in this file.

## [0.8.9] - 2021-??-??
### Added

In Progress:
- Ban appeals system (#13) - note to self: this should check to see if the time the ban has left is > the minimum time for appeal rather than check to see if the original ban length was > minimum time
- Multi-domain support via configuration file (#70)
- Reading more settings from a configuration file (#7)
- Tests for chat log permissions

Not Started:
- Ability to disable Oauth entirely via config file (#7)
- Ability to disable specific Oauth providers via config file (#7)
- Admin console application settings panel (#13)
- Compact tables user preference (#58)
- Display most recent edit date and justification for chat logs
- Tests for changing password on account security page
- Tests for chat log editing & deletion
- Tests for items unable to use integration tests in issue #31
- Tests for TOTP and U2F based 2FA
- Tests for U2F based 2FA


### Changed

In Progress:
- Centralized some validations (#73)

Not Started:
- Improved integer handling across all forms (#67)
- Moved all views to use new error messages view
- SSO users now use the same validations as tradtional users (#44)

## [0.8.8] - 2020-12-31
### Added
- Ability to enable image embeds and their max width in config file
- Ability to update the TOS via admin console (#87, formerly #13)
- Basic messaging system (#81)
- Contact form for admins (#13)
- Default flash classes (#69)
- Display users a person has banned on their profile (#66)
- Favicons
- Help page for supported Markdown
- HTML theme-color attribute based on user theme (#56)
- Markdown for messaging system
- Three new themes - Minty, Midnight, and Whiteout
- Tests for the new themes

### Changed
- Added Nginx 504 error diagnostics to readme
- Banning a user now generates a Message to them (#90)
- Case sensitive field now defaults to false instead of nil (#64)
- Centered footer
- Centralized escaped regex handling (#71)
- Centralized per page limitations and checking (#72 and #75)
- Explicitly declared Puma as webserver (#32)
- Fixed the file extensions of some views
- Fixed unescaped forward slashes causing string truncation (#74)
- Fixed incorrect parameter re: showing seconds in calls to datetime string converter method
- Hid edit/delete chat log button for non-admin users
- Improved message editing validation (#6)
- Increased consistency of centering (#61)
- Increased ease of deploying to Heroku (#32)
- Logged-in users can no longer access TOTP and U2F login paths (#78)
- Logged-out users can no longer access TOTP and U2F login paths (#78)
- Logged-out users can no longer access manage TOTP with U2F path (#78)
- Readme now suggests `rake db:schema:load` instead of `rake db:migrate` for setup
- Redid theme handler
- Remove ban properties from User model (#90)
- Set GitLab CI to manual
- SSL certificate domains
- Switched from using strings to symbols to access application configuration hash (#86)
- Upgraded to Rails 6.0
- Upgraded to Ruby 2.7

## [0.8.7] - 2019-05-14
### Added
- Bans system
- Easy way to promote to admin or demote to user
- Easy way to disable PNG QR code generation via config file
- Error message when user executes blank basic search
- GitLab CI for tests
- Instructions for deploying with Nginx and auto-starting Puma on boot
- Logging all searches
- Logging report and search browsing
- Notes about deploying to Heroku in readme
- Password change field in security settings
- Permissions for reports views
- Removing TOTP can be done with U2F tokens
- Removing U2F tokens requires 2FA
- Starting work on admin console
- Search on reports page
- Tests for admin report searches
- Tests for chat log basic & advanced searches
- Tests for report creation
- Tests for report deletion
- Tests for report editing
- Tests for report viewing
- Tests for TOTP based 2FA
- Tests for recovery codes
- Tests for user report searches
- Title for chat logs index
- Title for EULA view
- User management admin panel

### Changed
- Cleaning test DB before each test run
- Fixed broken datetime picker
- Fixed issue with 2FA allowing TOS acceptance bypass
- Fixed issue with flashes not clearing until second refresh
- Fixed issue with invalid per page values being used sometimes
- Fixed issue with TOTP code not copying to clipboard until refresh
- Fixed issue with TOTP QR code being generated with invalid format
- Fixed issue with rake db commands failing due to factories in tests
- Fixed issue with some themes having tough to read text in tables
- Fixed issues with model associations & field names preventing user deletion
- Fixed mobile formatting for chat logs & reports
- Fixed spacing issues for advanced search page
- Increased spacing consistency for many views
- Made navbar be fixed to top
- Moved Devise error messages into their own view
- Revised phrasing in account overview tab
- Removed Delete buttons from Admin reports view
- Removed Show/Resolve buttons from User reports view
- Removed unnecessary borders from Settings views
- Updated Boostrap, datetime picker, webauth, and other Gems
- Validation for preferences moved from controller to concerns

## [0.8.6] - 2019-01-25
### Added
- Decimal restriction for editing chat log time
- Edit page for reports (for editing the message to redact information)
- JS console message
- Lots of reports to seed data (so it's easier to manually test report
  pagination)
- No results row for empty ChatLogs and Reports
- Pagination for reports
- Seeds file specifically for tests
- Tests for account creation
- Tests for account login without 2FA
- Tests for traditional account settings (name, email, password) changes and
  account deletion
- Tests for accessing chat logs, settings, and reports without being logged in
- Tests for alias routes (/login, /signup, etc.)
- Tests for terms of service
- Tests for user appearance preferences
- Tests for user localization preferences
- Tests for user notification preferences
- Users can no longer add the same U2F token multiple times

### Changed
- Centralized local datetime conversion
- Cleaned up routes
- Fixed bug where default sort column wasn't changed for reports view, causing
  invalid column name errors
- Fixed bug where Firefox would not display SSO icons
- Fixed bug where offset of "00:00" was flagged as invalid in offset MS
  calculation
- Fixed bug where users can sign in with Twitter and not accept TOS until next
  login
- Fixed bug where users could insert decimals into search results per page and
  it would silently cast to int instead of erroring
- Fixed bug where User model reported incorrect values for U2F being enabled
- Fixed spacing inconsistencies for chat logs
- Improved layout of chat logs index and applied Bootstrap cards
- Moved Credentials into Devise
- Moved validation errors into I18n translation file
- Redid root/index page
- Renamed icons to avoid uBlock targetting

## [0.8.5] - 2019-01-18
### Added
- Basic message reporting system
- Detailed Show page for reports
- Resolved reports cannot be deleted
- Resolving for reports
- Sorting for message reports view
- WebAuthn based U2F 2FA

### Changed
- Centralizing some helper methods for ChatLogs and Reports
- Ruby version can now be 2.5.x, not just 2.5.1
- U2F tokens now work with browsers besides Chrome on desktop

### Removed
- Traditional U2F based 2FA (now using WebAuthn, which works with more than just Chrome)

## [0.8.4] - 2019-01-14
### Changed
- Settings tab highlights all appropriate pages
- User registration tab highlights additional pages
- Warning U2F users that U2F currently only works on Chrome

## [0.8.3] - 2019-01-13
### Added
- Dummy SSL certificates for running a local HTTPS server for U2F
- QR code generation for TOTP codes
- U2F based 2FA

### Changed
- Centralized flash discarding
- Fixed bug where users could login with incorrect password if TOTP was enabled
- Moved local datetime conversion into ApplicationsController
- Standardized spacing before footer

## [0.8.2] - 2019-01-12
### Added
- TOTP based 2FA and recovery codes

### Changed
- Moved away from the Ruby SASS gem (deprecated)
- Updated Bootstrap and Capybara gems

## [0.8.1] - 2019-01-07
### Added
- Notification settings menu
- Reports page mockup
- Security settings menu
- SSO users must accept EULA when creating account and when EULA is updated
- Traditional account users must accept EULA when creating account and when
  EULA is updated

### Changed
- Fixed being unable to remove backup email on SSO-only accounts
- Fixed display preference radio buttons not being selectable by their labels
- Fixed SSO icon alignment
- Updated Gemfile and README for easier installation

## [0.8.0] - 2019-01-02
### Added
- Account overview menu mockup
- Appearance/localization menu
- Chat log views now use 24 hour time setting
- Chat log views now use timezone offset
- Chat log index and searches now use default column and sort direction
- Chat log index and searches now use default search results per page
- New themes: Dark, Solarized, and Ubuntu
- Results per page field on advanced search page
- Show theme depending on user's preference
- Tabbed navigation bar for settings menu
- User preferences saving and loading

### Changed
- Changed account-related routes from /users to /account
- Changed copyright year
- Fixed bug where providing a starting ID but not an ending ID
  incorrectly flashed an error message to the user
- Fixed bug where updating account information deleted user account due to
  misplaced form declaration & end statements
- Fixed bug with Twitter SSO not being properly stored
- Moved to using shared file for account pages headers
- Updated old settings page to have tab bar
- Updated README.md to reflect new Oauth callback URIs

## [0.7.3] - 2018-12-28
### Added
- Simple terms of service form
- Search by starting and ending message ID
- Warning message when ending ID is greater than starting ID

### Changed
- Removed some obsolete code

## [0.7.2] - 2018-12-28
### Added
- Email grabber page for SSO providers without email (e.g. Twitter) so
  it doesn't pull up the new registration page & force the user to enter
  a password

### Changed
- Added auto-highlight to logs index search box
- Allow regular accounts to add SSO
- Attempting to add the same Oauth to 2 accounts generates an error
  instead of failing silently
- Added notification when user successfully adds a new SSO integration
- Fixed regular accounts claiming to successfully add SSO but failing
- Fixed not verifying "new password" & "confirm new password" fields matched
- Forgotten password screen improved with bootstrap cards
- SSO-only accounts no longer need to enter the (pre-generated) password
  when setting up a password for the first time
- Using SSO no longer auto-pairs to existing accounts

## [0.7.1] - 2018-12-26
### Added
- SSO panel that displays sign-in or add/remove links depending on if
  the user is signed in or not
- Ability to remove SSO integrations
- Error message when user searches for invalid regex

### Changed
- Account settings page redone with Bootstrap cards
- Advanced search page redone with Bootstrap cards
- Message editing page redone with Bootstrap cards
- Registration page redone
- Upgraded to Rails 5.2.2

## [0.7.0] - 2018-12-24
### Added
- Oauth - Discord, Facebook, GitHub, GitLab, Google, Twitch, Twitter

### Changed
- Readme has Oauth callback URIs
- Use secrets.yml for API key storage instead of api_keys.yml
- Login page revamped
- Changed login URI to /signin
- Changed logout URI to /signout
- Changed register URI to /signup

## [0.6.2] - 2018-12-17
### Changed
- Revamped readme to actually be helpful

## [0.6.1] - 2018-11-03
### Added
- Backup email
- Banned until date
- Perma banned status
- Account creation Unix timestamp
- Second navigation bar which works without JavaScript

### Changed
- Require login to access logs
- Message flash when trying to do something that requires authentication

## [0.6.0] - 2018-11-01
### Added
- Login system using Devise gem

### Changed
- Advanced search and edit form improvements (labels now select their
  their associated input box)

## [0.5.0] - 2018-10-30
### Added
- Advanced search functionality

## [0.4.0] - 2018-09-29
### Added
- Basic case-insensitive search functionality

## [0.3.1] - 2018-09-29
### Added
- Validation when editing fields

## [0.3.0] - 2018-09-29
### Added
- Ability to view a message in context
- Added flash alerts when viewing messages in context

### Changed
- Removed some unused code from the removed will_paginate module
- Bootstrap styling for kaminari pagination

## [0.2.2] - 2018-09-28
### Added
- Ability to specify a message to highlight in the table

### Changed
- Now using kaminari instead of will_paginate for pagination

## [0.2.1] - 2018-09-24
### Added
- Sorting by channel, sender, and (the default) time (shoutout to
  [this site](https://richonrails.com/articles/sortable-table-columns) for showing me a much better implementation than what I
  learned in my Ruby class!)
- Font-Awesome icons
- Pagination

### Changed
- Search bar and buttons now indicate search is a work in progress
- Ensure JQuery will load first to prevent breaking other scripts

## [0.2.0] - 2018-09-24
### Added
- Applied Bootstrap to all pages and alerts

### Changed
- Disabled creation of new logs - all logs must be inputted by a log bot
- View message page now shows the timestamp in a human readable format

## [0.1.3] - 2018-09-24
### Added
- Bootstrap & JQuery

## [0.1.2] - 2018-09-23
### Added
- Static index page for when I setup authentication

## [0.1.1] - 2018-09-22
### Added
- License

## [0.1.0] - 2018-09-22
### Added
- Changelog
- Seed data
- Timestamp converter
