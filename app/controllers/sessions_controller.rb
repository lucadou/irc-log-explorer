require 'uri'

class SessionsController < Devise::SessionsController
  after_action :clear_flashes, except: [:create, :destroy, :two_factor_auth_totp]
  before_action :set_domain, only: [:setup_omniauth]
  prepend_before_action :authenticate_with_two_factor, if: :two_factor_enabled?, only: [:create]
  prepend_before_action :partially_authenticate_user!, only: [:two_factor_auth_totp, :two_factor_auth_webauthn]

  def create
    # Override create method to force EULA acceptance for existing users
    # signing in to traditional accounts.
    super do
      session[:user_id] = @user.id # For RegistrationsController.accept_tos
      if @user.eula_acceptance_needed?
        # Eventually this will need to be redone with checks against the EULA
        # updated date to determine if the user needs to reaccept the EULA.
        sign_out(resource)
        redirect_to policy_tos_path
        flash[:notice] = "You must accept the Terms of Service before continuing."
        return # return statement exits the superclass's create method to
        # prevent the respond_with statement from executing. This prevents
        # double redirect errors when respond_with calls
        # after_sign_in_path_for(resource).
        # after_sign_in_path_for is in "lib/devise/controllers/helpers.rb",
        # so I'm not totally sure how to override it.
        # If anyone wants to do a proper method override, please put in an MR.
      end
    end
  end

  def destroy
    # This is here so you can sign out without having a form button for users
    # that have JavaScript disabled.
    super
  end

  def two_factor_auth_totp
    user = User.find_by_id(session[:user_id])
    @UserHasU2F = user&.credentials.count > 0
    # I use the safe navigation operator so @UserHasU2F is nil if user is
    # nil, without having to check if user is nil to prevent exceptions.
    # I had to move this out of "if request.get?" because on an incorrect
    # code attempt, the @UserHasU2F variable would not be properly assigned,
    # so the U2F button would not appear until they refreshed the page.
    # This work around that problem, at the cost of an unnecessary operation
    # when the request is a post instead of a get.
    if request.post?
      user = find_user
      if params[:user] && params[:user][:otp_attempt] && valid_otp_attempt?(user)
        flash[:success] = "Signed in with One-Time Password. If you used a recovery code, make sure to generate new codes so you don't run out!"
        if user.eula_acceptance_needed?
          # Ensure users have to accept the TOS
          # Fix for issue #76
          redirect_to policy_tos_path
          flash[:notice] = "You must accept the Terms of Service before continuing."
        else
          sign_in(user)
          redirect_to root_path
        end
      elsif !params[:user]
        logger.warn "User with ID #{session[:user_id]} appears to have tampered with the 2FA TOTP form; submitted with no params[:user]."
      else
        flash[:error] = "Invalid 2FA or recovery code, please try again."
        logger.info "User with ID #{user.id} attempted to sign in with invalid TOTP or recovery code: #{params[:user][:otp_attempt]}."
      end
    end
  end

  def two_factor_auth_webauthn
    user = User.find_by_id(session[:user_id])
    @UserHasTOTP = user&.otp_required_for_login
    @UserHasRecoveryCodes = user.otp_backup_codes.length > 0 if user.otp_backup_codes
    # The reason these variables aren't in "if request.get?" is the same
    # as for the two_factor_auth_totp method above.
    if request.post?
      if params[:id] && params[:response]
        # These params are present in responses from the token
        auth_response = WebAuthn::AuthenticatorAssertionResponse.new(
          credential_id: str_to_bin(params[:id]),
          client_data_json: str_to_bin(params[:response][:clientDataJSON]),
          authenticator_data: str_to_bin(params[:response][:authenticatorData]),
          signature: str_to_bin(params[:response][:signature])
        )

        user = User.find_by_id(session[:user_id])
        raise "User with ID #{session[:user_id]} never initiated sign up" unless user

        allowed_credentials = user.credentials.map do |cred|
          {
            id: str_to_bin(cred.external_id),
            public_key: str_to_bin(cred.public_key)
          }
        end

        if !auth_response.valid?(str_to_bin(user.current_challenge), request.base_url, allowed_credentials: allowed_credentials)
          flash[:error] = "Unrecognized token, did you insert an unregistered device?"
          render json: { status: "forbidden" }, status: :forbidden unless auth_response.valid?(
            str_to_bin(user.current_challenge),
            request.base_url,
            allowed_credentials: allowed_credentials
          )
        end

        sign_in(user)
        flash[:success] = "Signed in with U2F token."
        render json: { status: "ok" }, status: :ok
        # No redirect statement needed, that occurs in
        # assets/javascripts/credentials_helper.js
      else
        user = User.find_by_id(session[:user_id])

        if user
          credential_options = WebAuthn.credential_request_options
          credential_options[:allowCredentials] = user.credentials.map do |cred|
            { id: cred.external_id, type: "public-key" }
          end

          credential_options[:challenge] = bin_to_str(credential_options[:challenge])
          user.update!(current_challenge: credential_options[:challenge])

          respond_to do |format|
            format.json { render json: credential_options }
          end
        else
          respond_to do |format|
            format.json { render json: { errors: ["Username doesn't exist"] }, status: :unprocessable_entity }
          end
        end
      end
    end
  end

  def setup_omniauth
    provider = UserOauth.normalize_oauth_name(params[:provider]).downcase
    request.env['omniauth.strategy'].options[:client_id] = Rails.application.secrets[@domain]["#{provider}_app_id".to_sym]
    request.env['omniauth.strategy'].options[:client_secret] = Rails.application.secrets[@domain]["#{provider}_api_secret".to_sym]
    render :plain => "Omniauth setup phase.", :status => 404
  end

  private

  def two_factor_enabled?
    is_2fa_required(find_user)
  end

  def valid_otp_attempt?(user)
    user.validate_and_consume_otp!(params[:user][:otp_attempt]) ||
      user.invalidate_otp_backup_code!(params[:user][:otp_attempt])
  end

  def find_user
    if params[:user] && params[:user][:email]
      User.where(:email => params[:user][:email]).first
    elsif !params[:user]
      flash[:error] = "Invalid form"
      logger.warn "User with ID #{session[:user_id]} appears to have tampered with login or 2FA form; submitted with no params[:user]."
    elsif session[:user_id]
      User.find_by_id(session[:user_id])
    end
  end

  def authenticate_with_two_factor
    user = find_user
    session[:user_id] = user.id
    return unless user && is_2fa_required(user)

    if user && user.valid_password?(params[:user][:password])
      # Since this method is prepended before SessionsController.create,
      # this if statement needed to ensure someone doesn't enter an invalid
      # password and get to a 2FA prompt.
      redirect_to default_2fa_path(user)
    end
    # No else statement needed - it will return nil & the user will get an
    # invalid email or password message and they will be redirected to the
    # login page.
  end
end
