require 'time'
require 'uri'

class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  after_action :clear_flashes, only: []
  before_action :set_domain

  def all
    user = User.from_omniauth(request.env["omniauth.auth"], @domain)

    if user.class == Hash && user[:signed_in] && !user[:can_add]
      # User is logged in and is attempting to add a new Oauth provider.
      # However, the account they signed into with the provider is already
      # in use by someone else, so it will not be added.
      flash[:error] = I18n.t('.activerecord.errors.models.user_oauth.attributes.add.denied_provider_used_other_user', provider: user[:provider])
      user = user[:current_user]
      redirect_to edit_user_registration_path
    elsif (user.class == Hash && user[:signed_in] && !user[:can_add] &&
            user[:present])
      # User is logged in and is attempting to add a new Oauth provider.
      # However, they have already added that provider, so it will not be
      # added.
      flash[:error] = I18n.t('.activerecord.errors.models.user_oauth.attributes.add.denied_provider_used_current_user', provider: user[:provider])
      user = user[:current_user]
      redirect_to edit_user_registration_path
    elsif user.class == Hash && user[:signed_in] && user[:can_add]
      # User is logged in and is attempting to add a new Oauth provider.
      # The account they signed into with the provider is not already
      # in use by someone else, so it was successfully added.
      flash[:success] = I18n.t('.activerecord.errors.models.user_oauth.attributes.add.success_logged_in', provider: user[:provider])
      user = user[:current_user]
      redirect_to edit_user_registration_path
    elsif user.class == Hash && !user[:signed_in]
      # User is not logged in and is attempting to log in with Oauth.
      # The account signed into with the Oauth provider has not been
      # registered to an account on this site.
      # However, the email matches that of an existing user on this site,
      # so they cannot log in via this method until they bind this Oauth
      # integration via the Settings menu.
      flash[:error] = I18n.t('.activerecord.errors.models.user_oauth.attributes.add.denied_email_used', provider: user[:provider])
      redirect_to sign_in_path
    elsif (!user.nil? && user.persisted?)
      session[:user_id] = user.id

      initial_signin = user.created_at.to_i * 1000
      # *1000 converts to milliseconds

      current_time = Time.now.to_i * 1000
      if current_time - initial_signin < 60000
        # 60000 = 60 * 1000 = milliseconds in a minute

        # This fires if the user registered with SSO within the past minute.
        # If the user registerd within the last minute, this is likely a new
        # account, so the message below is displayed:
        flash[:info] = I18n.t('.activerecord.errors.models.user_oauth.attributes.add.success_new_account')
      end
      # Credit to https://stackoverflow.com/a/26937525 for teaching me about
      # __callee__, which provides the alias the method was called with.
      if __callee__ == :twitter && !user.email.index("@twitter.example").nil?
        # Twitter does not provide the email address unless you have elevated
        # permissions, so I put in this check just in case someone uses this
        # who has elevated permissions.
        flash[:notice] = I18n.t('.activerecord.errors.models.user_oauth.attributes.email.twitter')
        redirect_to finish_sso_path(:callback => __callee__)
      elsif __callee__ == :facebook && !user.email.index("@facebook.example").nil?
        # Facebook makes providing the email address optional, so if the user
        # opts not to provide an email address, an email is auto-added into
        # the database in the format "temp-email-fb-[uid]@facebook.example".
        # String.index returns nil if not found, so !.nil is true when it is
        # found, indicating they opted not to share the email address.
        flash[:notice] = I18n.t('.activerecord.errors.models.user_oauth.attributes.email.facebook')
        redirect_to finish_sso_path(:callback => __callee__)
      else
        # Fires every time a user signs in via Oauth (to an existing account
        # or to a new account)
        if user.eula_acceptance_needed?
          redirect_to policy_tos_path
        elsif is_2fa_required(user)
          redirect_to default_2fa_path(user)
        else
          sign_in_and_redirect user, notice: "Signed in!"
        end
      end
    else
      # If User.from_omniauth returns nil, this causes problems.
      # However, I'm not going to rescue NoMethodErrors because on the rare
      # occasion this happens, I want to know about it. So, I'll just make a
      # logger entry.
      if user.nil?
        logger.fatal "User.from_omniauth managed to return nil! This will cause a NoMethodError."
      end
      # Devise allow us to save the attributes even though 
      # we havent create the user account yet
      session["devise.user_attributes"] = user.attributes

      flash[:info] = I18n.t('.activerecord.errors.models.user_oauth.attributes.add.success_new_account')
      redirect_to new_user_registration_url(:callback => __callee__.to_s)
    end
  end

  def destroy
    # This method removes a given SSO integration from a signed-in user's
    # account
    provider = params[:provider]
    provider_found = User.omniauth_providers.include?(provider.to_sym)
    user = Current.user
    user_oauth = user.public_send("#{provider}_oauth") if user.respond_to? ("#{provider}_oauth")
    # Only call _oauth if it has a method that will respond to it (otherwise it is nil)
    provider = UserOauth.normalize_oauth_name(provider)
    integration_enabled = user_oauth.present?
    # Will be false if nil was returned by the _oauth method

    # Flash for removal success/failure
    if provider_found && integration_enabled
      begin
        user.save!
          # .save silently fails, while .save! raises RecordInvalid on failure
        user_oauth.destroy_all
        flash[:success] = I18n.t('.activerecord.errors.models.user_oauth.attributes.remove.success', provider: provider)
      rescue ActiveRecord::RecordInvalid => invalid
        logger.error "Error removing #{provider} from user #{user.id}: #{invalid.record.errors}"
        flash[:error] = I18n.t('.activerecord.errors.models.user_oauth.attributes.remove.fail', provider: provider)
      end
    elsif provider_found
      # User tried to remove an integration they never added
      flash[:error] = I18n.t('.activerecord.errors.models.user_oauth.attributes.remove.fail_integration_na', provider: provider)
    elsif !provider_found
      # User tried to remove an integration that does not exist
      flash[:error] = I18n.t('.activerecord.errors.models.user_oauth.attributes.remove.fail_provider_na', provider: provider)
    end

    # Check for user having any SSO provider enabled
    has_any_sso = user.user_oauths.count > 0
    if !has_any_sso # Warn them if no SSO providers are enabled on their account
      flash[:notice] = I18n.t('.activerecord.errors.models.user_oauth.attributes.remove.warn_no_sso')
    end

    redirect_to edit_user_registration_path # Force refresh to update page
  end

  # Add new providers here
  alias_method :discord, :all
  alias_method :facebook, :all
  alias_method :github, :all
  alias_method :gitlab, :all
  alias_method :google_oauth2, :all
  alias_method :twitter, :all
  alias_method :twitch, :all
end
