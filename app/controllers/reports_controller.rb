require 'date'

class ReportsController < ApplicationController
  include ReportsHelper
  after_action :clear_flashes, except: [:new, :create, :delete, :destroy, :edit, :resolve]
  before_action :set_report, only: [:show, :edit, :update, :resolve, :destroy]
  before_action :authenticate_user!
  before_action :verify_permissions
  before_action :verify_ownership, only: [:edit, :update, :delete, :destroy]
  helper_method :is_admin, :is_admin_view

  # GET /reports
  # GET /reports.json
  def index
    @prefs = ApplicationHelper.common_prefs

    # Extract params
    report_type = params[:resolved] if params[:resolved] && params[:resolved].length > 0
    report_reason_query = params[:reason] if params[:reason] && params[:reason].length > 0
    report_msg_query = params[:advanced_query] if params[:advanced_query] && params[:advanced_query].length > 0
    report_msg_id = params[:msg_id] if params[:msg_id] && params[:msg_id].length > 0
    report_msg_channel = params[:channel] if params[:channel] && params[:channel].length > 0
    report_msg_sender = params[:sender] if params[:sender] && params[:sender].length > 0
    report_start_time = params[:start_time] if params[:start_time] && params[:start_time].length > 0
    report_end_time = params[:end_time] if params[:end_time] && params[:end_time].length > 0
    report_per_page = params[:per_page] if params[:per_page] && params[:per_page].length > 0

    if report_type == 'all'
      report_status = nil
    elsif report_type == 'resolved'
      report_status = true
    elsif report_type == 'unresolved'
      report_status = false
    end

    @search_errors = Report.validate_search(
      reporter: current_user.id,
      resolved: report_status,
      reason: report_reason_query,
      message: report_msg_query,
      msg_id: report_msg_id,
      channel: report_msg_channel,
      sender: report_msg_sender,
      start_time: report_start_time,
      end_time: report_end_time,
      per_page: report_per_page
    )

    # Determine query_type
    if report_type.nil?
      query_type = :report_browse_user
    else
      query_type = :report_query_user
    end

    query_notes = {}
    if @search_errors.length == 0 && query_type == :report_query_user
      begin
        query_time = Benchmark.realtime {
          @reports = Report.search(
            reporter: current_user.id,
            resolved: report_status,
            reason: report_reason_query,
            message: report_msg_query,
            msg_id: report_msg_id,
            channel: report_msg_channel,
            sender: report_msg_sender,
            start_time: report_start_time,
            end_time: report_end_time
          )
        }
        # Benchmark.realtime = user + system + IO/network/etc time; basically,
        # the total time you would get if you used a stopwatch to measure it.
        # https://stackoverflow.com/a/1616287
        # Returns a float.
      rescue RegexpError, ActiveRecord::StatementInvalid # User entered invalid regex
        if !@search_errors.any? { |error| error.include? 'regex' }
          # Only add this error message if [invalid] regex error messages have
          # not yet been added to the list.
          @search_errors << "One or more invalid regular expressions"
        end
        query_time = Benchmark.realtime {
          @reports = Report.search(reporter: current_user.id)
        }
      end
    else
      if @search_errors.length > 0
        flash[:error] = I18n.t('.activerecord.errors.generic.search.failed')
        query_notes = flash.to_h.merge(:search_errors => @search_errors)
      end
      query_time = Benchmark.realtime {
        @reports = Report.search(reporter: current_user.id)
        # Filter to only reports sent by the current user
      }
    end

    custom_per_page = per_page == params[:per_page]
    Search.create(
      searcher: current_user.id,
      user_id: current_user.id,
      query_type: query_type,
      query_notes: query_notes,
      results: @reports.count,
      execution_time: query_time,
      search_params: {
        searched_type: report_type,
        channel: report_msg_channel,
        sender: report_msg_sender,
        message_query: report_msg_query,
        reporter: current_user.id,
        base_id: report_msg_id,
        highlight: params[:highlight],
        start_time: report_start_time,
        end_time: report_end_time,
        reason_query: report_reason_query,
        page: params[:page],
        per_page: per_page,
        sort_column: report_sort_column,
        sort_direction: sort_direction,
        custom_per_page: custom_per_page
      },
      url: request.original_url
    )

    @reports = @reports.order("#{report_sort_column} #{sort_direction}").page(params[:page]).per(per_page) # Sort results with results per page specified by the user
  end

  # GET /account/reports/1
  # GET /account/reports/1.json
  def show
    @NumReportsForMessage = Report.where(msg_id: @report.msg_id).count
      # Total # of reports for this message
    @NumReportsForSender = Report.where(msg_sender: @report.msg_sender).count
      # Total # of reports for messages by this sender
    @NumMessagesReportedForSender = Report.where(msg_sender: @report.msg_sender).distinct.count(:msg_id)
      # Total # of messages from this sender that have been reported
      # https://stackoverflow.com/a/19362288
    @NumReportsFromReporter = Report.where(reporter: @report.reporter).count
      # Total # of reports from this reporter
    @MessageExists = ChatLog.where(id: @report.msg_id).count > 0
    if @report.reporter != current_user.id && !is_admin
      # Eventually, I will extend this to allow admins to view all reports
      flash[:error] = "You are not authorized to view this report."
      redirect_to reports_path
    end
  end

  # GET /account/reports/new
  def new
    @report = Report.new
    @chat_log = ChatLog.find_by_id(params[:msg_id]) if params[:msg_id] && Integer(params[:msg_id]) rescue nil
    unless @chat_log
      flash[:error] = "#{I18n.t('.activerecord.errors.models.chat_log.attributes.default.name')} #{I18n.t('.activerecord.errors.models.chat_log.attributes.default.id_not_found', :n => params[:msg_id])}"
      redirect_to chat_logs_path and return
    end
    existing_reports = Report.where(reporter: current_user.id, msg_id: params[:msg_id]).length
    if existing_reports > 0
      report_count = " #{existing_reports} times" if existing_reports > 1
      report_plural = "s" if existing_reports > 1
      flash[:notice] = "You have already reported this message#{report_count}. To view the status of your report#{report_plural}, go to the Reports tab in the Settings menu."
    end
  end

  # GET /account/reports/1/edit
  def edit
    if request.patch? || request.post?
      r_params = if params[:report] then report_params else {} end
      if r_params.keys.length == 1 && r_params[:msg_text] && r_params[:msg_text].length > 0
        @report.msg_text = r_params[:msg_text]
        @report.save!
        logger.info "User with ID #{current_user.id} has updated the message copy for report #{@report.id} to #{@report.msg_text}."
        flash[:success] = "Successfully updated report message."
      elsif r_params.keys.length == 1 && r_params[:msg_text]
        logger.info "User with ID #{current_user.id} tried to change the message copy for report #{@report.id} to ''."
        flash[:error] = "Unable to save report with blank original message. If the entire message copy needs to be removed, change it to \"[redacted]\"."
      elsif r_params.keys.length != 1
        logger.warn "User with ID #{current_user.id} appears to have tampered with the edit report message form. Expected 1 key (msg_text), got: #{r_params.keys}."
        flash[:error] = "Invalid values submitted, please try again. If this continues, contact the administrator."
      end
      redirect_to report_path(@report)
    end
  end

  # POST /reports
  # POST /reports.json
  def create
    r_params = report_params
    if request.post?
      chat_log = ChatLog.find_by_id(r_params[:msg_id]) if r_params[:msg_id]
      # Verify the message exists
      if chat_log
        # Verify report length is in the range [1, 255]
        # Eventually, the minimum report length will be settable via a configuration file
        if r_params[:report_reason].length > 0 && r_params[:report_reason].length < 256
          # Fill in all other information
          r_params[:user_id] = current_user.id
          r_params[:reporter] = current_user.id
          # report_reason and msg_id are already in r_params
          r_params[:msg_channel] = chat_log[:channel]
          r_params[:msg_sender] = chat_log[:sender]
          r_params[:msg_text] = chat_log[:message]
          r_params[:msg_date] = chat_log[:date]
          @report = Report.new(r_params)
        else
          flash[:error] = "Invalid report reason (must be between 1 and #{Rails.configuration.application[:common][:string_type_max_length]} characters)."
        end
      else
        flash[:error] = "No message ID. To report a message, click Show next to a message and click on the Report button."
      end
    end

    respond_to do |format|
      if @report&.save
        format.html { redirect_to reports_path(:highlight => @report.id), notice: 'Report was successfully created.' }
        format.json { render :show, status: :created, location: @report }
      else
        # need to fix nil errors on blank reason
        format.html { redirect_to new_report_path(:msg_id => r_params[:msg_id]) }
        format.json { render json: @report.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /account/reports/1
  # PATCH/PUT /account/reports/1.json
  def update
    respond_to do |format|
      if @report.update(report_params)
        format.html { redirect_to @report, notice: 'Report was successfully updated.' }
        format.json { render :show, status: :ok, location: @report }
      else
        format.html { render :edit }
        format.json { render json: @report.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /account/reports/1
  # DELETE /account/reports/1.json
  def destroy
    if @report.resolved # Resolved reports cannot be deleted by users.
      # Eventually, I'll extend this to allow admins to delete reports that
      # have been resolved.
      flash[:error] = "Error: Cannot delete resolved report."
      if from_admin_view
        redirect_to admin_reports_path
      else
        redirect_to reports_path
      end
    else
      @report.destroy
      respond_to do |format|
        if from_admin_view
          format.html { redirect_to admin_reports_url, notice: 'Report was successfully deleted.' }
        else
          format.html { redirect_to reports_url, notice: 'Report was successfully deleted.' }
        end
        format.json { head :no_content }
      end
    end
  end

  def resolve
    session[:return_to] = request.original_url if !request.original_url.nil?
    # I set return_to so the user will be automatically redirected back once
    # they edit or delete the message.
    if request.get?
      @chat_log = ChatLog.find_by_id(@report.msg_id)
    elsif request.patch? || request.post? # Report has been resolved
      session[:return_to] = nil
      if params[:report][:resolving_action]
        if @report.resolved # Report has already been resolved
          success_message = "Updated report."
          failure_message = "Failed to update report, see "
        else
          success_message = "Successfully resolved report."
          failure_message = "Failed to resolve report, see "
          @report.resolved_at = DateTime.now()
        end
        @report.resolved = true
        @report.resolving_action = params[:report][:resolving_action]
        @report.resolver = current_user.id

        respond_to do |format|
          if @report&.save
            flash[:success] = success_message
            format.html { redirect_to admin_reports_path(:highlight => @report.id) } # When I implement the admin console, when an admin resolves a report, it should redirect them to the list of all reports
            format.json { render :index, status: :ok, location: @report }
          else
            if @report.errors.count > 1
              failure_message << "errors"
            else
              failure_message << "error"
            end
            failure_message << " below."
            flash[:error] = failure_message
            format.html { render 'resolve' }
            format.json { render json: @report.errors, status: :unprocessable_entity }
          end
        end
      else
        flash[:error] = "Error: you must provide an action taken."
        redirect_to resolve_report_path(@report.id)
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_report
    begin
      @report = Report.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      @report = nil
      if is_admin
        flash[:error] = "#{I18n.t('.activerecord.errors.models.report.attributes.default.name')} #{I18n.t('.activerecord.errors.models.report.attributes.default.not_found')}"
        redirect_to admin_reports_path
      else
        flash[:error] = I18n.t('.activerecord.errors.permissions.general.unauthorized')
        redirect_to root_path
      end
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def report_params
    params.require(:report).permit(
      :reporter,
      :msg_id,
      :msg_channel,
      :sender,
      :msg_text,
      :report_reason,
      :resolved,
      :resolving_action,
      :resolver,
      :resolved_at,
      :start_time,
      :end_time,
      :page,
      :per_page,
      :utf8
    )
  end

  def verify_permissions
    if permissions_list[action_name.to_sym]&.index(current_user.role.to_sym).nil?
      # action_name is a Rails method for the controller method name
      # ( https://stackoverflow.com/a/4274222 )
      # Safe navigation operator (&) only calls if not nil.
      flash[:error] = I18n.t('.activerecord.errors.permissions.general.unauthorized')
      redirect_to root_path #, :status => :bad_request
      # I cannot return HTTP 403 with content, so the user gets a page that
      # says "You are being redirected." with a link to the root page.
      # Returning 200 is not best practice here but it looks best to the
      # user who never has to see an unformatted redirect page.
    end
  end

  def permissions_list
    {
      :index => [:admin, :user],
      :show => [:admin],
      :new => [:admin, :user],
      :edit => [:admin],
      :create => [:admin, :user],
      :update => [:admin],
      :destroy => [:admin, :user],
      :resolve => [:admin]
    }
  end

  def verify_ownership
    unless is_admin || (@report && @report.reporter == current_user.id)
      flash[:error] = I18n.t('.activerecord.errors.permissions.general.unauthorized')
      redirect_to root_path
    end
  end

  def is_admin_view
    is_admin && request.path.split('/')[1] == 'admin'
    # request.path is the relative path, so I expect something like this:
    # "/admin/reports" -> ["", "admin", "reports"]
  end

  def from_admin_view
    is_admin && (request.referrer.split('/')[3] == 'admin' || request.referrer.split('/')[5] == @report.id.to_s)
    # request.referrer is the entire URL, not just the relative path, so
    # I expect to see something similar to this:
    # ["https:", "", "domain.example:3000", "admin", "reports"]
    # for "https://domain.example:3000/admin/reports", or
    # ["https:", "", "localhost.lucadou.sh:3000", "account", "reports", "36"]
    # for "https://localhost.lucadou.sh:3000/account/reports/36"
  end
end
