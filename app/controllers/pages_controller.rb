class PagesController < ApplicationController
  after_action :clear_flashes
  before_action :verify_not_banned!

  def index
  end

  def help_index
  end

  def help_markdown
  end

  def help_regex
  end
end
