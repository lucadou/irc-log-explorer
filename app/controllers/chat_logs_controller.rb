require 'benchmark'
require 'date'

class ChatLogsController < ApplicationController
  include ChatLogsHelper
  after_action :clear_flashes, except: [:advanced_search, :destroy, :update]
    # Without this exclusion, any error messages from advanced_search will
    # be removed while calling the index method, and the user would never
    # see them.
  before_action :set_chat_log, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :verify_permissions

  # Controller methods

  def index
    # Determine search type
    # Pontential referrers:
    # /chat_logs/1234 -> ["https:", "", "[domain:port]", "chat_logs", "1234"] -> context (if base_id exists)
    # /chat_logs/advanced_search -> ["https:", "", "[domain:port]", "chat_logs", "advanced_search"] -> advanced_search
    # /chat_logs/1234/edit -> ["https:", "", "[domain:port]", "chat_logs", "1234", "edit"] -> have to determine based on params
    referrer = if request.referrer then request.referrer.split('/') else '' end
    if ( (referrer.length == 3 && referrer[2].to_i > 0 && params[:base_id]) ||
         (referrer.length == 5 && referrer[4].to_i > 0 && params[:base_id]) )
      query_type = :context
    elsif ( (referrer.length == 3 && referrer[2] == 'advanced_search') ||
            (referrer.length == 5 && referrer[4] == 'advanced_search') )
      query_type = :advanced
    elsif ( (referrer.length == 2 && referrer[1] == 'chat_logs' && params[:query]) ||
            (referrer.length == 4 && referrer[3] == 'chat_logs' && params[:query]) )
      query_type = :basic
    else#elsif referrer.length == 4 && referrer[3] == 'edit'
      if params[:base_id]
        query_type = :context
      elsif params[:query]
        query_type = :basic
      elsif !params[:case_sensitive].nil?
        # Warning: wall of text.
        # If an advanced search is submitted, none of the params are separated
        # like with a form. That is, while editing a chat_log has these params:
        #   {"utf8"=>"✓", "authenticity_token"=>"[token]", "chat_log"=>
        #     {"channel"=>"[channel]", "sender"=>"[user]",
        #     "message"=>"[message]", "date"=>"[timestamp]"},
        #   "commit"=>"Edit", "id"=>"39"}
        # (note the chat_log sub-hash), an advanced search will have these:
        #   {"utf8"=>"✓", "authenticity_token"=>"[token]", "channel"=>"",
        #   "sender"=>"", "message"=>"[message]", "case_sensitive"=>"0",
        #   "start_time"=>"", "end_time"=>"", "start_id"=>"", "end_id"=>"",
        #   "per_page"=>"", "commit"=>"Search"}
        # Note how everything is in the same level of the hash. When you edit
        # a message and are redirected to the index, the only params that are
        # kept are the ones you submitted, with the exception of one:
        # case_sensitive. case_sensitive is always sent if any other values
        # were given in an advanced search. This is useful because I don't
        # want to check literally every other param for advanced searches.
        # So, if case_sensitive is present, an advanced search with any other
        # values has occured (since submitting a blank advanced search gives
        # a blank search error and has no advanced search params).
        # If the user did a blank advanced search, it is caught by the referrer
        # check above, but if the user edited a message, that referrer will
        # be 'edit'. In this case, I do not care, since it is the same as if
        # a user clicked on a message without ever going to the advanced search
        # page.
        # TL;DR - I can easily identify when an advanced search first executes,
        # but when a user edits a chat log and returns to the index, I can only
        # identify advanced searches if the user actually searched for
        # anything. If they didn't search for anything, I don't care about
        # trying to track that past the initial search.
        query_type = :advanced
      else
        query_type = :browse
      end
    #else
    #  query_type = :browse
    end

    # Clear previous page
    session.delete(:return_to)
    user ||= Current.user

    # Set timezone offset varaible for view to reference
    @UserTZOffset = current_user.user_preference.tz_offset #if !current_user.user_preference.tz_offset.match(PreferencesHelper.tz_utc_regex)
      # If offset is +00:00 or -00:00, it doesn't show the offset since that
      # is already UTC.

    # Extract variables from params
    channel = params[:channel]
    sender = params[:sender]
    basic_search = params[:query]
    advanced_search = params[:advanced_query]
    case_sensitive = params[:case_sensitive] || false

    base_id = params[:base_id].to_i if !params[:base_id].nil?
    start_id = params[:start_id].to_i if !params[:start_id].nil?
    end_id = params[:end_id].to_i if !params[:end_id].nil?

    start_time = params[:start_time] if !params[:start_time].nil?
    end_time = params[:end_time] if !params[:end_time].nil?

    if !params[:per_page].nil? && (!PreferencesHelper.valid_per_page(params[:per_page].to_i))
      flash[:error] = "#{I18n.t('.activerecord.errors.models.user_preference.attributes.search_results.name')}" \
          " #{I18n.t('.activerecord.errors.models.user_preference.attributes.search_results.invalid', min: PreferencesHelper.min_per_page, max: PreferencesHelper.max_per_page)}"
    end

    # Context searches
    if !base_id.nil? && ChatLog.find_by_id(base_id)
      base_chat_log = ChatLog.find(base_id)
      base_time = ApplicationHelper.datetime_from_epoch(base_chat_log.date)
      start_time = base_time - 1.minute
      end_time = base_time + 1.minute
      flash[:info] = "Displaying messages in #{base_chat_log.channel} from #{ApplicationHelper.datetime_to_str(start_time, show_offset: false, show_seconds: true)} to #{ApplicationHelper.datetime_to_str(end_time, show_offset: false, show_seconds: true)}"
    elsif !base_id.nil? && ChatLog.where(id: base_id).count == 0
      flash[:error] = "Unable to find message with ID #{base_id}"
    end

    # Display error for blank basic searches
    flash[:notice] = I18n.t('.activerecord.errors.generic.search.no_params_empty_search') if params[:query] && params[:query].length == 0

    flash.discard # Clear any flashes so the user only sees them on the index/search results page

    @search_errors = ChatLog.validate_search(
      channel: channel,
      sender: sender,
      basic_search: basic_search,
      advanced_search: advanced_search,
      case_sensitive: case_sensitive,
      start_id: start_id,
      end_id: end_id,
      start_time: start_time.clone,
      end_time: end_time.clone,
      per_page: per_page
    )
    # Have to use .clone on start_time and end_time to prevent modifications
    # within the ChatLog model from affecting the original parameters.

    if @search_errors.empty?
      begin
        query_time = Benchmark.realtime {
          @chat_logs = ChatLog.search(
            channel: channel,
            sender: sender,
            basic_search: basic_search,
            advanced_search: advanced_search,
            case_sensitive: case_sensitive,
            start_id: start_id,
            end_id: end_id,
            start_time: start_time.clone,
            end_time: end_time.clone
          )
        }
        # Benchmark.realtime = user + system + IO/network/etc time; basically,
        # the total time you would get if you used a stopwatch to measure it.
        # https://stackoverflow.com/a/1616287
        # Returns a float.
        chat_logs_count = @chat_logs.count # See explanation below.
      rescue RegexpError, ActiveRecord::StatementInvalid # User entered invalid regex
        flash[:error] = "Invalid regex. Your search has been run with regex disabled. Please fix your expression and try again."
        query_time = Benchmark.realtime {
          @chat_logs = ChatLog.search(
            channel: channel,
            sender: sender,
            basic_search: basic_search,
            advanced_search: ApplicationHelper.isolate_regex(advanced_search),
            case_sensitive: case_sensitive,
            start_id: start_id,
            end_id: end_id,
            start_time: start_time,
            end_time: end_time
          )
        }
        chat_logs_count = @chat_logs.count
          # If the user submits regex that is valid to Ruby but invalid to
          # Postgres, calling @chat_logs.count in the Search.create causes a
          # ActiveRecord::StatementInvalid exception. I don't know why this
          # does not occur for regex that is invalid to Ruby, but to prevent
          # the Search from failing to create, I have to load the value into
          # a variable here. I thought this might cause chat_logs_count to be
          # nil if it threw an exception, but the Search object gets the correct
          # number of results, so somehow calling @chat_logs.count outside this
          # block causes an error (but not if it's called from inside a view...).
          # Regex errors are weird. The regex that caused this error was:
          # /timoh(?i)/
      end
    else
      if @search_errors.length > 0
        flash[:error] = I18n.t('.activerecord.errors.generic.search.failed')
        #query_notes = flash.to_h.merge(:search_errors => @search_errors)
      end
      query_time = Benchmark.realtime {
        @chat_logs = ChatLog.all
      }
    end

    # Log search
    if query_type
      custom_per_page = per_page == params[:per_page]
      Search.create(
        searcher: current_user.id,
        user_id: current_user.id,
        query_type: query_type,
        query_notes: flash.to_h,
        results: chat_logs_count,
        execution_time: query_time,
        search_params: {
          basic_query: basic_search,
          advanced_query: advanced_search,
          channel: channel,
          sender: sender,
          case_sensitive: case_sensitive,
          start_id: start_id,
          base_id: base_id,
          end_id: end_id,
          start_time: start_time,
          end_time: end_time,
          page: params[:page],
          per_page: per_page,
          highlight: params[:highlight],
          sort_column: chatlog_sort_column,
          sort_direction: sort_direction,
          custom_per_page: custom_per_page
        },
        url: request.original_url)
    else
      logger.error "Could not create Search record - unrecognized query_type: #{query_type}."
    end

    @chat_logs = @chat_logs.order("#{chatlog_sort_column} #{sort_direction}").page(params[:page]).per(per_page) # sort results with results per page specified by the user
  end

  def show
    # Don't overwrite session[:return_to] if they are on the same URL
    # or when the user has edited a message.
    # I added this to prevent overwriting when the user refreshes the page.
    session[:return_to] = request.referer if !request.referer.nil? &&
      request.referer != request.original_url &&
      request.referer.index(edit_chat_log_path(ChatLog.find(params[:id]))).nil?
  end

  def advanced_search
    @prefs = ApplicationHelper.common_prefs

    if request.post?
      errors = []
      # Channel and sender
      channel = params[:channel] if params[:channel].present? && params[:channel].length > 0
      sender = params[:sender] if params[:sender].present? && params[:sender].length > 0
      message = params[:message] if params[:message].present? && params[:message].length > 0

      # ID range
      start_id = params[:start_id] if params[:start_id].present? && params[:start_id].length > 0
      end_id = params[:end_id] if params[:end_id].present? && params[:end_id].length > 0

      # Convert case sensitive from 1/0 to true/false
      case_sensitive = if !message.nil? && message.length > 0 then (params[:case_sensitive] == '1') else false end # No nil check needed because it is "0" if unchecked.

      start_time = params[:start_time] if params[:start_time].present? && params[:start_time].length > 0
      end_time = params[:end_time] if params[:end_time].present? && params[:end_time].length > 0

      # If no variables are set, this variable will be used to flash a message
      # that they submitted a blank form & no search has been performed in the
      # index controller method
      is_search = [channel, sender, message, case_sensitive, start_time, end_time, start_id, end_id, per_page].compact.length > 0
        # array.compact removes all nil values, so [nil nil nil].compact =>
        # [], and [].length == 0
      errors = ChatLog.validate_search(channel: channel, sender: sender, advanced_search: message, case_sensitive: case_sensitive, start_id: start_id, end_id: end_id, start_time: start_time, end_time: end_time, per_page: per_page)

      if errors.empty? && is_search
        clear_flashes and redirect_to chat_logs_path(:channel => channel, :sender => sender, :advanced_query => message, :case_sensitive => case_sensitive, :start_time => start_time, :end_time => end_time, :start_id => start_id, :end_id => end_id, :per_page => per_page)
        # clear_flashes removes any flashes from previous errors. Sometimes I
        # want to keep the flashes, other times I want to clear, which is why I
        # cannot just do a before_action for this method.
      elsif errors.empty?
        flash[:notice] = I18n.t('.activerecord.errors.generic.search.invalid_params_empty_search')
        redirect_to chat_logs_path
      else
        flash[:error] = I18n.t('.activerecord.errors.generic.search.failed')
        @errors = errors
        # No redirect_to advanced_search_chat_logs_path because doing so clears @errors
      end
    end
  end

  def edit
    #@chat_log_edit = ChatLogEdit.new(chat_log_edit_params)
    #@chat_log.chat_log_edit.build
  end

  def new
    @chat_log = ChatLog.new
  end

  def create
    @chat_log = ChatLog.new(chat_log_params)
    respond_to do |format|
      if @chat_log.save
        format.html { redirect_to @chat_log, notice: 'Chat log was successfully created.' }
        format.json { render :show, status: :created, location: @chat_log }
      else
        format.html { render :new }
        format.json { render json: @chat_log.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    success_message = 'Chat log was successfully updated.'

    cle_params = chat_log_params[:chat_log_edit]
    cle_params[:chat_log_id] = @chat_log.id
    cle_params[:chat_log_edited] = @chat_log.id
    cle_params[:editor_id] = current_user.id
    cle_params[:edited_by] = current_user.id
    @chat_log.chat_log_edits.build(cle_params)
    # Checking for success of @chat_log.update not needed before calling
    # .build - .build only runs if .update succeeds. That is, if the ChatLog
    # is invalid, the ChatLogEdit record will not be saved.

    update_params = chat_log_params
    update_params.delete(:chat_log_edit)
    respond_to do |format|
      if @chat_log.update(update_params)
        if session[:return_to]
          format.html { redirect_to session[:return_to], notice: success_message }
          # Used by ReportsController#resolve.
        else
          format.html { redirect_to @chat_log, notice: success_message }
        end
        format.json { render :show, status: :ok, location: @chat_log }
      else
        format.html { render :edit }
        format.json { render json: @chat_log.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @chat_log.destroy
    success_message = 'Chat log was successfully deleted.'
    respond_to do |format|
      if session[:return_to]
        format.html { redirect_to session[:return_to], notice: success_message }
        # Used by ReportsController#resolve.
      else
        format.html { redirect_to chat_logs_url, notice: success_message }
      end
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_chat_log
    begin
      @chat_log = ChatLog.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      flash[:error] = "#{I18n.t('.activerecord.errors.models.chat_log.attributes.default.name')} #{I18n.t('.activerecord.errors.models.chat_log.attributes.default.id_not_found', :n => params[:id])}"
      redirect_to chat_logs_path
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def chat_log_params
    params.require(:chat_log).permit(
      :channel,
      :sender,
      :message,
      :date,
      chat_log_edit: [
        :internal_justification,
        :external_justification]
    )
  end

  def verify_permissions
    if UserHelper.permissions_list[action_name.to_sym]&.index(current_user.role.to_sym).nil?
      # action_name is a Rails method for the controller method name
      # ( https://stackoverflow.com/a/4274222 )
      # Safe navigation operator (&) only calls if not nil.
      flash[:error] = I18n.t('.activerecord.errors.permissions.general.unauthorized')
      redirect_to root_path #, :status => :bad_request
      # I cannot return HTTP 403 with content, so the user gets a page that
      # says "You are being redirected." with a link to the root page.
      # Returning 200 is not best practice here but it looks best to the
      # user who never has to see an unformatted redirect page.
    end
  end
end
