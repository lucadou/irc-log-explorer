class Admin::DashboardController < ApplicationController
  include Admin::DashboardHelper
  include ReportsHelper
  after_action :clear_flashes, except: [:user_ban, :user_profile]
  before_action :authenticate_user!
  before_action :admin_tab_badge_data
  before_action :verify_permissions

  def index
  end

  def searches
    @prefs = ApplicationHelper.common_prefs

    # Extract params
    search_type = params[:query_type] if params[:query_type] && params[:query_type].length > 0
    search_etime_min = params[:execution_time_min] if params[:execution_time_min] && params[:execution_time_min].length > 0
    search_etime_max = params[:execution_time_max] if params[:execution_time_max] && params[:execution_time_max].length > 0
    search_reporter_id = params[:user_id] if params[:user_id] && params[:user_id].length > 0
    search_errors = params[:query_notes] if params[:query_notes] && params[:query_notes].length > 0
    search_start_time = params[:start_time] if params[:start_time] && params[:start_time].length > 0
    search_end_time = params[:end_time] if params[:end_time] && params[:end_time].length > 0
    search_per_page = params[:per_page] if params[:per_page] && params[:per_page].length > 0
    search_scope = params[:query_scope] if params[:query_scope] && params[:query_scope].length > 0

    # Set query_type
    if search_type
      # If search_type is not nil, that means the user hit Search, as Query
      # will always be submitted (it's a dropdown menu).
      query_type = :search_query
    else
      query_type = :search_browse
    end

    @search_errors = Search.validate_search(
      query_type: search_type,
      query_notes: search_errors,
      query_scope: search_scope,
      searcher: search_reporter_id,
      execution_time_min: search_etime_min,
      execution_time_max: search_etime_max,
      start_time: search_start_time,
      end_time: search_end_time,
      per_page: search_per_page
    )

    query_notes = {}
    if @search_errors.count == 0 && search_type
      query_time = Benchmark.realtime {
        @Searches = Search.search(
          query_type: search_type,
          query_notes: search_errors,
          query_scope: search_scope,
          searcher: search_reporter_id,
          execution_time_min: search_etime_min,
          execution_time_max: search_etime_max,
          start_time: search_start_time,
          end_time: search_end_time
        )
      }
    else
      if @search_errors.count > 0
        flash[:error] = I18n.t('.activerecord.errors.generic.search.failed')
        query_notes = flash.to_h.merge(:search_errors => @search_errors)
      end
      query_time = Benchmark.realtime {
        @Searches = Search.all
      }
    end

    custom_per_page = per_page == params[:per_page]
    Search.create(
      searcher: current_user.id,
      user_id: current_user.id,
      query_type: query_type,
      query_notes: query_notes,
      results: @Searches.count,
      execution_time: query_time,
      search_params: {
        searched_type: search_type,
        searched_scope: search_scope,
        searched_errors: search_errors,
        searched_id: search_reporter_id,
        exec_time_min: search_etime_min,
        exec_time_max: search_etime_max,
        start_time: search_start_time,
        end_time: search_end_time,
        page: params[:page],
        per_page: per_page,
        sort_column: dash_search_sort_column,
        sort_direction: sort_direction,
        custom_per_page: custom_per_page
      },
      url: request.original_url
    )

    @Searches = @Searches.order("#{dash_search_sort_column} #{sort_direction}").page(params[:page]).per(per_page) # sort results with results per page specified by the user
  end

  def system_log
  end

  def system_settings
  end

  def reports
    @prefs = ApplicationHelper.common_prefs

    # Extract params
    report_type = params[:resolved] if params[:resolved] && params[:resolved].length > 0
    report_reason_query = params[:reason] if params[:reason] && params[:reason].length > 0
    report_user_id = params[:user_id] if params[:user_id] && params[:user_id].length > 0
    report_msg_channel = params[:channel] if params[:channel] && params[:channel].length > 0
    report_msg_query = params[:advanced_query] if params[:advanced_query] && params[:advanced_query].length > 0
    report_msg_sender = params[:sender] if params[:sender] && params[:sender].length > 0
    report_msg_id = params[:msg_id] if params[:msg_id] && params[:msg_id].length > 0
    report_resolver_id = params[:resolver_id] if params[:resolver_id] && params[:resolver_id].length > 0
    report_start_time = params[:start_time] if params[:start_time] && params[:start_time].length > 0
    report_end_time = params[:end_time] if params[:end_time] && params[:end_time].length > 0
    report_per_page = params[:per_page] if params[:per_page] && params[:per_page].length > 0

    @search_errors = Report.validate_search(
      reporter: report_user_id,
      resolved: report_type,
      resolver: report_resolver_id,
      reason: report_reason_query,
      channel: report_msg_channel,
      message: report_msg_query,
      msg_id: report_msg_id,
      sender: report_msg_sender,
      start_time: report_start_time,
      end_time: report_end_time,
      per_page: report_per_page
    )

    # Determine query_type
    if report_type.nil?
      query_type = :report_browse
    else
      query_type = :report_query
    end

    query_notes = {}
    if @search_errors.length == 0 && dashboard_params[:resolved]
      begin
        query_time = Benchmark.realtime {
          @reports = Report.search(
            reporter: report_user_id,
            resolved: report_type,
            resolver: report_resolver_id,
            reason: report_reason_query,
            channel: report_msg_channel,
            message: report_msg_query,
            msg_id: report_msg_id,
            sender: report_msg_sender,
            start_time: report_start_time,
            end_time: report_end_time
          )
        }
        # Benchmark.realtime = user + system + IO/network/etc time; basically,
        # the total time you would get if you used a stopwatch to measure it.
        # https://stackoverflow.com/a/1616287
        # Returns a float.
      rescue RegexpError, ActiveRecord::StatementInvalid # User entered invalid regex
        #if !@search_errors.any? { |error| error.include? 'regex' }
        #  # Only add this error message if [invalid] regex error messages have
        #  # not yet been added to the list.
        #  @search_errors << "One or more invalid regular expressions"
        #end
        query_time = Benchmark.realtime {
          @reports = Report.all
        }
      end
    else
      if @search_errors.length > 0
        flash[:error] = I18n.t('.activerecord.errors.generic.search.failed')
        query_notes = flash.to_h.merge(:search_errors => @search_errors)
      end
      query_time = Benchmark.realtime {
        @reports = Report.all
      }
    end

    custom_per_page = per_page == params[:per_page]
    Search.create(
      searcher: current_user.id,
      user_id: current_user.id,
      query_type: query_type,
      query_notes: query_notes,
      results: @reports.count,
      execution_time: query_time,
      search_params: {
        searched_type: report_type,
        channel: report_msg_channel,
        sender: report_msg_sender,
        message_query: report_msg_query,
        reporter: report_user_id,
        resolver: report_resolver_id,
        base_id: report_msg_id,
        highlight: params[:highlight],
        start_time: report_start_time,
        end_time: report_end_time,
        reason_query: report_reason_query,
        page: params[:page],
        per_page: per_page,
        sort_column: report_sort_column,
        sort_direction: sort_direction,
        custom_per_page: custom_per_page
      },
      url: request.original_url
    )

    @reports = @reports.order("#{report_sort_column} #{sort_direction}").page(params[:page]).per(per_page) # Sort results with results per page specified by the user
  end

  def user_management
    @users = User.all

    @users = @users.order("#{dash_search_sort_column} #{sort_direction}").page(params[:page]).per(per_page) # sort results with results per page specified by the user
  end

  def user_profile
    @user = User.find_by_id(params[:id])
    if @user.nil?
      flash[:error] = "User not found"
      redirect_to admin_users_path and return
    end
    @admin_user_profile_page = true
      # current_page? cannot properly target this page, so I have to use
      # this variable instead.
    # Messages (Conversations) section
    @user_messages = {}
    # I know this refers to Conversations, I just name it user_messages because
    # I refer to them as Messages on the user profile page. It's for consistency.
    @user_messages[:recent_count] = 5
    @user_messages[:recent] = Conversation.recent_conversations(user_id: @user.id, num_conversations: @user_messages[:recent_count])
    @user_messages[:all_count] = Conversation.conversation_count(user_id: @user.id)

    # Reports section
    @user_reports = {}
    @user_reports[:recent_count] = 5
    @user_reports[:recent] = Report.recent_reports(user_id: @user.id, num_reports: @user_reports[:recent_count])
    @user_reports[:all_count] = Report.report_count(user_id: @user.id)
    @user_reports[:unresolved_count] = Report.report_count(user_id: @user.id, resolved: false)
    @user_reports[:resolved_count] = Report.report_count(user_id: @user.id, resolved: true)
    if @user.role == "admin"
      @user_reports[:admin_resolved_count] = Report.report_resolved_count(user_id: @user.id)
    end

    # Searches section
    @user_searches = {}
    @user_searches[:recent_count] = 5
    @user_searches[:recent] = Search.recent_searches(user_id: @user.id, num_searches: @user_searches[:recent_count])
    @user_searches[:all_count] = Search.num_searches(user_id: @user.id)
    # Calculate average search times
    @user_searches[:user_avg_time] = Search.avg_search_time(user_id: @user.id)
    @user_searches[:overall_avg_time] = Search.avg_search_time
    # Calculate difference between overall & user
    @user_searches[:avg_difference] = (@user_searches[:user_avg_time] - @user_searches[:overall_avg_time]).abs
    if @user_searches[:user_avg_time] >= @user_searches[:overall_avg_time]
      @user_searches[:trend] = "greater"
    else
      @user_searches[:trend] = "less"
    end

    # Bans section
    @user_bans = {}
    @user_bans[:bans_received] = @user.bans
    @user_bans[:issued_recent_count] = 5
    @user_bans[:bans_issued] = @user.bans_issued(num_bans: @user_bans[:issued_recent_count])
    @user_bans[:bans_given_count] = @user.num_users_banned
    @user_bans[:bans_removed_count] = @user.num_users_unbanned
    @user_bans[:bans_issued_count] = @user_bans[:bans_given_count] + @user_bans[:bans_removed_count]

    # Preferences section
    @user_preferences = {}
    @user_preferences[:prefs] = @user.user_preference
    @user_preferences[:defaults] = UserPreference.column_defaults
      # https://stackoverflow.com/a/34659251
    @user_preferences[:settable_notify] = PreferencesHelper.user_settable_prefs(user: @user)

    if request.post? # Promote user to admin or demote from admin
      @user = User.find_by_id(params[:id])
      if @user.role == 'admin'
        @user.role = :user
        if @user.save
          flash[:success] = "Successfully demoted user #{@user.id} from admin"
          redirect_to admin_manage_user_path(@user)
        else
          flash[:failure] = "Failed to demote user #{@user.id} from admin"
          redirect_to admin_manage_user_path(@user)
        end
      else
        @user.role = :admin
        if @user.save
          flash[:success] = "Successfully promoted user #{@user.id} to admin"
          redirect_to admin_manage_user_path(@user)
        else
          flash[:failure] = "Failed to promote user #{@user.id} to admin"
          redirect_to admin_manage_user_path(@user)
        end
      end
    elsif request.delete?
      user_id = @user.id
      @user = nil
      flash.merge!(User.delete_user(user_id: user_id, first_person: false))
      redirect_to admin_users_path
    end
  end

  def user_ban

  end

  private

  def dashboard_params
    params.permit(
      :resolved,
      :reason,
      :user_id,
      :channel,
      :advanced_query,
      :sender,
      :resolver_id,
      :msg_id,
      :start_time,
      :end_time,
      :query_type,
      :execution_time_min,
      :execution_time_max,
      :query_scope,
      :query_notes,
      :column,
      :direction,
      :highlight,
      :per_page,
      :utf8
    )
  end

  def user_ban_params
    params.require(:user).permit(
      :internal_reason,
      :external_reason,
      :banned_until,
      :permanent_ban
    )
  end

  def verify_permissions
    if permissions_list[action_name.to_sym]&.index(current_user.role.to_sym).nil?
      # action_name is a Rails method for the controller method name
      # ( https://stackoverflow.com/a/4274222 )
      # Safe navigation operator (&) only calls if not nil.
      flash[:error] = I18n.t('.activerecord.errors.permissions.general.unauthorized')
      redirect_to root_path #, :status => :bad_request
      # I cannot return HTTP 403 with content, so the user gets a page that
      # says "You are being redirected." with a link to the root page.
      # Returning 200 is not best practice here but it looks best to the
      # user who never has to see an unformatted redirect page.
    end
  end

  def permissions_list
    {
      :index => [:admin],
      :reports => [:admin],
      :searches => [:admin],
      :system_log => [:admin],
      :system_settings => [:admin],
      :user_management => [:admin],
      :user_profile => [:admin],
      :user_ban => [:admin]
    }
  end
end
