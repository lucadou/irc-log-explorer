class Admin::EulasController < ApplicationController
  include Admin::DashboardHelper
  include Admin::EulasHelper
  before_action :authenticate_user!
  before_action :admin_tab_badge_data
  before_action :set_tab_data
  before_action :set_eula, only: [:show]
  before_action :verify_permissions
  after_action :clear_flashes, except: [:create]

  def index
    @prefs = ApplicationHelper.common_prefs

    # Search fields
    search_params = search_eula_params

    # Extract params
    eula_note = search_params[:note] if search_params[:note] && search_params[:note].length > 0
    eula_eula_text = search_params[:eula_text] if search_params[:eula_text] && search_params[:eula_text].length > 0
    eula_publisher = search_params[:publisher] if search_params[:publisher] && search_params[:publisher].length > 0
    eula_start_time = search_params[:start_time] if search_params[:start_time] && search_params[:start_time].length > 0
    eula_end_time = search_params[:end_time] if search_params[:end_time] && search_params[:end_time].length > 0
    eula_per_page = search_params[:per_page] if search_params[:per_page] && search_params[:per_page].length > 0

    @search_errors = Eula.validate_search(note: eula_note, eula_text: eula_eula_text, publisher: eula_publisher,
                                          start_time: eula_start_time, end_time: eula_end_time, per_page: eula_per_page)

    query_notes = {}
    if @search_errors.none?
      begin
        query_time = Benchmark.realtime {
          @eulas = Eula.search(note: eula_note, eula_text: eula_eula_text, publisher: eula_publisher,
                               start_time: eula_start_time, end_time: eula_end_time)
        }
        @eulas.count # see ConversationsController for why I need this
      rescue RegexpError, ActiveRecord::StatementInvalid => err
        if !@search_errors.any? { |error| error.include? 'regex' }
          # Only add error message if invalid regex messages not yet added to list
          if err.message.match(ApplicationHelper.pg_invalid_regex_locator)
            @search_errors["#{I18n.t('.activerecord.errors.models.eula.attributes.default.regex_error_generic')} "] =
                            "(#{I18n.t('.activerecord.errors.models.eula.attributes.default.regex_error_details_known')}: " \
                            "#{err.message.match(ApplicationHelper.pg_invalid_regex_locator)[1]})"
          else
            @search_errors["#{I18n.t('.activerecord.errors.models.eula.attributes.default.regex_error_generic')} "] =
                            "(#{I18n.t('.activerecord.errors.models.eula.attributes.default.regex_error_details_unknown')})"
            logger.warn("Admin EULAs controller - failed to match rescued regex error message")
            logger.debug("Regex error message: #{err.message}, class: #{err.message.class}")
          end
        end
        query_time = Benchmark.realtime {
          @eulas = Eula.all
        }
      end
    else
      flash[:error] = I18n.t('.activerecord.errors.generic.search.failed')
      query_notes = flash.to_h.merge(:search_errors => @search_errors)
      eula_per_page = nil
      # If there is a search error, I reset per page, as it is used in the calls to
      # @eulas.order, meaning if this is not reset, invalid per page might be used.
      # This is not needed for the other form fields because none of those will be
      # used in the event of a search error being reported, but per_page still is,
      # hence the nullification.
      query_time = Benchmark.realtime {
        @eulas = Eula.all
      }
    end

    custom_per_page = per_page == params[:per_page]
    # TODO: create Search record here

    @eulas = @eulas.order("#{dash_eula_sort_column} #{sort_direction}").page(params[:page]).per(eula_per_page)
  end

  def show
  end

  def new
    @eula ||= Eula.new
  end

  def create
    @eula = Eula.new(eula_params)
    @eula.user_id = current_user.id
    @eula.publisher = current_user.id
    respond_to do |format|
      if @eula.save
        # Prevent current user from getting logged out
        user = current_user
        user.eula_accepted_at = Time.now()
        user.save
        eula_acceptance_notice = 'To prevent you from being logged out, your account has automatically been updated to accept the TOS'
        flash[:success] = 'TOS published'
        flash[:notice] = eula_acceptance_notice
        format.html { redirect_to admin_eula_path(@eula.id) }
        format.json { render :show, status: :created, location: @eula }
      else
        format.html { render :new }
        format.json { render json: @eula.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def eula_params
    params.require(:eula).permit(
      :note,
      :eula_text
    )
  end

  def search_eula_params
    params.permit(
      :utf8,
      :note,
      :eula_text,
      :publisher,
      :start_time,
      :end_time,
      :per_page,
      :column,
      :direction
    )
  end

  def set_tab_data
    @eulas_tab = true
  end

  def set_eula
    @eula = Eula.find(params[:id])
  end

  def permissions_list
    {
      :index => [:admin],
      :new => [:admin],
      :show => [:admin],
      :create => [:admin]
    }
  end

  def verify_permissions
    if permissions_list[action_name.to_sym]&.index(current_user.role.to_sym).nil?
      # action_name is a Rails method for the controller method name
      # ( https://stackoverflow.com/a/4274222 )
      # Safe navigation operator (&) only calls if not nil.
      flash[:error] = I18n.t('.activerecord.errors.permissions.general.unauthorized')
      redirect_to root_path
    end
  end
end
