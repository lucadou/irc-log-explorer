class Admin::MessagesController < ApplicationController
  after_action :clear_flashes, only: [:index]
  before_action :authenticate_user!
  before_action :admin_tab_badge_data
  before_action :set_conversation
  before_action :set_message, only: [:edit, :hide, :mark_unread, :update, :destroy]
  before_action :set_tab_data
  before_action :verify_ownership, only: [:edit, :hide, :mark_unread, :update, :destroy]
  before_action :verify_permissions

  def new
    @message = @conversation.messages.new
  end

  def create
    user_view = true
    if user_view
      msg_params = admin_message_params
    else
      msg_params = message_params
    end
    msg_params[:sender_id] = current_user.id
    msg_params[:sent_by] = current_user.id
    @message = @conversation.messages.new(msg_params)
    respond_to do |format|
      begin
        if @message.save!
          # some redirect, will be based on the user's permission
          format.html { redirect_to admin_conversation_path(@conversation.id), notice: "#{I18n.t('.activerecord.errors.models.message.attributes.default.name')} #{I18n.t('.activerecord.errors.models.message.attributes.default.created', time: Message.edit_window_str)}" }
          format.json { render :show, status: :created, location: @message }
        end
      rescue ActiveRecord::RecordInvalid
        #format.html { redirect_to admin_conversation_path(@conversation.id, )}
        #format.html { render :_admin_form, message: @message }
        format.html { redirect_to admin_conversation_path(@conversation.id), alert: @message.errors }
        format.json { render :_admin_form, status: :internal_server_error, location: @conversation }
      end
    end
  end

  def edit
  end

  def mark_unread
    # pseudocode:
    # get conversation
    # get datetime of this message
    # set conversation.read_by_admins to datetime of this message
    # redirect to conversations screen, tell them X messages were marked unread
    # note - need to make a conversations model helper method to assist in calculating unreads
    #
    # pseudocode for logic for determining unreads:
    # count # of messages in conversation.messages where created_at >= conversation.read_by_admins
    @conversation.read_by_admins = @message.created_at
    @conversation.save!
    flash[:success] = "Marked #{@conversation.admin_unread_count} messages in the conversation as unread."
    redirect_to admin_conversations_path
  end

  def update
    @message.edited_at = Time.now.utc
    respond_to do |format|
      if @message.editable? && @message.update(admin_message_params)
        # some redirect, will be based on the user's permission
        format.html { redirect_to admin_conversation_path(@conversation.id), notice: 'Message edited.' }
        format.json { render :show, status: :ok, location: @conversation }
      else
        format.html { redirect_to conversation_path(@conversation.id), notice: 'Failed to edit message, the edit period might have expired.' }
        format.json { render :show, status: :internal_server_error, location: @message }
      end
    end
  end

  def destroy
    @message.deleter_id = current_user.id
    @message.deleted_by = current_user.id
    @message.user_visible = false

    respond_to do |format|
      if @message.save!
        # some redirect, will be based on the user's permission
        format.html { redirect_to admin_conversation_path(@conversation.id), notice: 'Message deleted.' }
        format.json { render :show, status: :ok, location: @conversation }
      else
        format.html { redirect_to admin_conversation_path(@conversation.id), notice: 'Failed to delete message.' }
        format.json { render :show, status: :internal_server_error, location: @message }
      end
    end
  end

  def hide
    # This just sets the hidden from user attribute on the message
    @message = @conversation.messages.find(params[:id])
    @message.user_visible = false
    # TODO: permissions (only admins should be allowed to do this)

    respond_to do |format|
      if @message.valid?
        @message.save!
        # some redirect, will be based on the user's permission
        format.html { redirect_to admin_conversation_path(@conversation.id), notice: 'Message hidden.' }
        format.json { render :show, status: :ok, location: @conversation }
      else
        # If I just try to use @message.errors, it causes an error because
        # @message is nil. If I try @conversation.messages[0].errors.any?,
        # it returns false for some reason.
        # I cannot figure out any better way to pass back the error message
        # to the view, as even setting my own global variable (like @errors)
        # will be nil in the view.
        error_list = ""
        @message.errors.full_messages.each { |message| error_list << "<li>#{message}</li>" }
        if error_list.length == 0
          error_list = '.'
        else
          error_list = ": <ul>#{error_list}</ul>"
        end

        format.html { redirect_to admin_conversation_path(@conversation.id), notice: 
"Failed to hide message#{error_list}" }
        format.json { render :show, status: :internal_server_error, location: @message }
      end
    end
  end

  private

  def message_params
    params.require(:message).permit(
      :sender_id,
      :body,
      :user_visible,
      :deleter
    )
  end

  def admin_message_params
    params.require(:message).permit(
      :body,
      :body_plaintext,
      :user_visible,
      :sent_anonymously
    )
  end

  def set_conversation
    begin
      @conversation = Conversation.find(params[:conversation_id])
    rescue ActiveRecord::RecordNotFound
      @conversation = nil
      flash[:error] = "#{I18n.t('.activerecord.errors.models.conversation.attributes.default.name')} #{I18n.t('.activerecord.errors.models.conversation.attributes.default.not_found')}"
      redirect_to admin_conversations_path
    end
  end

  def set_message
    begin
      @message = @conversation.messages.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      @conversation = nil
      # No is_admin check because permissions system handles this controller
      flash[:error] = "#{I18n.t('.activerecord.errors.models.message.attributes.default.name')} #{I18n.t('.activerecord.errors.models.message.attributes.default.not_found')}"
      redirect_to admin_conversations_path
    end
  end

  def set_tab_data
    # Currently, this assumes the user is on the account page, not admin page
    @messages_tab = true
  end

  def verify_ownership
    current_user.id == @message.sent_by # add redirects for un-owned messages
  end

  def permissions_list
    {
      :new => [:admin],
      :create => [:admin],
      :edit => [:admin],
      :mark_unread => [:admin],
      :update => [:admin],
      :destroy => [:admin],
      :hide => [:admin]
    }
  end

  def verify_permissions
    if permissions_list[action_name.to_sym]&.index(current_user.role.to_sym).nil?
      # action_name is a Rails method for the controller method name
      # ( https://stackoverflow.com/a/4274222 )
      # Safe navigation operator (&) only calls if not nil.
      flash[:error] = I18n.t('.activerecord.errors.permissions.general.unauthorized')
      redirect_to root_path #, :status => :bad_request
      # I cannot return HTTP 403 with content, so the user gets a page that
      # says "You are being redirected." with a link to the root page.
      # Returning 200 is not best practice here but it looks best to the
      # user who never has to see an unformatted redirect page.
    end
  end
end
