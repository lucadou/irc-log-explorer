class CredentialsController < ApplicationController
  after_action :clear_flashes, except: [:delete_with_totp, :delete_with_u2f, :create]
  before_action :authenticate_user!
  before_action :check_ownership, only: [:edit, :delete_with_totp, :delete_with_u2f]

  def self.controller_path
    # I'm stuffing this under the Devise umbrella since it handles basically
    # everything account and login related.
    # https://stackoverflow.com/a/25194710
    "devise/u2f_credentials"
  end

  def create
    if request.post?
      if params[:id] && params[:response]
        # These params are present in responses from the token
        auth_response = WebAuthn::AuthenticatorAttestationResponse.new(
          attestation_object: str_to_bin(params[:response][:attestationObject]),
          client_data_json: str_to_bin(params[:response][:clientDataJSON])
        )

        if auth_response.valid?(str_to_bin(current_user.current_challenge), request.base_url)
          if params[:response][:attestationObject].present?
            credential = current_user.credentials.find_or_initialize_by(
              external_id: Base64.strict_encode64(auth_response.credential.id)
            )

            credential.update!(
              nickname: params[:name],
              public_key: Base64.strict_encode64(auth_response.credential.public_key)
            )
          end
          flash[:success] = "Successfully registered \"#{params[:name]}\""
          render json: { status: "ok" }, status: :ok
        else
          flash[:error] = "Unable to register \"#{params[:name]}\" - invalid response."
          render json: { status: "forbidden" }, status: :forbidden
        end
        # No redirect statement needed, that occurs in
        # assets/javascripts/credentials_helper.js
      elsif params[:error]
        flash[:error] = "Unable to register \"#{params[:name]}\" - invalid response. The device may have been interrupted or have already been added to your account."
        render json: { status: "bad request" }, status: :bad_request
      else
        credential_options = WebAuthn.credential_creation_options
        credential_options[:user][:id] = Base64.strict_encode64(current_user.id.to_s)
        credential_options[:user][:name] = current_user.name
        credential_options[:user][:displayName] = current_user.name
        # Prevent user from adding the same key multiple times
        # https://github.com/cedarcode/webauthn-rails-demo-app/pull/65
        credential_options[:excludeCredentials] = current_user.credentials.map do |credential|
          { id: credential.external_id, type: "public-key" }
        end

        credential_options[:challenge] = bin_to_str(credential_options[:challenge])
        current_user.update!(current_challenge: credential_options[:challenge])

        respond_to do |format|
          format.json { render json: credential_options.merge(user_id: current_user.id) }
        end
      end
    end
  end

  def edit
    credential = Credential.find_by_id(params[:token_id])
    @SecurityTabPage = true
    # The reason I have this variable is because there is no other way to
    # highlight the Security tab. I have tried:
    # current_page?(account_security_2fa_u2f_edit_path)
    #   Works on /u2f/keys and /u2f/:token_id/edit
    #   Causes ActionController::UrlGenerationError (no route matches) on /u2f/add
    # current_page?(controller: 'credentials', action: 'edit')
    #   Works on /u2f/keys and /u2f/:token_id/edit
    #   Causes ActionController::UrlGenerationError (no route matches) on /u2f/add
    # current_page?(account_security_2fa_u2f_edit_path(:token_id))
    #   Works on /u2f/keys and /u2f/add
    #   Does not highlight on /u2f/:token_id/edit
    # If current_page allowed regex, I could easily do this. For some reason,
    # however, current_page cannot properly match from /u2f/add, so there is
    # no other way to consistently highlight the Security tab. (If anyone
    # figures how to do this, let me know!)
    if request.get?
      if credential.user_id == session[:user_id]
        # Check to make sure the user isn't attempting to edit a token
        # that doesn't belong to them
        @TokenNickname = credential.nickname
      else
        flash[:error] = "You are not authorized to modify this token. If you have reached this in error, please contact the administrator."
        logger.warn "User with ID #{session[:user_id]} is attempting to modify a U2F key registered to someone else: #{params[:token_id]}."
        redirect_to account_security_2fa_u2f_keys_path
      end
    elsif request.post?
      if params[:nickname]&.length > 0 # &. is the safe navigation operator
        credential.nickname = params[:nickname]
        credential.save!
        flash[:success] = "Successfully updated device name."
      else
        name = "Token #{credential.public_key[0, 32]}"
        credential.nickname = name
        credential.save!
        flash[:error] = "Since you left the device name field blank, the name \"#{name}\" has been automatically generated for you. If you just want to unregister the device, please hit the Delete button in the table row."
      end
      redirect_to account_security_2fa_u2f_keys_path(:highlight => credential.id)
    end
  end

  def delete_with_totp
    # Make sure TOTP is enabled or recovery codes exist
    user = User.find_by_id(current_user.id)

    if !user.two_factor_otp_enabled? && user.otp_backup_codes.nil?
      flash[:error] = "Must have TOTP or recovery codes setup to remove a U2F token with them."
      redirect_to account_security_2fa_u2f_keys_path and return
    end

    @SecurityTabPage = true
    @user_has_u2f = user.two_factor_u2f_enabled?
    @user_has_totp = user.two_factor_otp_enabled?
    @user_has_recovery_codes = user.otp_backup_codes.length > 0 if user.otp_backup_codes

    if request.post?
      if params[:user] && params[:user][:otp_attempt] && valid_otp_attempt?(user)
        token_name = current_user.credentials.find_by_id(params[:token_id].to_i)&.nickname
        Credential.destroy(params[:token_id])
        flash[:success] = "Successfully removed \"#{token_name}\" from your account."
        disable_recovery_codes
        redirect_to account_security_2fa_u2f_keys_path
      elsif !params[:user]
        logger.warn "User with ID #{session[:user_id]} appears to have tampered with the 2FA TOTP form; submitted with no params[:user] while trying to delete Credential #{params[:token_id]}."
      else
        flash[:error] = "Invalid 2FA or recovery code, please try again."
        logger.info "User with ID #{user.id} attempted to delete Credential #{params[:token_id]} with invalid TOTP or recovery code: #{params[:user][:otp_attempt]}."
      end
    end
  end

  def delete_with_u2f
    # No need to ensure U2F is enabled - if the user has U2F tokens, they
    # have U2F enabled
    user = User.find_by_id(current_user.id)
    @SecurityTabPage = true
    @user_has_totp = user.two_factor_otp_enabled?
    @user_has_recovery_codes = user.otp_backup_codes.length > 0 if user.otp_backup_codes

    if request.post?
      if params[:token_id] && params[:response]
        # These params are present in responses from the token
        auth_response = WebAuthn::AuthenticatorAssertionResponse.new(
          credential_id: str_to_bin(params[:id]),
          client_data_json: str_to_bin(params[:response][:clientDataJSON]),
          authenticator_data: str_to_bin(params[:response][:authenticatorData]),
          signature: str_to_bin(params[:response][:signature])
        )

        allowed_credentials = user.credentials.map do |cred|
          {
            id: str_to_bin(cred.external_id),
            public_key: str_to_bin(cred.public_key)
          }
        end

        if !auth_response.valid?(str_to_bin(user.current_challenge), request.base_url, allowed_credentials: allowed_credentials)
          flash[:error] = "Unrecognized token, did you insert an unregistered device?"
          render json: { status: "forbidden" }, status: :forbidden unless auth_response.valid?(
            str_to_bin(user.current_challenge),
            request.base_url,
            allowed_credentials: allowed_credentials
          )
        else
          token_name = current_user.credentials.find_by_id(params[:token_id].to_i)&.nickname
          Credential.destroy(params[:token_id])
          flash[:success] = "Successfully removed \"#{token_name}\" from your account."
          render json: { status: "ok" }, status: :ok
          # No redirect statement needed, that occurs in
          # assets/javascripts/credentials_helper.js
          disable_recovery_codes
        end
      else
        # User has pressed the U2F prompt button
        user = User.find_by_id(session[:user_id])

        if user
          credential_options = WebAuthn.credential_request_options
          credential_options[:allowCredentials] = user.credentials.map do |cred|
            { id: cred.external_id, type: "public-key" }
          end

          credential_options[:challenge] = bin_to_str(credential_options[:challenge])
          user.update!(current_challenge: credential_options[:challenge])

          respond_to do |format|
            format.json { render json: credential_options }
          end
        else
          respond_to do |format|
            format.json { render json: { errors: ["Username doesn't exist"] }, status: :unprocessable_entity }
          end
        end
      end
    end
  end

  def new
  end

  def index
  end

  private

  # Make sure the credential belongs to the user
  def check_ownership
    #error_not_found = "Token not found."
    error_not_authorized = error_not_found = "You are not authorized to modify this token. If you have reached this in error, please contact the administrator."
      # I display the same error for both not found & not authorized because
      # I don't want to leak information about which tokens exist to potential
      # attackers.
    if Credential.find_by_id(params[:token_id]).nil?
      flash[:error] = error_not_found
      logger.warn "User with ID #{current_user.id} attempted to #{action_name} Credential with ID #{params[:token_id]} (does not exist). "
      redirect_to account_security_2fa_u2f_keys_path and return false
    elsif Credential.find_by_id(params[:token_id]).user_id != current_user.id
      flash[:error] = error_not_authorized
      logger.warn "User with ID #{current_user.id} attempted to #{action_name} Credential with ID #{params[:token_id]} (actually belongs to user with ID #{Credential.find_by_id(params[:token_id]).user_id}). "
      redirect_to account_security_2fa_u2f_keys_path and return false
    end
  end

  def disable_recovery_codes
    if !current_user.two_factor_enabled? && !current_user.otp_backup_codes_enabled_at.nil?
      # Only clears recovery codes if no other 2FA methods are enabled
      # and the user has recovery codes
      current_user.otp_backup_codes = nil
      current_user.otp_backup_codes_enabled_at = nil
      current_user.save!
      flash[:notice] = "Because you have no 2FA methods enabled, your account recovery codes  have been disabled. To generate and use recovery codes, please enable a 2FA method."
    end
  end

  def valid_otp_attempt?(user)
    user.validate_and_consume_otp!(params[:user][:otp_attempt]) ||
      user.invalidate_otp_backup_code!(params[:user][:otp_attempt])
  end
end
