class UserValidator < ActiveModel::Validator
  def validate(user)
    if user.name.nil? || user.name.length < 1
      user.errors.add(I18n.t('.activerecord.errors.models.user.attributes.name.name'), I18n.t('.activerecord.errors.models.user.attributes.name.blank'))
    end
    if user.email.nil? || user.email.length < 1
      # I was worried that this would block SSO that didn't provide an email
      # (Facebook w/permissions revoked, Twitter w/o special permissions),
      # but since SSO doesn't create the record until the user provides their
      # email, it doesn't caues any problems.
      user.errors.add(I18n.t('.activerecord.errors.models.user.attributes.email.name'), I18n.t('.activerecord.errors.models.user.attributes.email.blank'))
    elsif (user.email =~ /^.+[@].+[.].{2,}$/) != 0
      # This validates that the user supplies [>=1 char]@[>=1 char].[>=2 chars]
      # This means you can supply "a@a.co" (a.co is a valid domain, it's owned
      # by Amazon) and have it be accepted, and it also allows any non-ANSI
      # character domain names and email addresses.
      # It's not the best email validator, but it's good enough for now.
      # It's also better than just using a 6 character minimum, as that would
      # allow "aaaaaa" to be a valid email address.
      user.errors.add(I18n.t('.activerecord.errors.models.user.attributes.email.name'), I18n.t('.activerecord.errors.models.user.attributes.email.invalid'))
    end
    if !user.backup_email.nil? && user.backup_email.length > 0 # This field is optional
      if user.backup_email == user.email
        user.errors.add(I18n.t('.activerecord.errors.models.user.attributes.backup_email.name'), I18n.t('.activerecord.errors.models.user.attributes.backup_email.duplicate_primary'))
      elsif (user.backup_email =~ /^.+[@].+[.].{2,}$/) != 0
        user.errors.add(I18n.t('.activerecord.errors.models.user.attributes.backup_email.name'), I18n.t('.activerecord.errors.models.user.attributes.email.invalid'))
      end
    end
    if !user.password.nil?
      if user.password.length < 16
        user.errors.add(I18n.t('.activerecord.errors.models.user.attributes.password.name'), I18n.t('.activerecord.errors.models.user.attributes.password.invalid'))
      end
    end
  end
end