class MessageValidator < ActiveModel::Validator
  def validate(message)
    validate_user_visible(message)
    validate_conversation_unlocked(message)
    validate_body(message)
  end

  def validate_user_visible(message)
    if !message.user_visible && message.admin_relative_id == 1
      if message.deleted_by
        message.errors[I18n.t('.activerecord.errors.models.message.attributes.user_visible.msg_not_deletable')] << I18n.t('.activerecord.errors.models.message.attributes.user_visible.msg_not_deletable_first')
      else
        message.errors[I18n.t('.activerecord.errors.models.message.attributes.user_visible.msg_not_hideable')] << I18n.t('.activerecord.errors.models.message.attributes.user_visible.msg_not_hideable_first')
      end
    end
  end

  def validate_conversation_unlocked(message)
    if message.conversation.locked
      message.errors[I18n.t('.activerecord.errors.models.message.attributes.default.name_failed_action')] << I18n.t('.activerecord.errors.models.message.attributes.default.error_locked')
    end
  end

  def validate_body(message)
    if !message.body ||
        message.body.length < Rails.configuration.application[:messages][:body][:min_length] ||
        message.body.length > Rails.configuration.application[:messages][:body][:max_length]
      message.errors.add(I18n.t('activerecord.errors.models.message.attributes.body.name'),
                         I18n.t('activerecord.errors.models.message.attributes.body.invalid_length',
                                min: Rails.configuration.application[:messages][:body][:min_length],
                                max: Rails.configuration.application[:messages][:body][:max_length]))
    end
  end

  def validate_sender(message)
    if message.sender_id &&
        message.sender_id == Rails.configuration.application[:common][:system_user_id]
      message.errors.add(I18n.t('activerecord.errors.models.message.attributes.sender.name'),
                         I18n.t('activerecord.errors.models.message.attributes.sender.invalid'))
    elsif message.sender_id
      begin
        User.find(message.sender_id)
      rescue ActiveRecord::RecordNotFound
        # User should exist since this is a non-null column
        message.errors.add(I18n.t('activerecord.errors.models.message.attributes.sender.name'),
                           I18n.t('activerecord.errors.models.message.attributes.sender.not_found'))
      end
    end
  end
end
