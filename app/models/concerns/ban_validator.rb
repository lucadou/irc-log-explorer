require 'date'

class BanValidator < ActiveModel::Validator
  def validate(ban)
    if ban.user_id.nil? || ban.user_id.to_s.length == 0
      # Banned user's ID not present
      ban.errors.add(I18n.t('.activerecord.errors.models.ban.attributes.user_id.name'), I18n.t('.activerecord.errors.models.ban.attributes.user_id.blank'))
    elsif User.find_by_id(ban.user_id).nil?
      # Banned user's ID not valid
      ban.errors.add(I18n.t('.activerecord.errors.models.ban.attributes.user_id.name'), I18n.t('.activerecord.errors.models.ban.attributes.user_id.invalid'))
    end
    if ban.banning_user.nil? || ban.banning_user.to_s.length == 0
      # Banning user's ID not present
      ban.errors.add(I18n.t('.activerecord.errors.models.ban.attributes.banning_user.name'), I18n.t('.activerecord.errors.models.ban.attributes.banning_user.blank'))
    elsif User.find_by_id(ban.banning_user).nil?
      # Banning user's ID not valid
      ban.errors.add(I18n.t('.activerecord.errors.models.ban.attributes.banning_user.name'), I18n.t('.activerecord.errors.models.ban.attributes.banning_user.invalid'))
    end
    if !ban.ban_end_time.nil? && ban.ban_end_time.to_datetime <= DateTime.now()
      # Ban ending time is less than current system time
      ban.errors.add(I18n.t('.activerecord.errors.models.ban.attributes.ban_end_time.name'), I18n.t('.activerecord.errors.models.ban.attributes.ban_end_time.before_current_time'))
    end
    if ban.permanent_ban.nil?
      # Permanent ban not specified
      ban.errors.add(I18n.t('.activerecord.errors.models.ban.attributes.permanent_ban.name'), I18n.t('.activerecord.errors.models.ban.attributes.permanent_ban.blank'))
    elsif !ban.ban_end_time.nil? && ban.ban_end_time.class == ActiveSupport::TimeWithZone && ban.permanent_ban == true
      # Ban is permanent but an ending time was specified.
      # I use == true because if I just did "&& ban.permanent_ban",
      # it would be true for ban.permanent_ban being 12, "f", etc.
      ban.errors.add(I18n.t('.activerecord.errors.models.ban.attributes.permanent_ban.name'), I18n.t('.activerecord.errors.models.ban.attributes.permanent_ban.invalid_with_timed_ban'))
    end
    if ban.ban_reversal.nil?
      # Ban reversal not specified
      ban.errors.add(I18n.t('.activerecord.errors.models.ban.attributes.ban_reversal.name'), I18n.t('.activerecord.errors.models.ban.attributes.ban_reversal.blank'))
      if ban.ban_end_time.nil? && !ban.permanent_ban
        # No ban ending time and this is not a permanent ban and it's not a ban reversal
        ban.errors.add(I18n.t('.activerecord.errors.models.ban.attributes.default.name'), I18n.t('.activerecord.errors.models.ban.attributes.default.no_time_non_reversal'))
      end
    elsif ban.ban_reversal == true && !ban.ban_end_time.nil?
      # User submitted a ban reversal with an ending time
      ban.errors.add(I18n.t('.activerecord.errors.models.ban.attributes.ban_reversal.name'), I18n.t('.activerecord.errors.models.ban.attributes.ban_reversal.invalid_with_timed_ban'))
    elsif ban.ban_reversal == true && ban.permanent_ban == true
      # User submitted a ban reversal with a permanent ban
      ban.errors.add(I18n.t('.activerecord.errors.models.ban.attributes.ban_reversal.name'), I18n.t('.activerecord.errors.models.ban.attributes.ban_reversal.invalid_with_permanent_ban'))
    end
    if ban.internal_reason.nil? || ban.internal_reason.length == 0
      # No internal reason given
      ban.errors.add(I18n.t('.activerecord.errors.models.ban.attributes.internal_reason.name'), I18n.t('.activerecord.errors.models.ban.attributes.internal_reason.blank'))
    elsif ban.internal_reason.length > Ban.max_internal_reason_length
      # Internal reason too long
      ban.errors.add(I18n.t('.activerecord.errors.models.ban.attributes.internal_reason.name'), I18n.t('.activerecord.errors.models.ban.attributes.internal_reason.invalid_length', :n => Ban.max_internal_reason_length))
      # https://stackoverflow.com/a/9223173
    end
    if ban.external_reason.nil? || ban.external_reason.length == 0
      # No external reason given
      ban.errors.add(I18n.t('.activerecord.errors.models.ban.attributes.external_reason.name'), I18n.t('.activerecord.errors.models.ban.attributes.external_reason.blank'))
    elsif ban.external_reason.length > Ban.max_external_reason_length
      # External reason too long
      ban.errors.add(I18n.t('.activerecord.errors.models.ban.attributes.external_reason.name'), I18n.t('.activerecord.errors.models.ban.attributes.external_reason.invalid_length'))
    end
  end
end
