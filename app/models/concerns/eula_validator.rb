class EulaValidator < ActiveModel::Validator
  def validate(eula)
    if eula.note.nil? || eula.note.length < Eula.min_note_length || eula.note.length > Eula.max_note_length
      eula.errors.add(I18n.t('activerecord.errors.models.eula.attributes.note.name'),
                      I18n.t('activerecord.errors.models.eula.attributes.note.invalid_length',
                             min: Eula.min_note_length, max: Eula.max_note_length))
    end

    if eula.eula_text.nil? || eula.eula_text.length < Eula.min_eula_length || eula.eula_text.length > Eula.max_eula_length
      eula.errors.add(I18n.t('activerecord.errors.models.eula.attributes.eula_text.name_agrmnt'),
                      I18n.t('activerecord.errors.models.eula.attributes.eula_text.invalid_length',
                             min: Eula.min_eula_length, max: Eula.max_eula_length))
    end

    if eula.publisher.nil? || eula.publisher.to_i.to_s != eula.publisher.to_s
      eula.errors.add(I18n.t('activerecord.errors.models.eula.attributes.publisher.name'),
                      I18n.t('activerecord.errors.models.eula.attributes.publisher.not_found'))
    end
  end
end
