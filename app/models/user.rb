require 'securerandom'

class User < ApplicationRecord
  attr_encrypted :otp_secret, key: Rails.application.secrets.devise_db_encryption_key
  devise :two_factor_authenticatable,
         :otp_secret_encryption_key => Rails.application.secrets.devise_db_encryption_key
  validates_with UserValidator

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :registerable, :omniauthable, :recoverable, :rememberable,
         :trackable, :validatable, :two_factor_backupable,
         omniauth_providers: [:discord, :facebook, :github, :gitlab, :google_oauth2, :twitter, :twitch]
  has_one :user_preference, dependent: :destroy
  has_many :bans, dependent: :nullify
  has_many :credentials, dependent: :destroy # FIDO2 U2F WebAuthn tokens
  has_many :conversations, dependent: :nullify
  has_many :messages
    # No dependent: :nullify here because each message can have 2 users - 
    # the sender and the deleter. I cannot figure out how to tell it to
    # nullify both, instead it tries to nullify user_id, which does not
    # exist (the keys are sender_id and deleter_id).
    # Instead, I set it to nullify the dependents in the migration file
    # which I used to define the Messages table, and it works fine.
    # Trying to use dependent: :nullify here causes errors when users
    # attempt to delete their accounts.
  has_many :reports, dependent: :nullify
  has_many :searchs, dependent: :nullify
    # dependent: :nullify means deleting the (parent) user doesn't force
    # deletion of (child) bans, reports, and searches.
    # https://stackoverflow.com/a/47152230
  has_many :user_oauths, dependent: :destroy
  has_many :eulas, dependent: :nullify

  accepts_nested_attributes_for :user_preference
  validates_confirmation_of :password
    # Prevents password change when "New password" != "Confirm new password"
    # field

  # Oauth accessor shortcuts
  User.omniauth_providers.each do |provider|
    define_method(:"#{provider}_oauth") do |domain = nil|
      # Metaprogramming is neat
      # https://stackoverflow.com/a/5349874
      if domain
        records = UserOauth.where(domain: domain, user_id: self.id, provider: provider)
      else
        records = UserOauth.where(user_id: self.id, provider: provider)
      end

      if records.count >= 1
        records
      else # records.count < 1
        nil
      end
    end
  end

  def self.from_omniauth(auth, domain = nil, signed_in_resource = nil)
    unless User.omniauth_providers.include?(auth.provider.to_sym)
      logger.error "Unknown oauth provider: #{auth.provider}"
    end

    if auth.provider != 'facebook'
      oauth_all_domains = UserOauth.where(provider: auth.provider,
                                          uid: auth.uid)
      oauth_this_domain = UserOauth.where(domain: domain,
                                          provider: auth.provider,
                                          uid: auth.uid)
    else
      # Facebook, being oh so special, have decided a UID is not static.
      # That is, if you log in from the same account to 2 different
      # applications, you will get different UIDs. No other service that I
      # have dealt with does this.
      # Thanks, Zuckerburg, for making me write an additional branch.
      oauth_all_domains = UserOauth.where(provider: auth.provider,
                                          email: auth.info.email)
      oauth_this_domain = UserOauth.where(domain: domain,
                                          provider: auth.provider,
                                          email: auth.info.email)
    end

    if Current.user # User is signed in
      user = Current.user
      if oauth_this_domain.none?
        # User is adding a provider that exists on another domain, OR
        # User is adding a provider that does not exist on any domains
        if oauth_all_domains.none? || (oauth_all_domains.any? &&
            oauth_all_domains.pluck(:user_id).uniq.one? &&
            oauth_all_domains.first.user_id == user.id)
          # Sanity check: it should be impossible for multiple users to add
          # the same Oauth account to different accounts on multiple domains.
          # .uniq.one? should always return true.
          # User is allowed to add this provider - either only they have
          # added it before, or nobody has ever added it before.
          # ACTION: create method
          UserOauth.create_oauth_record(auth, domain, user)
          user_status = {:signed_in => true, :can_add => true,
                         :provider => UserOauth.normalize_oauth_name(auth.provider),
                         :current_user => user}
          return user_status
        elsif (oauth_all_domains.any? &&
                oauth_all_domains.pluck(:user_id).uniq.one?)
          # User is not allowed to add this, as someone else has added
          # this method on some domain
          # ACTION: deny, provider is on another account
          user_status = {:signed_in => true, :can_add => false,
                         :provider => UserOauth.normalize_oauth_name(auth.provider),
                         :current_user => user}
          return user_status
        else
          # Sanity check failed - oauth_all_domains.pluck(:user_id).uniq.one?
          # returned false.
          # ACTION: deny, provider is on multiple accounts
          if auth.provider != 'facebook'
            logger.error "UserOauth error: multiple accounts appear to have an entry for provider=#{auth.provider}, uid=#{auth.uid}! Present on accounts id=#{oauth_all_domains.pluck(:user_id).uniq}"
          else
            logger.error "UserOauth error: multiple accounts appear to have an entry for provider=#{auth.provider}, email=#{auth.info.email}! Present on accounts id=#{oauth_all_domains.pluck(:user_id).uniq}"
          end
          # Return same status as when sanity check passes since the user
          # does not need to know about this error
          user_status = {:signed_in => true, :can_add => false,
                         :provider => UserOauth.normalize_oauth_name(auth.provider),
                         :current_user => user}
          return user_status
        end
      else
        # User is adding a provider that already exists for this domain
        # ACTION: deny, provider already added
        logger.error "User with id=#{user.id} attempted to add Oauth with provider=#{auth.provider} when it already exists for domain=#{domain}"
        user_status = {:signed_in => true, :can_add => false,
                       :provider => UserOauth.normalize_oauth_name(auth.provider),
                       :current_user => user, :present => true}
        return user_status
      end
    else # User is not signed in
      # If this account has been signed in to on another domain, I do not
      # need to perform additional checks because it is already authorized
      # for an account.
      if oauth_all_domains.any? && oauth_this_domain.one?
        # User is signing in and has done so on this domain before
        # ACTION: sign in
        user = oauth_this_domain.first.user
        return user
      elsif (oauth_all_domains.any? &&
                oauth_all_domains.pluck(:user_id).uniq.one?)
        # User is signing in with this provider for the first time on this
        # domain, and the sanity check passed
        # ACTION: create method, sign in
        user = oauth_all_domains.first.user
        UserOauth.create_oauth_record(auth, domain, user)
        return user
      elsif oauth_all_domains.any?
        # User is signing in with this provider for the first time on this
        # domain, but the sanity check failed
        # ACTION: deny, provider already added
        if auth.provider != 'facebook'
          logger.error "UserOauth error: multiple accounts appear to have an entry for provider=#{auth.provider}, uid=#{auth.uid}! Present on accounts id=#{oauth_all_domains.pluck(:user_id).uniq}"
        else
          logger.error "UserOauth error: multiple accounts appear to have an entry for provider=#{auth.provider}, email=#{auth.info.email}! Present on accounts id=#{oauth_all_domains.pluck(:user_id).uniq}"
        end
        # Return same status as when sanity check passes since the user
        # does not need to know about this error
        user_status = {:signed_in => true, :can_add => false,
                       :provider => UserOauth.normalize_oauth_name(auth.provider),
                       :current_user => user}
        return user_status
      else
        # Locate by email
        user_with_email = User.find_by_email(auth.info.email)
        if user_with_email # User found
          # User is signing in with this provider for the first time
          # ACTION: deny, you must add this method first on any domain (NO login)
          user_status = {:signed_in => false, :can_add => false,
                         :provider => UserOauth.normalize_oauth_name(auth.provider)}
          return user_status
        else # User not found
          # New user is signing in
          # ACTION: create account, create method, sign in

          # Create account
          user = User.new
          random_password = SecureRandom.hex + SecureRandom.hex
          # Generate 64 character random password because each user must have
          # a password >= 16 characters.
          # I will prevent password login since OAuth was used, but still need
          # something in the password column.
          user.password = random_password
          user.password_confirmation = random_password
          user.confirmable = false
          user.name = auth.info.name
          case auth.provider
          when "discord", "github", "gitlab", "google_oauth2", "twitch"
            user.email = auth.info.email
          when "facebook", "twitter"
            if auth.info.email.nil?
              # Facebook allows the user to not share information -
              # Assign a temp email if they decline to share it.
              # Twitter doesn't provide the email address unless you meet
              # their requirements for elevated permissions, so I put in
              # this check to assume this is not setup by default.
              if auth.provider == "facebook"
                user.email = "temp-email-fb-#{auth.uid}@facebook.example"
              else
                user.email = "temp-email-tw-#{auth.info.nickname}@twitter.example"
              end
              # I chose this format because it contains a unique element
              # (the UID) and cannot be used to sign up for Facebook or
              # Twitter beacuse @{provider}.example can never be a valid email
              # address.
              # The reason it's invalid is because it uses 1 of the 4 reserved
              # TLDs: .example, .invalid, .localhost, and .test
            else
              # I'm not entirely sure Twitter provides it in auth.info.email
              # if you have elevated permissions or if it's somewhere else.
              # If anyone uses this who has elevated permissions, please let
              # me know if this guess about its location is correct.
              user.email = auth.info.email
            end
          end
          # blah more stuff
          user.save!

          UserOauth.create_oauth_record(auth, domain, user)
          UserPreference.create!(user_id: user.id)
            # I excluded the rescue for ActiveRecord::RecordNotUnique because
            # I don't believe that should occur, but if it does, I want to see
            # the error so I can investigate further.
          # I have to include this because OmniAuth doesn't call
          # RegistrationsController.create, so the Preferences object isn't
          # created without this statement.
          return user
        end
      end
    end
  end

  def can_delete_credentials?
    credentials.size > 0
  end

  def self.delete_user(user_id: nil, first_person: true)
    user_id ||= Current.user.id
    # Preferences and Credentials should auto-delete since I use .destroy
    # instead of .delete, and Reports & Searches should nullify their user_id
    # fields.

    begin
      User.find_by_id(user_id).destroy!
    rescue ActiveRecord::InvalidForeignKey
      logger.error "Failed to delete the user with ID #{user_id}. The associated UserPreference or Credentials records likely failed to delete, or the Reports and Searches failed to nullify."
      return {:error => I18n.t('.activerecord.errors.models.user.attributes.delete_account.failed_prefs')}
    end

    if User.find_by_id(user_id) # Deletion failed
      logger.error "Failed to delete the user with ID #{user_id}, the account is likely in a glitched state. If the user retries the account deletion, it should work."
      return {:error => I18n.t('.activerecord.errors.models.user.attributes.delete_account.failed_prefs_removed')}
    end

    # Deletion succeeded
    logger.info "Deleted account for user with ID #{user_id}."
    if first_person
      return {:notice => I18n.t('.activerecord.errors.models.user.attributes.delete_account.succeeded_first_person')}
    else
      return {:notice => I18n.t('.activerecord.errors.models.user.attributes.delete_account.succeeded_third_person')}
    end
  end

  def minimum_password_length
    @minimum_password_length = User.password_length.min
  end

  def oauth_domains(provider)
    self.user_oauths.where(provider: provider).pluck(:domain)
  end

  def oauth_domains_includes(provider, included)
    # included can be:
    # a string with 1 domain
    # an array with 1 or more domains
    # Returns an array with the domains that match included
    if included.is_a? String
      self.oauth_domains(provider).select { |domain| included == domain }
    else
      self.oauth_domains(provider).select { |domain| included.index domain }
    end
  end

  def oauth_domains_except(provider, excluded)
    # excluded can be:
    # a string with 1 domain
    # an array with 1 or more domains
    # Returns an array with the domains that do not match included
    if excluded.is_a? String
      self.oauth_domains(provider).select { |domain| excluded != domain }
    else
      self.oauth_domains(provider).select { |domain| !excluded.index domain }
    end
  end

  def bans_received
    bans.size
  end

  def bans_issued(num_bans: 5)
    # Returns all bans (and unbans) issued by the user
    Ban.where(banning_user: self.id).order(created_at: :desc).limit(num_bans)
  end

  def num_users_banned
    # Returns the number of bans the user has issued
    Ban.where(banning_user: self.id, ban_reversal: false).count
  end

  def num_users_unbanned
    # Returns the number of ban reversals the user has issued
    Ban.where(banning_user: self.id, ban_reversal: true).count
  end

  def css_theme
    base_theme = user_preference.theme

    case base_theme.to_s
    when "Dark"
      :application_dark
    when "Midnight (Dark)"
      :application_midnight
    when "Minty (Light)"
      :application_minty
    when "Solarized (Dark)"
      :application_solarized
    when "Ubuntu (Light)"
      :application_ubuntu
    else
      :application
    end
  end

  def css_theme_color
    # HTML theme-color meta tag attribute
    base_theme = user_preference.theme

    case base_theme.to_s
    when "Dark"
      # Navbar color: #375a7f
      "#375a7f"
      # "#2f4d6d" was a close second
    when "Midnight (Dark)"
      # Navbar color: #2a9fd6
      "#258fc1"
    when "Minty (Light)"
      # Navbar color: #78c2ad
      "#66baa2"
    when "Solarized (Dark)"
      # Navbar color: #073642
      "#002b36"
    when "Ubuntu (Light)"
      # Navbar color: #e95420
      "#e95420"
      # "#da4816" was a close second, but too red-ish
    else
      ApplicationHelper.default_theme_color
    end
  end

  def banned_until
    self.bans.last&.ban_end_time
  end

  def is_banned?
    self.permanently_banned? || (self.banned_until && self.banned_until.utc > DateTime.now().utc)
  end

  def eula_acceptance_needed?
    self.eula_accepted_at.nil? || self.eula_accepted_at < Eula.get_latest_eula.created_at
  end

  def permanently_banned?
    self.bans.last&.permanent_ban
  end

  def two_factor_enabled?
    two_factor_otp_enabled? || two_factor_u2f_enabled?
  end

  def two_factor_otp_enabled?
    otp_required_for_login?
  end

  def two_factor_u2f_enabled?
    credentials.size > 0
  end

  def two_factor_u2f_num_tokens
    credentials.size
  end

  # For Twitter (save the session eventhough we redirect user to registration page first)
  def new_with_session(params, session)
    if session["devise.user_attributes"]
      new(session["devise.user_attributes"]) do |user|
        user.attributes = params
        user.valid?
      end
    else
      super
    end
  end

  def password_required?
    # Allows adding new SSO methods to normal accounts and SSO-only accounts,
    # and adding a password to an SSO-only account.
    # Note: changing your password on a traditional account still requires
    # entering the current password.
    return false
  end
end
