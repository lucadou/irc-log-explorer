class Eula < ApplicationRecord
  validates_with EulaValidator
  belongs_to :user, foreign_key: :user_id, optional: true
  # optional: true required in Rails 6, per https://stackoverflow.com/a/37803756

  def self.get_latest_eula
    if !Eula.last
      # NOTE: If I ever switch to UUIDs, the publisher will have to be updated! Both here and in the admin TOS views!
      if Rails.configuration.application[:eulas][:show_not_found_screen]
        Eula.new(user_id: nil, note: I18n.t('activerecord.errors.eula.none_present.note'),
                 eula_text: I18n.t('activerecord.errors.eula.none_present.eula_text'),
                 publisher: Rails.configuration.application[:common][:system_user_id]).save!
      else
        Eula.new(user_id: nil, note: I18n.t('activerecord.errors.eula.none_present.note'),
                 eula_text: '', publisher: Rails.configuration.application[:common][:system_user_id]).save!
      end
    end
    Eula.last
  end

  def is_latest?
    self.id == Eula.get_latest_eula.id
  end

  def self.min_note_length
    Rails.configuration.application[:eulas][:note][:min_length]
  end

  def self.min_eula_length
    Rails.configuration.application[:eulas][:eula_text][:min_length]
  end

  def self.max_note_length
    Rails.configuration.application[:eulas][:note][:max_length]
  end

  def self.max_eula_length
    Rails.configuration.application[:eulas][:eula_text][:max_length]
  end

  def self.search(note: nil, eula_text: nil, publisher: nil, start_time: nil, end_time: nil, limit: nil, sort: nil)
    with_note(note)
      .with_eula_text(eula_text)
      .with_publisher(publisher)
      .with_start_time(start_time)
      .with_end_time(end_time)
      .with_limit(limit)
      .with_sort(sort)
  end

  def self.validate_search(note: nil, eula_text: nil, publisher: nil, start_time: nil, end_time: nil, per_page: nil)
    errors = {}

    # Validate note
    if note.present? && ApplicationHelper.is_regex?(note) && !ApplicationHelper.pg_valid_regex?(note)
      errors[I18n.t('activerecord.errors.models.eula.attributes.note.name')] =
        I18n.t('activerecord.errors.models.eula.attributes.note.invalid_regex')
    end

    # Validate eula_text
    if eula_text.present? && ApplicationHelper.is_regex?(eula_text) && !ApplicationHelper.pg_valid_regex?(eula_text)
      errors[I18n.t('activerecord.errors.models.eula.attributes.eula_text.name_agrmnt')] =
        I18n.t('activerecord.errors.models.eula.attributes.eula_text.invalid_regex')
    end

    # Validate publisher
    if publisher.present? && publisher.to_i.to_s != publisher.to_s
      # Non-int ID; check for email address
      targets = User.where(email: publisher)
      if targets.count > 1
        errors[I18n.t('.activerecord.errors.models.eula.attributes.publisher.name')] =
          I18n.t('.activerecord.errors.models.eula.attributes.publisher.multiple_found')
      elsif targets.count < 1
        errors[I18n.t('.activerecord.errors.models.eula.attributes.publisher.name')] =
          I18n.t('.activerecord.errors.models.eula.attributes.publisher.not_found')
      end
    elsif publisher.present? && Eula.where(publisher: publisher).none?
      # Int ID not found
      errors[I18n.t('.activerecord.errors.models.eula.attributes.publisher.name')] =
        I18n.t('.activerecord.errors.models.eula.attributes.publisher.not_found')
    end

    # Validate start_time
    if start_time.present? && !(start_time.is_a?(ActiveSupport::TimeWithZone) || start_time.is_a?(DateTime) || start_time.is_a?(Time))
      # start_time is not a Time-related object; make sure it is a valid string
      begin
        start_time = ApplicationHelper.datetime_from_str(start_time)
        valid_start_date = true
      rescue ArgumentError
        errors[I18n.t('.activerecord.errors.models.eula.attributes.created_at.name_start')] =
          I18n.t('.activerecord.errors.models.eula.attributes.created_at.invalid_format')
      end
    elsif start_time.present?
      valid_start_date = true
    end

    # Validate end_time
    if end_time.present? && !(end_time.is_a?(ActiveSupport::TimeWithZone) || end_time.is_a?(DateTime) || end_time.is_a?(Time))
      # end_time is not a Time-related object; make sure it is a valid string
      begin
        end_time = ApplicationHelper.datetime_from_str(end_time)
        if valid_start_date
          if start_time >= end_time
            errors[I18n.t('.activerecord.errors.models.eula.attributes.created_at.name_end')] =
              I18n.t('.activerecord.errors.models.eula.attributes.created_at.error_end_before_start')
          end
        end
      rescue ArgumentError
        errors[I18n.t('.activerecord.errors.models.eula.attributes.created_at.name_end')] =
          I18n.t('.activerecord.errors.models.eula.attributes.created_at.invalid_format')
      end
    elsif end_time.present? && valid_start_date # Make sure start_time < end_time
      if start_time >= end_time
        errors[I18n.t('.activerecord.errors.models.eula.attributes.created_at.name_end')] =
          I18n.t('.activerecord.errors.models.eula.attributes.created_at.error_end_before_start')
      end
    end

    # Validate per_page
    if per_page.present? && (per_page.to_i.to_s != per_page.to_s || !PreferencesHelper.valid_per_page(per_page))
      # Either per_page is not an int OR is is not a valid number
      errors[I18n.t('.activerecord.errors.models.user_preference.attributes.search_results.name')] =
        I18n.t('.activerecord.errors.models.user_preference.attributes.search_results.invalid', min: PreferencesHelper.min_per_page, max: PreferencesHelper.max_per_page)
    end

    return errors
  end

  # Search helpers
  scope :with_note, proc { |note|
    # Case insensitive; regex-enabled
    if note.present?
      if ApplicationHelper.is_regex?(note) # Using regex
        where("note ~* ?", ApplicationHelper.isolate_regex(note))
      else # Not using regex
      if ApplicationHelper.is_escaped_regex?(note)
        note = ApplicationHelper.unescape_regex(note)
      end
      where("note ILIKE ?", "%#{note}%") # Case insensitive search
      end
    end
  }

  scope :with_eula_text, proc { |eula_text|
    # Case insensitive; regex-enabled
    if eula_text.present?
      if ApplicationHelper.is_regex?(eula_text) # Using regex
        where("eula_text ~* ?", ApplicationHelper.isolate_regex(eula_text))
      else # Not using regex
      if ApplicationHelper.is_escaped_regex?(eula_text)
        eula_text = ApplicationHelper.unescape_regex(eula_text)
      end
      where("eula_text ILIKE ?", "%#{eula_text}%") # Case insensitive search
      end
    end
  }

  scope :with_publisher, proc { |publisher|
    if publisher.present? # Get numerical ID if not already numerical ID
      if publisher.to_i.to_s == publisher.to_s
        where(publisher: publisher)
      else
        targets = User.where(email: publisher)
        where(publisher: targets.first.id)
        # This assumes that .validate_search was called, which will alert when
        # there is !=1 user
      end
    end
  }

  scope :with_start_time, proc { |start_time|
    if start_time.present?
      if !(start_time.is_a?(ActiveSupport::TimeWithZone) || start_time.is_a?(DateTime) || start_time.is_a?(Time))
        # Convert to a DateTime object
        start_time = ApplicationHelper.datetime_from_str(start_time)
      end
      where("created_at >= ?", start_time)
    end
  }

  scope :with_end_time, proc { |end_time|
    if end_time.present?
      if !(end_time.is_a?(ActiveSupport::TimeWithZone) || end_time.is_a?(DateTime) || end_time.is_a?(Time))
        # Convert to a DateTime object
        end_time = ApplicationHelper.datetime_from_str(end_time)
      end
      where("created_at <= ?", end_time)
    end
  }

  scope :with_sort, proc { |sort|
    if sort.present?
      order(sort)
    end
  }

  scope :with_limit, proc { |limit|
    if limit.present?
      limit(limit)
    end
  }
end
