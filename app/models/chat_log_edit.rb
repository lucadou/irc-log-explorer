class ChatLogEdit < ApplicationRecord
  belongs_to :chat_log
  belongs_to :editor, foreign_key: :editor_id, class_name: 'User', optional: true
  validates_with ChatLogEditValidator

  def self.internal_justification_min_length
    Rails.configuration.application[:chat_log_edits][:internal_justification][:min_length]
  end

  def self.internal_justification_max_length
    Rails.configuration.application[:chat_log_edits][:internal_justification][:max_length]
  end

  def self.external_justification_min_length
    Rails.configuration.application[:chat_log_edits][:external_justification][:min_length]
  end

  def self.external_justification_max_length
    Rails.configuration.application[:chat_log_edits][:external_justification][:max_length]
  end
end
