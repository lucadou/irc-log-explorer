class Current < ActiveSupport::CurrentAttributes
  attribute :user
  # This allows accessing the current user from a model.
  # It is not best practice, but given that I put oauth in the user model
  # (I've seen it this way on several tutorials so I'm assuming that is
  # good practice), I have no other option for adding/removing integrations.

  # Source: https://evilmartians.com/chronicles/rails-5-2-active-storage-and-beyond#current-everything
  # Found at: https://stackoverflow.com/a/49886703
end
