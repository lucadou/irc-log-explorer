<%= render "pages/help_title" %>

<div class="row">
  <div class="col">
    <%= render "pages/help_tabs" %>
    <div class="card border-top-0">
      <div class="card-body">
        <h4 class="card-title">Regex Help</h4>
        <p class="card-text">
          Regular expressions (sometimes known as 'regex' or 'regexp') are a powerful tool for text search.
          Some search fields support regular expressions to allow for complex searches, and this is designed to go over the basics, but it is not comprehensive.
          This application uses PostgreSQL, so regular expressions use the syntax <code>STRING ~ 'pattern'</code>, or <code>STRING ~* 'pattern'</code> if the field is not case sensitive.
        </p>
        <p class="card-text">
          If you already know how to use regular expressions, just keep in mind that PostgreSQL implements POSIX basic and extended regular expressions, plus some additional extensions, so some syntax you might be used to in other implementations will not be available in this application.
          For example, although <code>\b</code> and <code>\B</code> are implemented in GNU extended regular expressions as word/non-word boundaries, in PostgreSQL, they are interpreted as backspace and backslash, respectively, and the GNU ERE equivalents in PostgreSQL are <code>\m</code> (beginning of word), <code>\M</code> (end of word), <code>\y</code> (beginning or end of word; word boundary), and <code>\Y</code> (neither beginning nor end of word; non-word boundary).
          For more information, scroll down to the External Resources section. If you are a beginner to regular expressions, read on to learn about the basics.
        </p>
        <hr />
        <h5>Specifying Regular Expressions</h5>
        <p class="card-text">
          If you want to execute a regular expression search, you can only do so in a field that supports it.
          These fields will state that they support regular expressions, and to use them, you need to surround your query <code>/with forward slashes/</code>.
          The forward slahes will be removed, so searching <code>/^(tim(Clip|Minus|Plus)(\s)*){3,}$/</code> will be executed as <code>FIELD ~ '^(tim(Clip|Minus|Plus)(\s)*){3,}$'</code>.
        </p>
        <p class="card-text">
          If you want to execute case insensitive regex, the <code>(?i)</code> syntax is not supported.
          If a field has a case sensitive checkbox, checking it will cause the query to execute as <code>STRING ~ 'pattern'</code>.
          If a field is not marked as case sensitive (or has a case sensitive checkbox and you leave it unchecked), the query will execute as <code>STRING ~* 'pattern'</code>.
          <br />When in doubt, if a field does not explicitly say it is case sensitive (or have a checkbox to make it case sensitive), you can assume the field is case insensitive.
        </p>
        <h4><small class="text-muted">A note about escaping regex</small></h4>
        <!-- Using <h4> produces a 24px font-size, but putting a <small> inside results in 19.2px, while just <h5> is 20px, and <small> inside <h5> is 16px. A tiny difference, but a good middle-ground since <h6> is 16px, which is the same as <p>. -->
        <p class="card-text">
          If you want to search for a string surrounded by forward slashes OR a string starting with a forward slash <i>without</i> using regular expressions, escape them with backslashes like so: <code>\/no regex for me, please\/</code> (or, if you just have a forward slash at the start, like this: <code>\/no regex for me, please</code>).
          Your search will be executed as <code>FIELD ILIKE '/no regex for me, please/'</code> (or <code>FIELD LIKE</code> if it is case sensitive) instead.
          Forward slashes that are not at the very start or end do not need to be escaped.
        </p>
        <p class="card-text">
          If you just want to search with one or more forward slashes that are anywhere except the front of the string (e.g. <code>no /regex for me, please/</code>), there is no need to escape them.
        </p>
        <hr />
        <h5>Basic Text Matching</h5>
        <p class="card-text">
          For the sake of simplicity and being more faithful to how most regex implementations handle case sensitivity, this guide will be written assuming you are searching with case sensitive regex.
          How the results may differ for a case-insensitive regex query is left as a thought exercise for the reader.
        </p>
        <p class="card-text">
          Let's say you want to search for any line containing the word "fox".
          How would you go about doing this with regular expressions?
          The simplest way would be to just search for <code>fox</code>.
          Let's see how that would turn out with some sample input (note that the specific part where the regex engine matched the string is highlighted):
        </p>
        <%= markdown("| Original String | Matches? | Match Location |\n|-----------------------------------------------|----------|--------------------------------------------------|\n| The quick brown fox jumped over the lazy dog | Yes | The quick brown ==fox== jumped over the lazy dog |\n| The slow blue fox did not clear the dog | Yes | The slow blue ==fox== did not clear the dog |\n| foxfox afoxy grandpa | Yes | ==fox====fox== a==fox==y grandpa |\n| The quick brown faux jumped over the lazy dog | No |  |\n| The quick brown FOX jumped over the lazy dog | No |  |") %>
        <p class="card-text">
          Notice how a phrase can be matched multiple times in a single string, even when the text you are looking for is surrounded by other letters.
          If you wanted to match just the "fox" and not "afoxy" or "foxfox", you could use the non-word character class (or a variety of other methods), but that will be covered later.
        </p>
        <hr />
        <h5>The OR Operator - | and []</h5>
        <p class="card-text">
          If you have multiple words that you want to search for and they are all very similar, [brackets] allow you to easily match them all.
          The regex engine will look for 1 character matching in the brackets, so if you want to search for the words "hit", "lit", and "wit", you can search for <code>[hlw]it</code>.
          Note that the characters are de-duplicated, so <code>[hhlwlw]it</code> will be executed the same as <code>[hlw]it</code>.
        </p>
        <%= markdown("| Original String | Matches? | Match Location |\n|----------------------------------------|----------|--------------------------------|\n| He hit the ball | Yes | He ==hit== the ball |\n| They made a witty comeback | Yes | They made a ==wit==ty comeback |\n| hilitiwittil | Yes | hi==lit==i==wit==til |\n| He threw a fit when he lost his mit | No |  |\n| Literally nothing could change my mind | No |  |") %>
        <p class="card-text">
          You can use this with any set of characters - letters, numbers, spaces, etc.
          (although some chracters need to be escaped or reordered, e.g. <code>[hl-w]it</code> will match "h", and any letter from "l" through "w" (but not "-it", as might be expected from the "-"),
          so "mit" is matched since "m" falls between "l" and "w" - you can either rearrange it as <code>[hlw-]it</code> or escape it like <code>[hl\-w]it</code> - but this will be covered in more depth later on).
        </p>
        <p class="card-text">
          This works fine for very basic, specific matches, but what about matching more than 1 character at a time? For that, we can use (parenthesis) and | (the "vertical bar" or "pipe").
        </p>
        <p class="card-text">
          In regular expressions, (parenthesis) create a capture group.
          That might sound complex, but all you need to know is that it contains multiple "groups" of characters, separated by |pipes|, which looks for an exact match with one of them.
          While <code>[hlw]it</code> is functionally equivalent to <code>(h|l|w)it</code>, the reason to use (|) over [] is because (|) matches more than 1 character.
          What if you wanted to search for the words "looking", "looker", "looked", "lookup", and "lookout"?
          There is no good way to do this using [brackets].
          Instead, we can do <code>look(ing|er|ed|up|out)</code>, and the engine will only match strings that contain "look" directly followed by "ing", "er", "ed", "up", or "out":
        </p>
        <%= markdown("| Original String | Matches? | Match Location |\n|--------------------------------------------------------------------------------|----------|----------------------------------------------------------------------------------------|\n| She looked up the definition | Yes | She ==look**ed**== up the definition |\n| After launching their online student lookup, student directory sales plummeted | Yes | After launching their online student ==look**up**==, student directory sales plummeted |\n| lookouting | Yes | ==look**out**==ing |\n| Looking up from his desk, he saw an angry customer approaching | No |  |\n| Be on the look-out for an escaped convict | No |  |") %>
        <p class="card-text">
          In addition to the usual highlight, the specific text that was matched from within the (parenthesis) was bolded for extra emphasis.
          You can combine (|) and [] like <code>look(ing|e[rd])</code> to match "looking", "looker", and "looked", but you cannot do this in reverse - that is,
          <code>look[(ing)s]</code> will not just match "looking" or "looks" - it will match "look(", "looki", "lookn", "lookg", "look)",
          and "looks" (and although it matches "looking", that's because of "looki", e.g. "lookiry" would also be matched, which is probably not what you intended).
        </p>
        <hr />
        <h5>Character Classes/Meta Sequences (and Negations)</h5>
        <p class="card-text">
          What if you wanted to match just a number?
          Or just a letter?
          Or neither?
          With character classes and meta sequences, you can do just that.
          Character classes refer to ranges of characters, such as <code>[a-z]</code> and <code>[0-9]</code>, while meta sequences refer to things like <code>\w \W \d \D \s \S</code> etc.
          These will be discussed in greater detail in a bit.
        </p>
        <p class="card-text">
          You can also negate character classes by using <code>^</code>, such as <code>[^hlw]it</code>, which will match "it", "kit", "mit", "nit", etc. but will never match "hit", "lit", or "wit".
          Negating capture groups are more complex, however - let's say you are looking for any string except ones containing "day".
          One might imagine you could just do <code>(^day)</code> since all it takes to match a string containing "day" is <code>(day)</code>.
          However, <code>(^day)</code> will actually only match a string that contains "^day", like "today is a good ^day".
          If you were to try <code>^(day)</code>, the only strings it can match are those who start with "day", it does not negate it.
          Instead, you would need to use <code>^((?!(day)).)*$</code>.
          What even is this, you ask?
          Well, <code>(?!(&lt;sequence&gt;))</code> is a negative lookahead asserting that the enclosed <code>&lt;sequence&gt;</code> (<code>day</code>) does not match.
          <code>(&lt;sequence&gt;.)*</code> creates a group to match the given <code>&lt;sequence&gt;</code>, followed by any character, and it checks for the sequence (<code>&lt;sequence&gt;</code> + <code>.</code>) to exist between zero and unlimited times.
          The <code>^&lt;sequence&gt;$</code> are also important, as it asserts the <code>&lt;sequence&gt;</code> must match from the start of the string through the end of the string (<code>^</code> and <code>$</code> will be covered in the Quantifiers section).
          <br />Back to meta sequences, here's a handy reference:
        </p>
        <%= markdown("| Meta Sequence | Description | What it matches |\n|---------------|--------------------------|-------------------------------------------------------------------------------------|\n| \\s | Whitespace character | Spaces (\" \"), tabs (\"\\t\"), newlines (\"\\n\") |\n| \\S | Non-whitespace character | Anything that is not \" \", \"\\t\", or \"\\n\" |\n| \\d | Digit | 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 (or, as a character class, `[0-9]`) |\n| \\D | Non-digit characters | Anything that is not one of: 0123456789 (alternatively, `[^0-9]`) |\n| \\w | Word character | Letters (`[a-zA-Z]`), digits (`[0-9]`), and underscores (\"_\") |\n| \\W | Non-word character | Anything not a letter, digit, or underscore (`[^a-zA-Z0-9_]`) |\n| . | Any character | Everything |") %>
        <p class="card-text">
          These can be combined in a variety of different ways.
          For example, if you want to search for dates in the format YYYY-MM-DD, you could use meta sequences like <code>\d\d\d\d-\d\d-\d\d</code>, but if you wanted to be more specific, you could use character ranges like <code>[1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]</code>:
        </p>
        <%= markdown("| Original String | Match for `\\d\\d\\d\\d-\\d\\d-\\d\\d`? | Match for `[1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]`? |\n|-----------------|-----------------------------------------|---------------------------------------------------------|\n| 2019-12-04 | Yes | Yes |\n| Today is2020-04-15! | Yes | Yes |\n| 0090-10-99 | Yes | No |\n| 1280-20-11 | Yes | No |\n| 2k20-04-15 | No | No |") %>
        <p class="card-text">
          You can also combine these with capture groups.
          For example, if you wanted to search for a string that ended in either two digits (the first of which must be between 3 and 5) or a word character followed by an uppercase letter, you could use <code>([3-5]\d|\w[A-Z])$</code>:
        </p>
        <%= markdown("| Original String | Matches? | Match Location |\n|---------------------------------------------------------------|----------|-------------------------|\n| 2019-12-30 | Yes | 2019-12-==30== |\n| I'm going to the ER | Yes | I'm going to the ==ER== |\n| hello wor1D | Yes | hello wor==1D== |\n| I prefer movies when they're in 3d | No |  |\n| Hey Patrick, I just thought of something funnier than 24...25 | No |  |") %>
        <p class="card-text">
          Character classes and meta sequences are powerful, but negation can be tricky if you're not using character classes.
          When combined with Quantifiers (covered in the next section), they become far more useful and flexible.
        </p>
        <hr />
        <h5>Quantifiers - ?, *, +, and {}</h5>
        <p class="card-text">
          Let's say you want to match a string based on a word repeating.
          The most straightforward method would be to just type out the word however many times, but this is very limited.
          You might want a minimum number of repeats or a maximum.
          If you wanted to search for strings that had between 2 and 5 occurences of an individual character, a character class, or a meta sequence, you can use <code>{a,b}</code> notation,
          e.g. <code>((Alpha|Beta|Charlie)\s?){2,5}</code> will search for strings with between 2 and 5 occurences (inclusive) of the words "Alpha", "Beta", or "Charlie" (<code>(Alpha|Beta|Charlie)</code>), separated by zero to one whitespace characters (<code>\s?</code>).
          You can also specify just a minimum (e.g. <code>((Alpha|Beta|Charlie)\s?){2,}</code> searches for 2 to infinity occurences of the words) or an exact number (e.g. <code>((Alpha|Beta|Charlie)\s?){2}</code> searches for exactly 2 occurences of the words).
        </p>
        <p class="card-text">
          But there are quantifiers besides <code>{}</code>, including one used above (<code>?</code>).
          A short summary of these quantifiers:
        </p>
        <%= markdown("| Quantifier | Range | Equivalent `{a,b}` Notation |\n|------------|-----------|--------------------------|\n| ? | 0 or 1 | `{0,1}` |\n| \* | 0 or more | `{0,}` |\n| + | 1 or more | `{1,}` |") %>
        <p class="card-text">
          How matches actually form can be interesting, as shown by the execution of <code>((Kappa|Keepo)\s?){2,3}</code>:
        </p>
        <%= markdown("| Original String | Matches? | Match Location |\n|-------------------------------------------|----------|-------------------------------------------------------|\n| Kappa KappaKappaKeepo Kappa KappaKappa | Yes | **Kappa KappaKappa***Keepo Kappa Kappa*Kappa |\n| Kappa KappaKappaKeepo KappaKap KappaKappa | Yes | **Kappa KappaKappa***Keepo Kappa*Kap **KappaKappa** |\n| Kappa keepo Keepo Keep. | No |  |") %>
        <p class="card-text">
          To show the different captured text, bold/italics alternating is used.
          The first thing you might be wondering is why the first one was matched when there are clearly more than 2-3 occurences of the words "Kappa" and "Keepo"?
          Well, after finding the first group of text (in bold), it still has leftover text to search through, so it keeps searching and finding matches.
          To prevent this, you can use an anchor (covered in the Anchors section later on) or attempt to use a negative lookahead assertion like <code>((Kappa|Keepo)\s?){2,3}((?!(Kappa|Keepo)).)*$</code>.
          However, the negative lookahead still falls flat, as "Keepo Kappa KappaKappa" is still valid - it just matches "Keepo <b>Kappa KappaKappa</b>" - that is, although "Keepo Kappa Kappa" still has another "Kappa" after it, "Kappa KappaKappa" does not have anything after it, which is technically all you asked for!
          If you try using a second negative lookahead, like <code>((?!(Kappa|Keepo)).)*((Kappa|Keepo)\s?){2,3}((?!(Kappa|Keepo)).)*$</code>, it still will not work - "Keepo Kappa KappaKappa" matches "K<b>eepo Kappa KappaKappa</b>".
          Technically, "eepo" is neither "Kappa" nor "Keepo", making it a valid match!
          This might seem ridiculous, but it helps to demonstrate how difficult regexes can be to build - the engine is completely literal in its execution, which can lead to unexpected results.
        </p>
        <p class="card-text">
          Now, let's demonstrate the other quantifiers using <code>[yY][eE]+[st]?\s(yes)*</code>:
        </p>
        <%= markdown("| Original String | Matches? | Match Location |\n|-----------------|----------|-----------------|\n| yYes yesyes | Yes | y**Yes yesyes** |\n| yet yessss | Yes | **yet yes**sss |\n| yeet yes | Yes | **yeet yes** |\n| yee yes | Yes | **yee yes** |\n| \"yeeee YESYyee \" | Yes | \"_yeeee&#32;_YESY_yee&#32;_\" |\n| eest yes | No |  |\n| yt yes | No |  |") %>
        <p class="card-text">
          The first and second matches are fairly easy to understand.
          The third match uses more than 1 of <code>[eE]</code>, as the <code>+</code> allows for 1 to infinity occurences,
          and the fourth match does not include one of <code>[st]</code>, which is fine since <code>?</code> looks for 0 or 1 occurence.
        </p>
        <p class="card-text">
          The fifth string uses underlines to denote the whitespace being captured since you cannot bold it.
          It might seem odd that this is a match, but "yeeee " and "yee " are fairly simple matches for <code>[yY][eE]+[st]?\s</code>,
          and because <code>(yes)*</code> has a <code>*</code>, it looks for 0 or more occurences of it - its presence is optional.
        </p>
        <p class="card-text">
          The sixth string is not a match because even though it matches <code>(yes)*</code>, it does not match the preceeding <code>[yY][eE]+[st]?\s</code>.
          Adding a space or linebreak would allow it to match the first part, which is all that is necessary since the second group is optional.
          The seventh string is not a match because it fails to match <code>[eE]+</code> (1 or more occurences of "e" or "E").
          Adding an "e" or changing <code>[eE]+</code> to <code>[eE]*</code> would cause it to match.
        </p>
        <p class="card-text">
          You might be wondering how you can use ?, +, and * without having them be treated as quantifiers, e.g. "2+2" or "A* search algorithm".
          That will be covered in the Escaping Characters section.
        </p>
        <hr />
        <h5>Word Boundaries</h5>
        <p class="card-text">
          We have seen how you can inadvertantly match parts of words, e.g. <code>(yes)*</code> will match "yjasdf_l34f8jfsd<b>yes</b>sdfj34sdj!9", but what if you want to only match a specific word exactly?
          For example, if you wanted to search for strings containing "yes" as a standalone word only (that is, "<b>yes</b> I can" will match, but "<b>yes</b>terday" will not match), how would you do that?
          With word boundaries, you can ensure a match will only occur when your selection is not a part of a larger word.
        </p>
        <p class="card-text">
          For those already familiar with word boundaries, I recommend looking at the PostgreSQL
          <a href="https://www.postgresql.org/docs/current/functions-matching.html#POSIX-CHARACTER-ENTRY-ESCAPES-TABLE">Regular Expression Character-Entry Escapes</a> and
          <a href="https://www.postgresql.org/docs/current/functions-matching.html#POSIX-CONSTRAINT-ESCAPES-TABLE">Regular Expression Constraint Escapes</a> documentation,
          as <code>\b</code> and <code>\B</code> do not behave the way you might expect if you are familiar with the implementations of GNU BRE/ERE (awk, grep, etc.), Java, PCRE (PHP, Perl, etc.), Python, Ruby, etc.
          Alternatively, just see the table below.
        </p>
        <p class="card-text">
          The various word boundaries are as follows:
        </p>
        <%= markdown("| Word Boundary | Description | Sample Regex | How it Matches |\n|---------------|--------------------------------------------|--------------|------------------------------------------------------------|\n| \\m | Start of word boundary | `\\mday` | What **day** is today? The **day**of the dead |\n| \\M | End of word boundary | `day\\M` | What **day** is to**day**? The dayof the dead |\n| \\y | Beginning or end of word boundary | `\\yday\\y` | What **day** is today? The dayof the dead |\n| \\y | Neither beginning nor end of word boundary | `\\yday\\y` | What day is to**day**s date? Is it the dayof the dead yet? |") %>
        <p class="card-text">
          You can search for anything in a word boundary - you can use spaces, character classes, etc. and ensure it is not part of a larger word.
          So, what constitutes a word boundary?
          A word boundary is any non-word character (that is, the <code>\W</code> character class), so as long as it's not a letter, number, or underscore, it is a word boundary.
          This means spaces, line breaks, start/end of a string, punctuation, etc. all count.
        </p>
        <p class="card-text">
          If you want to search for just "yes", e.g. you want to match "<b>yes</b> sir", but do not want to match "<b>yes</b>terday",
          you could search for <code>\myes\M</code>, <code>\yyes\y</code>, <code>\yyes\M</code>, or <code>\myes\y</code>.
          But it doesn't have to be a single, isolated word - you can combine what we have used previously, such as <code>\mthis\s?was\M</code>:
        </p>
        <%= markdown("| Original String | Matches? | Match Location |\n|----------------------------------|----------|-------------------------------|\n| Pretty sure this was true | Yes | Pretty sure **this was** true |\n| Pretty sure thiswas true | Yes | Pretty sure **thiswas** true |\n| Pretty sure thatthis was true | No |  |\n| Pretty sure thiswasn't true | No |  |\n| Pretty sure thatthis wasn't true | No |  |") %>
        <p class="card-text">
          The third string does not match because "this" is not the start of a word - it is a part of "thatthis".
          The fourth string does not match because "was" is not at the end of a word boundary - it is part of "wasn't".
          The fifth string does not match because both "this" is not at the start of a word (it is a part of "thatthis") and because "was" is not at the end of a word boundary (it is a part of "wasn't").
          Now that you know how to isolate a word, you might want to know how you can search for just "\m" or another regex term literally (e.g. to find a string "hello\world"). This will be covered in the next section, Escaping Characters.
        </p>
        <hr />
        <h5>Escaping Characters</h5>
        <p class="card-text">
          If you want to search for a string like "5*9" or "hello\world", you cannot just search <code>5*9</code>, as the regex engine would interpret that as 0 to infinity 5's, followed by a 9.
          Instead, you have to escape special characters.
          There are 14 common special characters you will need to be diligent about escaping:
          <code>\ | [ ] ( ) . ? * + { } ^ $</code>.
          This is typically done with a backslash, e.g. if you wanted to search for the string "hello\world? I have $20.42!", you would need to use <code>hello\\world\? I have \$20\.42!</code>.
        </p>
        <p class="card-text">
          However, there are some special cases to be aware of.
          Some of the more common ways you can get yourself into trouble are detailed below:
        </p>
        <%= markdown("| Regex String | What it Does |\n|---------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------|\n| `[hlw]` | Matches 1 character of \"h\", \"l\", or \"w\" |\n| `[\^hlw]` | Matches any character that is not one of \"h\", \"l\", or \"w\" |\n| `[\\\^hlw]` | Matches 1 character of \"\\^\", \"h\", \"l\", or \"w\" |\n| `[hlw\^]` | Matches 1 character of \"h\", \"l\", \"w\", or \"\\^\" |\n| `[hlw-] or [-hlw]` | Matches 1 character of \"h\", \"l\", \"w\", or \"-\" |\n| `[hlw.-]` | Matches 1 character of \"h\", \"l\", \"w\", \".\", or \"-\" |\n| `[hlw.-\^]` | Matches 1 character of \"h\", \"l\", \"w\", or a character between \".\" and \"\\^\" (ASCII values 46-94; [more info on ASCII values](http://www.asciitable.com/)) |\n| `[hlw.\\-\^]` | Matches 1 character of \"h\", \"l\", \"w\", \".\", \"-\", or \"\\^\" |\n| `\\[\\\\d\\}{3}\\]` | Matches a \"[\", followed by \"\\\", \"d\", 3 instances of \"}\", and a \"]\" (that is, it matches the string \"[\\d}}}]\") |") %>
        <p class="card-text">
          There are a lot of potential special cases not covered here, but if you are using any of the 14 common special characters (or are using a "-" inside a character class),
          you might want to open a regex debugger (such as Regex101, linked in the External Resources section) to make sure you are not going to mess things up.
          Next, we are going to talk about Anchors, a way to assert the position of a sequence at the start or end of the string.
        </p>
        <hr />
        <h5>Anchors - \A, \Z, ^, and $</h5>
        <p class="card-text">
          If you want to search for a string that starts or ends in a specific sequence of characters, such as a string that ends in "ing", you use anchors.
          There are 4 anchors available:
        </p>
        <%= markdown("| Anchor | What it Matches |\n|--------|---------------------|\n| \^ | Start of the string |\n| $ | End of the string |\n| \A | Start of the string |\n| \Z | End of the string |") %>
        <p class="card-text">
          Word boundaries are similar to anchors, and some sources will group them together, but this page separates them since they are used for differently.
        </p>
        <p class="card-text">
          Using anchors is very simple - if you want to assert that something is at the start of the string, use <code>^&lt;expression&gt;</code> or <code>\A&lt;expression&gt;</code>,
          and if you want to assert that something is at the end of the string, use <code>&lt;expression&gt;$</code> or <code>&lt;expression&gt;\Z</code>.
          You can also combine them, like <code>^I.*dog$</code> (or <code>\AI.*dog\Z</code>):
        </p>
        <%= markdown("| Original String | Matches? | Match Location |\n|-------------------------|----------|-----------------------------------|\n| I got a new dog | Yes | **I**_&#32;got a new&#32;_**dog** |\n| I ate the hotdog | Yes | **I**_&#32;ate the hot_**dog** |\n| I fed the dog my hotdog | Yes | **I**_&#32;fed the dog my hot_**dog** |\n| I'm getting a new dog | Yes | **I**_'m getting a new&#32;_**dog** |\n| i pet the dog | No |  |\n| I pet the dogs | No |  |") %>
        <p class="card-text">
          The start and end matches are bolded, the middle matches are underlined since they are not as important (if you recall, <code>.*</code> will match any characters an infinite number of times).
          The first 4 are matches because they all start with "I" and end with "dog".
          The fifth is not a match because the "i" at the start is not capitalized, and the sixth is not a match because it ends with "dogs", which is not the same as "dog".
        </p>
        <p class="card-text">
          You might be wondering if there is any difference between <code>\A</code> and <code>^</code>, and <code>\Z</code> and <code>$</code>.
          This is really a technicality, as under some configurations, <code>^</code> and <code>$</code> will not refer to newline characters
          (i.e. <code>^test$</code> could match "\n<b>test</b>\n" as <code>^</code> and <code>$</code> would refer to "\n<b>t</b>es<b>t</b>\n"),
          but still remain able to match the start/end of a string if there was no newline characters (or if you overrode it).
          This is the case <i>if and only if</i> you enable newline-sensitive matching or inverse partial newline-sensitive matching
          (the difference being that inverse partial newline-sensitive matching also changes the behavior of <code>.</code> to never refer to a newline character).
          However, this application uses the PostgreSQL default (non-newline-sensitive matching), so <code>\A</code> is executed the same as <code>^</code>,
          and <code>\Z</code> the same as <code>$</code> (i.e. <code>^test$</code> will <i>not</i> match "\n<b>test</b>\n").
          You can read more about this in PostgreSQL's <a href="https://www.postgresql.org/docs/current/functions-matching.html#POSIX-MATCHING-RULES">Regular Expression Matching Rules</a> and <a href="https://www.postgresql.org/docs/current/functions-matching.html#POSIX-METASYNTAX">Regular Expression Metasyntax</a> documentation.
          <br />TL;DR - There is no real difference between <code>\A</code> and <code>^</code>, and <code>\Z</code> and <code>$</code>.
        </p>
        <hr />
        <h5>So, what's the catch?</h5>
        <p class="card-text">
          If you've been read this all the way through, you're probably beginning to grasp the power of regular expressions.
          This guide was very basic, covering only a very small set of operators available in PostgreSQL, but it helps to demonstrate the information density and potential of regular expressions.
          Take <code>^\d{3,}\D+(?=[a-z])[^aeiuo]$</code>, for example - with just 24 characters, we have told the regex engine to only match a string that starts with at least 3 numbers (<code>^\d{3,}</code>),
          followed by at least 1 non-numeric character (<code>\D+</code>) until it reaches the last character (<code>$</code>),
          which must be a lowercase character (<code>(?=[a-z])</code>) AND that last character cannot be a vowel (<code>[^aeiuo]</code>).
        </p>
        <p class="card-text">
          Regular expressions can become very complex - see <a href="https://emailregex.com/">what it takes to validate email addresses</a>.
          With all the complex patterns you can build regular expressions around, there are plenty of footguns - they can be difficult to debug, maintain, and reason about,
          and you can also destroy system performance through <a href="https://www.regular-expressions.info/catastrophic.html">catastrophic backtracking</a>.
          PostgreSQL seems to do a good job avoiding catastrophic backtracking, but all queries and their execution times are logged just in case it happens.
        </p>
        <hr />
        <h5>External Resources</h5>
        <p class="card-text">
          Basic regular expression information:
        </p>
        <ul>
          <li><a href="https://www.regular-expressions.info/tutorial.html">Regular Expressions Tutorial</a></li>
          <li><a href="https://regexone.com/">RegexOne - Learn Regular Expressions</a></li>
          <li><a href="https://blog.usejournal.com/regular-expressions-a-complete-beginners-tutorial-c7327b9fd8eb">Regular Expressions | A Complete Beginners Tutorial</a></li>
          <li><a href="https://medium.com/factory-mind/regex-tutorial-a-simple-cheatsheet-by-examples-649dc1c3f285">Regex tutorial — A quick cheatsheet by examples</a></li>
          <li><a href="https://regex101.com/">Regex101 - Online regex tester and debugger</a> (very useful)</li>
        </ul>
        <p class="card-text">
          Some more advanced resources specifically about POSIX regular expressions and their implementation in PostgreSQL:
        </p>
        <ul>
          <li><a href="https://www.postgresql.org/docs/current/functions-matching.html#FUNCTIONS-POSIX-REGEXP">PostgreSQL Documentation - Pattern Matching - POSIX Regular Expressions</a></li>
          <li><a href="https://www.postgresql.org/docs/current/functions-matching.html#POSIX-SYNTAX-DETAILS">PostgreSQL Documentation - Pattern Matching - Regular Expressions Details</a></li>
          <li><a href="https://www.regular-expressions.info/posix.html">Regular Expressions - POSIX Basic Regular Expressions</a></li>
          <li><a href="https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap09.html">Open Group POSIX Regular Expressions Manpage (2018 Edition)</a></li>
          <li><a href="https://www.boost.org/doc/libs/1_38_0/libs/regex/doc/html/boost_regex/syntax/basic_extended.html">POSIX Extended Regular Expressions Syntax</a></li>
          <li><a href="http://man7.org/linux/man-pages/man7/regex.7.html">Manpage of regex(7)</a></li>
          <li><a href="https://www.gnu.org/software/findutils/manual/html_node/find_html/posix_002dextended-regular-expression-syntax.html">GNU POSIX-Extended Regular Expression Syntax</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
