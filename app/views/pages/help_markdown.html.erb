<%= render "pages/help_title" %>

<div class="row">
  <div class="col">
    <%= render "pages/help_tabs" %>
    <div class="card border-top-0">
      <div class="card-body">
        <h4 class="card-title">Markdown Help</h4>
        <p class="card-text">
          Markdown is a very simple, common, and lightweight plaintext markup language for basic text styling that is mainly used in the admin messaging system on this site.
          When using the message system, you can use some Markdown features, plus some additional features.
          Not all Markdown syntax is available (see the Limitations section for more information on what is not included), but what is available should be more than enough for the areas of this application that use it.
        </p>
        <hr />
        <h5>Line Breaks</h5>
        <p class="card-text">
          The most common people run into when using Markdown for the first time is not being able to put things on a separate line.
          In Markdown, there are 2 common ways to do this:
        </p>
        <ul>
          <li>Typing two line breaks (enter key):
            <pre class="codeblock-fenced">This will be in

a separate paragraph</pre>
            <%= markdown("This will be in\n\na separate paragraph") %>
          </li>
          <li>Or typing 2 spaces and a single line-break:
            <pre class="codeblock-fenced">This will be in  
the same paragraph</pre>
            <%= markdown("This will be in  \nthe same paragraph") %>
          </li>
        </ul>
        <hr />
        <h5>Italics, Bold, and Underlined Text</h5>
        <p class="card-text">
          Applying italics, bold, and underlines to text in Markdown is simple, but the Markdown used on this site might be slightly different than what other sites use.
        </p>
        <ul>
          <li>For italics, surround your text with single asterisks:
            <pre class="codeblock-fenced">Munroe, Randall. *What If?: Serious Scientific Answers to Absurd Hypothetical Questions.* Houghton Mifflin Harcourt, 2014.</pre>
            <%= markdown("Munroe, Randall. *What If?: Serious Scientific Answers to Absurd Hypothetical Questions.* Houghton Mifflin Harcourt, 2014.") %>
          </li>
          <li>For bold, surround your text with 2 asterisks:
            <pre class="codeblock-fenced">**GET OFF MY LAWN YOU CRAZY KIDS**</pre>
            <%= markdown("**GET OFF MY LAWN YOU CRAZY KIDS**") %>
          </li>
          <li>For underlines, surround your text with single underscores (the <code>++underline++</code> syntax does not work due to limitations of the RedCarpet gem):
            <pre class="codeblock-fenced">_Everyone knows you should underline everything if you want it to seem important!_</pre>
            <%= markdown("_Everyone knows you should underline everything if you want it to seem important!_") %>
          </li>
          <li>You can combine these in many ways, but it can be tricky to get it right:
              <pre class="codeblock-fenced">Has **Anyone** ***Really*** _Been ***Far***_ Even as Decided to Use *Even _Go_* Want to do ***Look _More Like_?***</pre>
            <%= markdown("Has **Anyone** ***Really*** _Been ***Far***_ Even as Decided to Use *Even _Go_* Want to do ***Look _More Like_?***") %>
          </li>
        </ul>
        <hr />
        <h5>Quoting</h5>
        <p class="card-text">
          Quoting people or thing is elegant and easy in Markdown.
        </p>
        <ul>
          <li>Simple quotes can be done by prefacing a line with an &gt; character:
            <pre class="codeblock-fenced">>Who are you quoting?</pre>
            <%= markdown(">Who are you quoting?") %>
          </li>
          <li>You can also use standard Markdown syntax inside a quote:
            <pre class="codeblock-fenced">> 90% of quotes on the internet are made up.

> *— Albert Einstein*</pre>
            <%= markdown("> 90% of quotes on the internet are made up.\n\n> *— Albert Einstein*") %>
          </li>
          <li>You can even nest quotes:
            <pre class="codeblock-fenced">>>What if we're all in a giant simulation?

>no u</pre>
            <%= markdown(">>What if we're all in a giant simulation?\n\n>no u") %>
          </li>
        </ul>
        <hr />
        <h5>Links</h5>
        <p class="card-text">
          Embedding links is easy, but there are some things you need to watch out for.
        </p>
        <ul>
          <li>If you want to send just a link, anything starting with <code>http://</code>, <code>https://</code>, <code>ftp://</code>, or <code>www.</code> will automatically be converted into a clickable link. However, non-www domains without <code>http(s)://</code> will not be converted into URLs:
            <pre class="codeblock-fenced">ftp://ftp.idsoftware.com https://google.com http://google.com https://www.google.com http://www.google.com google.com www.google.com internal.google.com</pre>
            <%= markdown("ftp://ftp.idsoftware.com https://google.com http://google.com https://www.google.com http://www.google.com google.com www.google.com internal.google.com") %>
          </li>
          <li>In addition, email addresses are also automatically recognized and give a <code>mailto:</code> link:
            <pre class="codeblock-fenced">someperson@domain.test</pre>
            <%= markdown("someperson@domain.test") %>
          </li>
          <li>If you want to embed a link inside text, the syntax is fairly easy:
            <pre class="codeblock-fenced">[The best website ever](https://spacejam.com/)</pre>
            <%= markdown("[The best website ever](https://spacejam.com/)") %>
          </li>
          <li>Unlike some Markdown parsers, you do not need to escape closing parenthesis:
            <pre class="codeblock-fenced">[Wikipedia links just got a lot more reliable](https://en.wikipedia.org/wiki/James_R._O%27Neill_(correspondent))</pre>
            <%= markdown("[Wikipedia links just got a lot more reliable](https://en.wikipedia.org/wiki/James_R._O%27Neill_(correspondent))") %>
          </li>
          <li>You can also use reference-style and link-text links:
            <pre class="codeblock-fenced">[A reference-style link][arbitrary CaSe-Insensitive reference]

[Link-text link - also cAsE iNsensitive]

[arbitrary case-insensitive reference]: https://luna.lucadou.sh/
[Link-text link - also case insensitive]: https://gitlab.com/lucadou/irc-log-explorer</pre>
            <%= markdown("[A reference-style link][arbitrary CaSe-Insensitive reference]\n\n[Link-text link - also cAsE iNsensitive]\n\n[arbitrary case-insensitive reference]: https://luna.lucadou.sh/\n[Link-text link - also case insensitive]: https://gitlab.com/lucadou/irc-log-explorer") %>
          </li>
        </ul>
        <hr />
        <h5>Headings</h5>
        <p class="card-text">
          If you want to create headings, it's easy! Just add one or more pound signs (<code>#</code>) at the start of the line.
        </p>
        <ul>
          <li>There are 6 heading levels that correspond with <code>&lt;h1&gt;</code> through <code>&lt;h6&gt;</code>:
            <pre class="codeblock-fenced"># H1  
## H2  
### H3 
#### H4  
##### H5  
###### H6</pre>
            <%= markdown("# H1  \n## H2  \n### H3  \n#### H4  \n##### H5  \n###### H6") %>
          </li>
          <li>In addition, you can use the alternate syntax for <code>&lt;h1&gt;</code> and <code>&lt;h2&gt;</code> (3 or more <code>=</code> or <code>-</code>'s on the following line for H1 and H2, respectively):
            <pre class="codeblock-fenced">H1  
===  
H2  
---</pre>
            <%= markdown("H1  \n===  \nH2  \n---") %>
          </li>
          <li>However, you <i>have</i> to include the space between the <code>#</code> and the text or you will end up getting regular text, which could be useful if you want to use a hashtag:
            <pre class="codeblock-fenced">#Regular text</pre>
            <%= markdown("#Regular text") %>
          </li>
          <li>If you want to put a <code># </code> without adjusting the text size, you need to start the line using a space (the space should be hidden by your browser so it won't look out of alignment):
            <pre class="codeblock-fenced"> # Regular size text</pre>
            <%= markdown(" # Regular size text") %>
          </li>
          <li>Just as with quotes, you can easily insert other Markdown formatting into headers:
            <pre class="codeblock-fenced">### *Looks* **_fancy_**, [doesn't it](https://www.youtube.com/watch?v=pWRGpLmJ7ms)?</pre>
            <%= markdown("### *Looks* **_fancy_**, [doesn't it](https://www.youtube.com/watch?v=pWRGpLmJ7ms)?") %>
          </li>
        </ul>
        <hr />
        <h5>Lists</h5>
        <p class="card-text">
          Markdown supports both ordered and unordered lists, and making them is very simple (although this implementation of Markdown does not support ordered lists with an offset).
        </p>
        <ul>
          <li>For ordered lists, just start your list with <code>1. </code> (the space is important) and go from there - you do not need to do 2 line breaks or even add 2 spaces at the end of the line:
            <pre class="codeblock-fenced">1. First item
2. Second item
3. Third item
4. Fourth item
5.This is why you need a space in between the '.' and the item</pre>
            <%= markdown("1. First item\n2. Second item\n3. Third item\n4. Fourth item\n5.This is why you need a space in between the '.' and the item") %>
          </li>
          <li>The order of the numbers does not matter, as long as they are all positive:
            <pre class="codeblock-fenced">2. First item
8. Second item
12. Third item
0. Fourth item
-3. Negative numbers do not work</pre>
            <%= markdown("2. First item\n8. Second item\n12. Third item\n0. Fourth item\n-3. Negative numbers do not work") %>
          </li>
          <li>For nested ordered lists, simply indent your items (the first level can be 1 space, the second level must be at least 4 spaces), and the order does not matter:
            <pre class="codeblock-fenced">1. First top-level item
1. Second top-level item
  2. First nested item
 1. Second nested item
     3. First second-level nested item (5 spaces)
1. Third top-level item</pre>
            <%= markdown("1. First top-level item\n1. Second top-level item\n  2. First nested item\n 1. Second nested item\n     3. First second-level nested item (5 spaces)\n1. Third top-level item") %>
          </li>
          <li>For unordered lists, you can use any combination of <code>+</code>, <code>-</code>, and <code>*</code>, and you can nest lists:
              <pre class="codeblock-fenced">- First item
+ Second item
* Third item
  - Nested item
+ Fourth item
+Don't forget the space!</pre>
            <%= markdown("- First item\n+ Second item\n* Third item\n  - Nested item\n+ Fourth item\n+Don't forget the space!") %>
          </li>
          <li>You can combine ordered and unordered lists and, like other elements, insert other Markdown as well:
              <pre class="codeblock-fenced">1. Ordered list at first
3. *With* _*some*_ Markdown
  - Throw in some _unordered_ items while we're at it  
  And if you put 2 spaces on the line above, you can do a line-break
  * *That's a spicy looking* ***Markdown s_ou_p*** *we've got brewing*
9. Fourth item</pre>
            <%= markdown("1. Ordered list at first\n3. *With* _*some*_ Markdown\n  - Throw in some _unordered_ items while we're at it  \n  And if you put 2 spaces on the line above, you can do a line-break\n  * *That's a spicy looking* ***Markdown s_ou_p*** *we've got brewing*\n9. Fourth item") %>
          </li>
        </ul>
        <hr />
        <h5>Horizontal Rules (Lines/Separators)</h5>
        <p class="card-text">
          To create a line or separator that stretches across the page (like the <code>&lt;hr/&gt;</code> tag), there are multiple ways to go about doing it.
        </p>
        <ul>
          <li>Use 3 or more asterisks or underscores on a line by themselves (dashes also work, with some limitations):
            <pre class="codeblock-fenced">You do not need to do 2 line breaks or add 2 spaces for this:
___
All you need are 3, but you can add as many as you want
*************************************************************
Makes no difference</pre>
            <%= markdown("You do not need to do 2 line breaks or add 2 spaces for this:\n___\nAll you need are 3, but you can add as many as you want\n*************************************************************\nMakes no difference") %>
          </li>
          <li>You can also use dashes, but you <i>must</i> separate it by 2 line breaks or it will be interpreted as alternate H2 syntax:
            <pre class="codeblock-fenced">This does work

---
Only 1 line break required for the bottom

If you forget the extra line break, however...
---
It's not going to be what you expect.</pre>
            <%= markdown("This does work\n\n---\nOnly 1 line break required for the bottom\n\nIf you forget the extra line break, however...\n---\nIt's not going to be what you expect.") %>
          </li>
          <li>However, you cannot include anything else on this line or it will not work:
            <pre class="codeblock-fenced">*** hmmm</pre>
            <%= markdown("*** hmmm") %>
          </li>
        </ul>
        <% if ApplicationHelper.image_embeds_allowed %>
          <hr />
          <h5>Images</h5>
          <p class="card-text">
            Although you cannot upload images to the application, you can embed external images in your messages.
          </p>
          <ul>
            <li>Instead, you can upload your images to external sites and embed them using the syntax <code>![img alt-text (shown when image fails to load)](link/to/image "img title text (shown when you hover over the image)")</code>:
              <pre class="codeblock-fenced">![Shopping cart turf war](https://i.redd.it/njgmk8xti1z21.jpg "hmmm")</pre>
              <%= markdown("![Shopping cart turf war](https://i.redd.it/njgmk8xti1z21.jpg \"hmmm\")") %>
            </li>
            <li>You can also create links you can click on on images using the syntax <code>[![image alt-text](link/to/image "img title text")](link/when/clicked/on)</code>:
              <pre class="codeblock-fenced">[![UNC Charlotte, viewed from the 10th floor of the library overlooking Woodward, CHHS, COED, and the Student Union](https://i.imgur.com/gjO7WLo.jpg "Picture of UNCC before the building of the Health and Wellness Center, date & photographer unknown")](https://www.uncc.edu/)</pre>
              <%= markdown("[![UNC Charlotte, viewed from the 10th floor of the library overlooking Woodward, CHHS, COED, and the Student Union](https://i.imgur.com/gjO7WLo.jpg \"Picture of UNCC from the 10th floor of the library, photographer unknown\")](https://www.uncc.edu/)") %>
            </li>
            <li>Reference-style images are also supported using the syntax <code>![alt-text][Case Insensitive Reference]</code>, followed by <code>[case iNseNsitive reference]: link/to/image "title text"</code> on another line:
              <pre class="codeblock-fenced">![A cat in a raincoat][cAt PiCture]

Other _Markdown_ *text* that will go **below** the image

[cat picture]: https://i.imgur.com/f2cQCCu.jpg "A cute cat in a raincoat"</pre>
              <%= markdown("![A cat in a raincoat][cAt PiCture]\n\nOther _Markdown_ *text* that will go **below** the image\n\n[cat picture]: https://i.imgur.com/f2cQCCu.jpg \"A cute cat in a raincoat\"") %>
            </li>
          </ul>
        <% end %>
        <hr />
        <h5>Inline Code Tags</h5>
        <p class="card-text">
          If you want to make a small bit of text appear <code>like this</code>, it's very simple.
        </p>
        <ul>
          <li>Just enclose it in ticks (<code>`</code>, also known as the backtick, grave accent, or diacritic):
            <pre class="codeblock-fenced">This text looks normal, `but this text does not.`</pre>
            <%= markdown("This text looks normal, `but this text does not.`") %>
          </li>
          <li>Unlike the other elements, any Markdown tags have no effect here:
            <pre class="codeblock-fenced">Markdown works _*here*_, `but not _*here*_`</pre>
            <%= markdown("Markdown works _*here*_, `but not _*here*_`") %>
          </li>
          <li>The ticks cannot be escaped with a backslash, either:
            <pre class="codeblock-fenced">`Esacping this \` does not work`</pre>
            <%= markdown("`Esacping this \` does not work`") %>
          </li>
        </ul>
        <hr />
        <h5>Code Blocks</h5>
        <p class="card-text">
          If you want to post a large amount of preformatted text (e.g. an error message, a chunk of code, or some ASCII art), code blocks are what you need!
        </p>
        <ul>
          <li>Just enclose the lines with 3 ticks:
            <pre class="codeblock-fenced">```
               ((`\
            ___ \\ '--._
         .'`   `'    o  )
        /    \   '. __.'
       _|    /_  \ \_\_
jgs   {_\______\-'\__\_\
```</pre>
            <!-- Source for ASCII art: https://www.asciiart.eu/animals/rabbits -->
            <%= markdown("```\n               ((`\\\n            ___ \\\\ '--._\n         .'`   `'    o  )\n        /    \\   '. __.'\n       _|    /_  \\ \\_\\_\njgs   {_\______\\-'\\__\\_\\\n```") %>
          </li>
          <li>You can also encose it with 3 tildes:
            <pre class="codeblock-fenced">~~~
               ((`\
            ___ \\ '--._
         .'`   `'    o  )
        /    \   '. __.'
       _|    /_  \ \_\_
jgs   {_\______\-'\__\_\
~~~</pre>
            <%= markdown("~~~\n               ((`\\\n            ___ \\\\ '--._\n         .'`   `'    o  )\n        /    \\   '. __.'\n       _|    /_  \\ \\_\\_\njgs   {_\______\\-'\\__\\_\\\n~~~") %>
          </li>
          <li>You can even open with tildes and close with ticks (and vice versa). Regardless of which you pick, Markdown is not used in code blocks:
            <pre class="codeblock-fenced">~~~
_*Hello, World!*_
1. First item
2. Second item
3. Third item
```</pre>
            <%= markdown("~~~\n_*Hello, World!*_\n1. First item\n2. Second item\n3. Third item\n```") %>
          </li>
          <li>Syntax highlighting is not supported, so specifiying the language has no effect:
            <pre class="codeblock-fenced">```ruby
puts "Hello, World!"
```</pre>
            <%= markdown("```ruby\nputs \"Hello, World!\"\n```") %>
          </li>
        </ul>
        <hr />
        <h5>Tables</h5>
        <p class="card-text">
          Markdown tables can look a bit odd at first, but are relatively easy once you learn how they are made.
        </p>
        <ul>
          <li>The easiest way to write tables is by formatting them like ASCII art:
            <pre class="codeblock-fenced">| Column A | Column B   | Column C |
| -------- | ---------- | -------- |
| Cell A1  | Cell B1    | Cell C1  |
| Cell A2  | Cell B2    | Cell C2  |
| Cell A3  | Cell B3    | Cell C3  |
| Cell A4  | Cell B4    | Cell C4  |</pre>
            <%= markdown("| Column A | Column B   | Column C |\n| -------- | ---------- | -------- |\n| Cell A1  | Cell B1    | Cell C1  |\n| Cell A2  | Cell B2    | Cell C2  |\n| Cell A3  | Cell B3    | Cell C3  |\n| Cell A4  | Cell B4    | Cell C4  |") %>
          </li>
          <li>You can align columns to the left, right, and center by putting colons on the left, right, and center of the line of dashes:
            <pre class="codeblock-fenced">| Column A | Column B   | Column C |
| --------:|:---------- |:--------:|
| This     |   This one | This one |
| column   |    is left | is       |
| is right |    aligned |   center |
| aligned  |            |  aligned |</pre>
            <%= markdown("| Column A | Column B   | Column C |\n| --------:|:---------- |:--------:|\n| This     |   This one | This one |\n| column   |    is left | is       |\n| is right |    aligned |   center |\n| aligned  |            |  aligned |") %>
          </li>
          <li>The most important thing to remember is that you only need 3 dashes separating each column, you can leave a cell empty, and the outer pipes are unnecessary. Other Markdown elements can be used, and you can make some weird looking but syntactically-valid tables that render just fine:
            <pre class="codeblock-fenced">This | isn't very | pretty
--- | ---:| ---
but it *still* | | gets the job done
and it _renders_ | `just fine` | and you can still use
column alignments | like this middle | column does</pre>
            <%= markdown("This | isn't very | pretty\n--- | ---:| ---\nbut it *still* | | gets the job done\nand it _renders_ | `just fine` | and you can still use\ncolumn alignments | like this middle | column does") %>
          </li>
        </ul>
        <hr />
        <h5>Highlights</h5>
        <p class="card-text">
          Highlighting text (also knows as "marking" or "marked" text) is also easy to accomplish with Markdown.
        </p>
        <ul>
          <li>The syntax for highlighting is just <code>==highlighted text==</code>:
            <pre class="codeblock-fenced">I want to really ==emphasize this point==</pre>
            <%= markdown("I want to really ==emphasize this point==") %>
          </li>
          <li>You can also mix it in with other Markdown syntax, like this:
            <pre class="codeblock-fenced">==*Lots* **of** _**Markdown**_==</pre>
            <%= markdown("==*Lots* **of** _**Markdown**_==") %>
          </li>
        </ul>
        <hr />
        <h5>Superscripts</h5>
        <p class="card-text">
          You can easily use superscripts with Markdown, and it's not just for numerical powers.
        </p>
        <ul>
          <li>The basic syntax for superscripts is just <code>a^b</code>:
            <pre class="codeblock-fenced">a^b^c^d^e</pre>
            <%= markdown("a^b^c^d^e") %>
          <% if !ApplicationHelper.image_embeds_allowed %>
            <li>Image embeds have been disabled by the administrator.</li>
          <% end %>
          </li>
          <li>If you want to spread the superscript across multiple words, use parenthesis or raise each word to the appropriate superscript:
            <pre class="codeblock-fenced">This^(uses parenthesis), but^this ^does ^^not</pre>
            <%= markdown("This^(uses parenthesis), but^this ^does ^^not") %>
          </li>
          <li>The only limitation of using over 1 level of superscript is that parenthesis stop working properly and you have to manually raise each word to the level you want:
            <pre class="codeblock-fenced">This^^(does not work), but^^this ^^does ^^work</pre>
            <%= markdown("This^^(does not work), but^^this ^^does ^^work") %>
          </li>
        </ul>
        <hr />
        <h5>Backslashes and Escaping Characters</h5>
        <p class="card-text">
          Some special characters can be escaped to prevent syntax from being interpreted. Specifically, you can escape the characters <code>\ ` * _ {} [] () # + - . !</code>.
        </p>
        <ul>
          <li>A single backslash will usually negate the effect of the special character next to it, for example:
            <pre class="codeblock-fenced">\* Without the backslash, this would be a bullet in an unordered list.

\* Without the backslash, this would be another bullet in an unordered list.</pre>
            <%= markdown("\\* Without the backslash, this would be a bullet in an unordered list.\n\n\\* Without the backslash, this would be another bullet in an unordered list.") %>
          </li>
          <li>However, sometimes escaping characters does not work as you might expect:
            <pre class="codeblock-fenced">This^(does (not\) work), but this^syntax ^\(works\) ^well</pre>
            <%= markdown("This^(does (not\\) work), but this^syntax ^\\(works\\) ^well") %>
          </li>
        </ul>
        <hr />
        <h5>Limitations</h5>
        <p class="card-text">
          While the Markdown parser this application uses (RedCarpet) does a decent job at covering the basics, it is not as comprehensive as some of the parsers out there.
          Specifically:
        </p>
        <ul>
          <li>Abbreviations are not supported.</li>
          <li>Anchor tags are not generated for header elements.</li>
          <li>Code blocks do not have syntax highlighting.</li>
          <li>Definitions are not supported.</li>
          <li>Footnotes are disabled because RedCarpet seems to handle them inconsistently.</li>
          <% if !ApplicationHelper.image_embeds_allowed %>
            <li>Image embeds have been disabled by the administrator.</li>
          <% end %>
          <li>No inlined HTML is allowed (for security reasons).</li>
          <li>Numbered lists ignore starting offsets, they will always start at 1.</li>
          <li>Subscripts are not supported.</li>
          <li>Tasks are not supported.</li>
          <li>The <code>++inserted text++</code> syntax for underlines is not supported.</li>
        </ul>
        <hr />
        <h5>External Resources</h5>
        <p class="card-text">
          Some resources you might find helpful for learning more about Markdown:
        </p>
        <ul>
          <li><a href="https://commonmark.org/help/">CommonMark - Markdown Reference</a></li>
          <li><a href="https://daringfireball.net/projects/markdown/syntax">Daring Fireball - Markdown Syntax Documentation (Detailed)</a></li>
          <li><a href="https://www.markdownguide.org/basic-syntax">Markdown Guide - Basic Syntax</a></li>
          <li><a href="https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet">Markdown Here - Markdown Cheatsheet</a></li>
        </ul>
        <p class="card-text">For an interactive Markdown editor, try these:</p>
        <ul>
          <li><a href="https://dillinger.io/">Dillinger - Online Markdown Editor</a></li>
          <li><a href="https://pandao.github.io/editor.md/en.html">Editor.md - Open source online Markdown editor</a></li>
          <li><a href="https://jbt.github.io/markdown-editor/">JBT's Markdown Editor</a></li>
          <li><a href="https://markdown-it.github.io/">markdown-it Demo</a></li>
          <li><a href="https://typora.io/">Typora - Desktop Markdown Editor and Reader</a></li>
        </ul>
        <p class="card-text">
          Note that "Editor.md" and "markdown-it Demo" implement some features not supported here, so be mindful that some of their fancier syntax might not work on this site.
        </p>
      </div>
    </div>
  </div>
</div>
