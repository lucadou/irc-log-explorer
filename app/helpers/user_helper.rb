module UserHelper
  def self.min_email_length
    Rails.configuration.application[:users][:email_addresses][:min_length]
  end

  def min_email_length
    UserHelper.min_email_length
  end

  def self.permissions_list
    { :index => [:admin, :user], :show => [:admin, :user], :advanced_search => [:admin, :user], :new => [:admin], :edit => [:admin], :create => [:admin], :update => [:admin], :destroy => [:admin] }
  end

  def permission_list
    UserHelper.permissions_list
  end
end
