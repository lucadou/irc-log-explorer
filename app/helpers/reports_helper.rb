module ReportsHelper
  include ApplicationHelper

  def self.report_statuses
    {
      'All': 'all',
      'Resolved': 'resolved',
      'Unresolved': 'unresolved'
    }
  end

  # Sorting
  def report_sort_link(column, title = nil)
    # The title parameter is used if the column name isn't what I want to use
    title ||= column.titleize
    direction = column == report_sort_column && sort_direction == "asc" ? "desc" : "asc"
    icon = sort_direction == "asc" ? "fas fa-caret-up" : "fas fa-caret-down"
    icon = column == report_sort_column ? icon : ""
    link_to "#{title} <span class='#{icon}'></span>".html_safe, params.permit(:column, :direction, :id, :channel, :sender, :advanced_query, :resolved_at, :resolved, :reason, :start_time, :end_time, :msg_id, :user_id, :resolver_id, :valid_search, :page, :highlight, :per_page, :utf8).merge(:column => column, :direction => direction)
  end

  def report_sortable_columns
    ["id", "msg_channel", "msg_id", "msg_sender", "created_at", "resolved"]
  end

  def report_sort_column
    user_column = Current.user.user_preference.default_sort_col
    user_column = "created_at" if user_column == "date"
    user_column = "msg_sender" if user_column == "sender"
    user_column = "msg_channel" if user_column == "channel"
    report_sortable_columns.include?(params[:column]) ? params[:column] : user_column
      # Only uses user preferences default column sort when user has
      # not clicked on any other columns
  end
end
