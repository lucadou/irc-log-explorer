module RedcarpetHelper
  class BootstrapRenderer < Redcarpet::Render::HTML
    def block_code(code, language)
      "<pre class=\"codeblock-fenced\">" \
        "<code>#{code}</code>" \
      "</pre>"
    end

    def block_quote(quote)
      "<blockquote class=\"blockquote blockquote-md\">" \
        "#{quote}" \
      "</blockquote>"
    end

    def codespan(code)
      "<code class=\"fenced\">" \
        "#{code}" \
      "</code>"
    end

    def image(link, title, alt_text)
      max_width = "style=\"max-width: #{ApplicationHelper.image_embed_max_size};\"" if ApplicationHelper.image_embed_max_size
      "<img class=\"img-fluid\" src=\"#{link}\" title=\"#{title}\"" \
        "alt=\"#{alt_text}\" #{max_width}" \
      ">"
    end

    def table(header, body)
      "<table class=\"table table-striped table-bordered\">" \
        "#{header}#{body}" \
      "</table>"
    end
  end
end
