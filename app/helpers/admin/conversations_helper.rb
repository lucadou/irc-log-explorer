module Admin::ConversationsHelper
  include ApplicationHelper

  # Conversations page helper methods

  def dash_conversation_statuses
    {'All': 'all', 'Closed': 'locked', 'Open': 'unlocked'}
  end

  def dash_conversation_unreads
    {'N/A': 'all', 'Yes': 'true', 'No': 'false'}
  end

  # Sorting

  def dash_conversation_search_sort_link(column, title = nil)
    # The title parameter is used if the column name isn't what I want to use
    title ||= column.titleize
    direction = column == dash_conversation_sort_column && sort_direction == "asc" ? "desc" : "asc"
    icon = sort_direction == "asc" ? "fas fa-caret-up" : "fas fa-caret-down"
    icon = column == dash_conversation_sort_column ? icon : ""
    link_to "#{title} <span class='#{icon}'></span>".html_safe, params.permit(:column, :direction, :locked, :subject, :op_msg_query, :admin_unreads, :target_user, :start_time, :end_time, :all_msg_query, :page, :highlight, :per_page, :utf8).merge(:column => column, :direction => direction)
  end

  def dash_conversation_sortable_columns
    ["subject", "target_id", "created_at", "has_unreads"]
  end

  def dash_conversation_sort_column
    user_column = Current.user.user_preference.default_sort_col
    if user_column == "sender"
      user_column = "target_id"
    else # "date" or "channel"
      user_column = "created_at"
    end
    dash_conversation_sortable_columns.include?(params[:column]) ? params[:column] : user_column
      # Only uses user preferences default column sort when user has
      # not clicked on any other columns
  end
end
