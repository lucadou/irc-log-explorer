module Admin::EulasHelper
  include ApplicationHelper

  # Sorting

  def dash_eula_search_sort_link(column, title = nil)
    # The title parameter is used if the column name isn't what I want to use
    title ||= column.titleize
    direction = column == dash_eula_sort_column && sort_direction == "asc" ? "desc" : "asc"
    icon = sort_direction == "asc" ? "fas fa-caret-up" : "fas fa-caret-down"
    icon = column == dash_eula_sort_column ? icon : ""
    link_to "#{title} <span class='#{icon}'></span>".html_safe, params.permit(:column, :direction, :note, :eula_text, :publisher, :start_time, :end_time, :page, :highlight, :per_page, :utf8).merge(:column => column, :direction => direction)
  end

  def dash_eula_sortable_columns
    ["id", "created_at", "publisher"]
  end

  def dash_eula_sort_column
    user_column = Current.user.user_preference.default_sort_col
    if user_column == "sender"
      user_column = "publisher"
      # I use publisher instead of user_id because user_id is nullified on deletion
    else # "date" or "channel"
      user_column = "created_at"
    end
    dash_eula_sortable_columns.include?(params[:column]) ? params[:column] : user_column
    # Only uses user preferences default column sort when user has
    # not clicked on any other columns
  end
end
