class RemoveBanAttributesFromUsers < ActiveRecord::Migration[6.0]
  def change
    remove_column :users, :banned_until, :datetime
    remove_column :users, :permanently_banned, :boolean
    remove_column :users, :ban_reason_internal, :text
    remove_column :users, :ban_reason_external, :string
  end
end
