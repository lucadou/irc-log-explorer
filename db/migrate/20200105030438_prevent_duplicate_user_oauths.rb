class PreventDuplicateUserOauths < ActiveRecord::Migration[5.2]
  def change
    add_index :user_oauths, [:provider, :user_id], unique: true
    add_index :user_oauths, [:provider, :uid], unique: true
    # I enforce uniqueness on a combination of provider + user_id because
    # no user (as referenced by their user_id) should have 2 records with
    # the same provider.
    # I also enforce uniqueness on a combination of provider + uid because
    # that would seem to indicate the same Oauth account has been added to
    # more than 1 user.
    # https://stackoverflow.com/a/1449466
  end
end
