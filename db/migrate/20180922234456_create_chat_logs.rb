class CreateChatLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :chat_logs do |t|
      t.string      :channel, nil: false, default: ""
      t.string      :sender, nil: false, default: ""
      t.text        :message, nil: false, default: ""
      t.integer     :date, nil: false, default: ""
      
      t.integer     :date_last_edited, nil: true, default: nil
      t.string      :last_edit_reason, nil: true, default: nil

      t.timestamps
    end
  end
end
