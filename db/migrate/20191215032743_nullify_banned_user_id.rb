class NullifyBannedUserId < ActiveRecord::Migration[5.2]
  def change
    change_column_null :bans, :user_id, true
    add_column :bans, :banned_user, :integer, null: false
  end
end
