class CreateReports < ActiveRecord::Migration[5.2]
  def change
    create_table :reports do |t|
      t.belongs_to  :user, index: true, foreign_key: true
      t.integer     :reporter, null: false
      t.string      :report_reason, null: false
      t.boolean     :resolved, null: false, default: false
      t.string      :resolving_action, null: true
      t.datetime    :resolved_at, null: true
      t.integer     :resolver, null: true
      
      # Original message
      t.integer     :msg_id, null: false
      t.string      :msg_channel, null: false
      t.string      :msg_sender, null: false
      t.text        :msg_text, null: false
      t.integer     :msg_date, null: false, limit: 8
      
      t.timestamps
    end
  end
end
