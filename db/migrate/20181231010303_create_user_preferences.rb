class CreateUserPreferences < ActiveRecord::Migration[5.2]
  def change
    create_table :user_preferences do |t|
      t.belongs_to :user,           index: {unique: true}, foreign_key: true
      
      t.string     :tz_offset,      null: false, default: "+00:00"
      t.boolean    :use_24hr_time,  null: false, default: true
      t.string     :date_format,    null: false, default: "%Y-%m-%d"
        # YYYY-mm-dd, e.g. 2018-01-31, 0042-12-01, etc.
        # Time formatting will be %H:%M:%S.%L (HH:MM:SS.sss) 24 hr time or
        # %I:%M:%S.%L %p (HH:MM:SS.sss AM/PM) for 12 hr time
      
      t.string    :default_sort_col,    null: false, default: "date"
      t.string    :default_sort_order,  null: false, default: "desc"
      t.integer   :search_results,      null: false, default: 10
      t.string    :theme,               null: false, default: "Light (Default)"
      
      # Notification settings
      t.boolean   :notify_pwchange_pri, null: false, default: true
      t.boolean   :notify_pwchange_bkp, null: false, default: false
      t.boolean   :notify_pwreset_pri,  null: false, default: true
      t.boolean   :notify_pwreset_bkp,  null: false, default: false
      t.boolean   :notify_sso_pri,      null: false, default: true
      t.boolean   :notify_sso_bkp,      null: false, default: false
      t.boolean   :notify_2fa_pri,      null: false, default: true
      t.boolean   :notify_2fa_bkp,      null: false, default: false
      t.boolean   :notify_admin_pri,    null: false, default: true
      t.boolean   :notify_admin_bkp,    null: false, default: false
      
      t.timestamps
    end
  end
end
