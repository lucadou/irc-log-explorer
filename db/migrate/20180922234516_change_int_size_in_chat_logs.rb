class ChangeIntSizeInChatLogs < ActiveRecord::Migration[5.1]
  def change
    change_column :chat_logs, :date, :integer, limit: 8
  end
end
