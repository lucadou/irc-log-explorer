class AddSecondUseridColToTos < ActiveRecord::Migration[6.0]
  def change
    # The reason I am adding this column is for a backup user ID because deleting a user account will nullify the user_id column.
    add_column :eulas, :publisher, :int
  end
end
