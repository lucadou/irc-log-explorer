class MakeTosSecondaryUseridColNonnull < ActiveRecord::Migration[6.0]
  def change
    change_column :eulas, :publisher, :int, null: false
  end
end
