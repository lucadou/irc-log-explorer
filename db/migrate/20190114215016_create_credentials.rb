class CreateCredentials < ActiveRecord::Migration[5.2]
  def change
    create_table :credentials do |t|
      # For FIDO2 U2F tokens
      t.string :external_id
      t.string :public_key
      t.bigint :user_id
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: false
      t.string :nickname
      t.references :user, foreign_key: true
      
      t.timestamps
    end
  end
end
