class MultiDomainOauth < ActiveRecord::Migration[5.2]
  def self.up
    # Remove non-multi-domain indexes
    remove_index :user_oauths, name: "index_user_oauths_on_provider_and_user_id"
    remove_index :user_oauths, name: "index_user_oauths_on_provider_and_uid"
    # Add domain column
    add_column :user_oauths, :domain, :string, null: true, default: nil
    # Add multi-domain indexes
    add_index :user_oauths, [:domain, :provider, :user_id], unique: true
    add_index :user_oauths, [:domain, :provider, :uid], unique: true
    # From the previous migration:
    # I enforce uniqueness on a combination of provider + user_id because
    # no user (as referenced by their user_id) should have 2 records with
    # the same provider.
    # I also enforce uniqueness on a combination of provider + uid because
    # that would seem to indicate the same Oauth account has been added to
    # more than 1 user.
    # https://stackoverflow.com/a/1449466
    # I also now add in the domain to both indexes because I want to allow
    # for adding the same Oauth account onto multiple domains
  end

  def self.down
    remove_index :user_oauths, name: "index_user_oauths_on_domain_and_provider_and_uid"
    remove_index :user_oauths, name: "index_user_oauths_on_domain_and_provider_and_user_id"
    remove_column :user_oauths, :domain
    add_index :user_oauths, [:provider, :user_id], unique: true
    add_index :user_oauths, [:provider, :uid], unique: true
  end
end
