class CreateRoles < ActiveRecord::Migration[5.2]
  def change
    create_table :roles do |t|
      t.string :name,         null: false, default: ""
      t.string :description,  null: false, default: ""
      t.boolean :enabled,     null: false, default: true
      t.references :creator,  index: true
      t.integer :created_by,  null: true, default: nil
        # ID of user who created it (incase account is deleted)

      t.timestamps
    end
    add_foreign_key :roles, :users, column: :creator_id
  end
end
