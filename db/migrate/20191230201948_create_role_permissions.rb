class CreateRolePermissions < ActiveRecord::Migration[5.2]
  def change
    create_table :role_permissions do |t|
      t.jsonb :permissions,  null: false
      t.references :role,    foreign_key: true
      t.references :creator, index: true
      t.integer :created_by, null: true, default: nil

      t.timestamps
    end
    add_foreign_key :role_permissions, :users, column: :creator_id
  end
end
