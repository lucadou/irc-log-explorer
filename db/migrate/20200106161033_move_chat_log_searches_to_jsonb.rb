class MoveChatLogSearchesToJsonb < ActiveRecord::Migration[5.2]
  def self.up
    puts "Migrating searches where query_type is one of: advanced, basic, browse, context"
    execute("UPDATE searches" \
            "  SET search_params = jsonb_build_object(" \
            "    'basic_query', basic_query," \
            "    'advanced_query', advanced_query," \
            "    'channel', channel," \
            "    'sender', sender," \
            "    'case_sensitive', case_sensitive," \
            "    'start_id', start_id," \
            "    'base_id', base_id," \
            "    'end_id', end_id," \
            "    'start_time', start_time," \
            "    'end_time', end_time," \
            "    'per_page', per_page," \
            "    'page', page," \
            "    'highlight', message_highlight," \
            "    'sort_column', sort_column," \
            "    'sort_direction', sort_direction," \
            "    'custom_per_page', custom_per_page" \
            "  )" \
            "  WHERE query_type IN ('advanced', 'basic', 'browse', 'context');")
    puts "Migrated searches where query_type is one of: advanced, basic, browse, context"
  end

  def self.down
    puts "Defining cast function"
    execute("CREATE OR REPLACE FUNCTION cast_bigint_default(text, bigint) RETURNS bigint AS $$" \
            "  BEGIN" \
            "    RETURN CAST($1 AS bigint);" \
            "  EXCEPTION" \
            "    WHEN invalid_text_representation THEN" \
            "      RAISE NOTICE 'WARNING: default cast value used for %, data has likely been lost!%', $1, '\n';" \
            "      RETURN $2;" \
            "  END;" \
            " $$ LANGUAGE PLPGSQL IMMUTABLE;")
    puts "Cast function defined"
    # The purpose of this function is to do a very rough conversion on
    # start_time and end_time. (If it casts to int, great, if not, it uses
    # the second variable for a default value.)
    # I am moving from converting datetime str->epoch in the controller to the
    # model, so I am now saving the str the use provided in the database. This
    # means that if you upgrade from an older version, some start/end_time will
    # be an int (epoch time), while others will be a str. And if you downgrade,
    # data will be lost, as you go from a str datetime -> int. I could try
    # extracting the epoch time in Postgres, but any invalid values would cause
    # exceptions , plus the potential for errors based on the user's date format.
    # This application does not have the abiliy to search on this field, so
    # losing this data is not much of a problem.
    # Credit for this goes to https://stackoverflow.com/a/10307443

    puts "Migrating searches back where case_sensitive is not null"
    execute("UPDATE searches" \
            "  SET" \
            "    basic_query = search_params::jsonb->'basic_query'," \
            "    advanced_query = search_params::jsonb->'advanced_query'," \
            "    channel = search_params::jsonb->'channel'," \
            "    sender = search_params::jsonb->'sender'," \
            "    case_sensitive = (search_params::jsonb->>'case_sensitive')::boolean," \
            "    start_id = (search_params::jsonb->>'start_id')::integer," \
            "    base_id = (search_params::jsonb->>'base_id')::integer," \
            "    end_id = (search_params::jsonb->>'end_id')::integer," \
            "    start_time = cast_bigint_default(search_params::jsonb->>'start_time', 0)," \
            "    end_time = cast_bigint_default(search_params::jsonb->>'end_time', 0)," \
            "    per_page = (search_params::jsonb->>'per_page')::integer," \
            "    page = (search_params::jsonb->>'page')::integer," \
            "    message_highlight = (search_params::jsonb->>'highlight')::integer," \
            "    sort_column = search_params::jsonb->'sort_column'," \
            "    sort_direction = search_params::jsonb->'sort_direction'," \
            "    custom_per_page = (search_params::jsonb->>'custom_per_page')::boolean" \
            "  WHERE query_type IN ('advanced', 'basic', 'browse', 'context')" \
            "    AND search_params::jsonb->'case_sensitive' IS NOT NULL;")
    puts "Migrated searches back where case_sensitive is not null"
    # I made separate migrations for case_sensitive because the database has a
    # non-null constraint. Trying to save a value which might not be present in
    # jsonb causes exceptions.

    puts "Migrating searches back where case_sensitive is null"
    execute("UPDATE searches" \
            "  SET" \
            "    basic_query = search_params::jsonb->'basic_query'," \
            "    advanced_query = search_params::jsonb->'advanced_query'," \
            "    channel = search_params::jsonb->'channel'," \
            "    sender = search_params::jsonb->'sender'," \
            "    start_id = (search_params::jsonb->>'start_id')::integer," \
            "    base_id = (search_params::jsonb->>'base_id')::integer," \
            "    end_id = (search_params::jsonb->>'end_id')::integer," \
            "    start_time = cast_bigint_default(search_params::jsonb->>'start_time', 0)," \
            "    end_time = cast_bigint_default(search_params::jsonb->>'end_time', 0)," \
            "    per_page = (search_params::jsonb->>'per_page')::integer," \
            "    page = (search_params::jsonb->>'page')::integer," \
            "    message_highlight = (search_params::jsonb->>'highlight')::integer," \
            "    sort_column = search_params::jsonb->'sort_column'," \
            "    sort_direction = search_params::jsonb->'sort_direction'," \
            "    custom_per_page = (search_params::jsonb->>'custom_per_page')::boolean" \
            "  WHERE query_type IN ('advanced', 'basic', 'browse', 'context')" \
            "    AND search_params::jsonb->'case_sensitive' IS NOT NULL;")
    puts "Migrated searches back where case_sensitive is null"

    puts "Removing cast function"
    execute("DROP FUNCTION IF EXISTS cast_bigint_default;")
    puts "Cast function removed"
  end
end
