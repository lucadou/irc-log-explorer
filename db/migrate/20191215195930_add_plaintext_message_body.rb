class AddPlaintextMessageBody < ActiveRecord::Migration[5.2]
  def change
    add_column :messages, :body_plaintext, :text, default: ''
    change_column_null :messages, :body_plaintext, false
    # This column is intended to be used for storing a plaintext version of the
    # body field for easy searching, as RedCarpet has a plaintext renderer and
    # Postgres has no ability to search Markdown as plaintext.
  end
end
