class CreateUserOauths < ActiveRecord::Migration[5.2]
  def change
    create_table :user_oauths do |t|
      t.string :provider,       null: false
      t.string :token,          null: false
      t.string :email,          null: true, default: nil
        # Some providers require special permissions for emails, so it might
        # need to be null
      t.string :uid,            null: false
      t.string :username,       null: true, default: nil
      t.datetime :expires_at,   null: true, default: nil
      t.references :user,       foreign_key: true

      t.timestamps
    end
  end
end
