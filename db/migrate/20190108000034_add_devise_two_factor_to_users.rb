class AddDeviseTwoFactorToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :encrypted_otp_secret, :string, null: true, default: nil
    add_column :users, :encrypted_otp_secret_iv, :string
    add_column :users, :encrypted_otp_secret_salt, :string
    add_column :users, :consumed_timestep, :integer
    add_column :users, :otp_required_for_login, :boolean, null: false, default: false
    add_column :users, :otp_verification_timeout, :datetime, null: true, default: nil
    add_column :users, :otp_enabled_at, :datetime, null: true, default: nil
    add_column :users, :otp_backup_codes, :string, null: true, default: nil, array: true
    add_column :users, :otp_backup_codes_enabled_at, :datetime, null: true, default: nil
  end
end
