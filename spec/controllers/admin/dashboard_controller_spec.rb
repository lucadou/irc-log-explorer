require 'rails_helper'

RSpec.describe Admin::DashboardController, type: :controller do

  describe "GET #index" do
    it "returns a 302 when not signed in" do
      get :index
      expect(response).to have_http_status(302)
      expect(response).to redirect_to(login_path)
    end
  end

end
