require 'rails_helper'
include AuthenticationHelpers

RSpec.describe "ResourceAuthenticationRequirements", type: :request do
  it "can access ChatLogs when logged in" do
    sign_up_with('Test User 1', 'testuser1@domain.test', 'CorrectHorseBatteryStaple', 'CorrectHorseBatteryStaple')
    expect(page).to have_content("Welcome! You have signed up successfully.")
    expect(page).to have_content("Terms of Service")
    click_on "Continue"
    expect(page).to have_content("Thank you for accepting the terms of service!")
    click_on "View Logs"
    expect(page).to have_content("Search")
    expect(page).to have_content("Channel")
    expect(page).to have_content("Sender")
    expect(page).to have_content("Message")
    expect(page).to have_content("Time")
  end
  it "can access Reports when logged in" do
    sign_up_with('Test User 1', 'testuser1@domain.test', 'CorrectHorseBatteryStaple', 'CorrectHorseBatteryStaple')
    expect(page).to have_content("Welcome! You have signed up successfully.")
    expect(page).to have_content("Terms of Service")
    click_on "Continue"
    expect(page).to have_content("Thank you for accepting the terms of service!")
    click_on "navbar-settings-link"
    expect(page).to have_content("My Account")
    click_on "Reports"
    expect(page).to have_content("No reports")
  end
  it "can access Settings when logged in" do
    # Test all tabs in Settings
    sign_up_with('Test User 1', 'testuser1@domain.test', 'CorrectHorseBatteryStaple', 'CorrectHorseBatteryStaple')
    expect(page).to have_content("Welcome! You have signed up successfully.")
    expect(page).to have_content("Terms of Service")
    click_on "Continue"
    expect(page).to have_content("Thank you for accepting the terms of service!")
    click_on "navbar-settings-link"
    expect(page).to have_content("My Account")
    click_on "Overview"
    expect(page).to have_content("Account Overview")
    click_on "Account Settings"
    expect(page).to have_content("Change Account Information")
    click_on "Appearance & Localization"
    expect(page).to have_content("Appearance")
    click_on "Notifications"
    expect(page).to have_content("Notification Preferences")
    click_on "Reports"
    expect(page).to have_content("Your Reports")
    click_on "Security"
    expect(page).to have_content("Security Preferences")
  end
  it "cannot access ChatLogs when not logged in" do
    visit root_path
    expect(page).to have_content("Sign in")
    expect(page).to have_content("Register")
    visit chat_logs_path
    expect(current_path).to eq new_user_session_path
    expect(page).to have_content("You must be signed in to complete this action")
    visit advanced_search_chat_logs_path
    expect(current_path).to eq new_user_session_path
    expect(page).to have_content("You must be signed in to complete this action")
    # I will have to test the new logs path if making chat logs is enabled
    # once I create the configuration file with an option for that
    #visit new_chat_log_path
    #expect(current_path).to eq new_user_session_path
    #expect(page).to have_content("You must be signed in to complete this action")
    visit chat_log_path(1)
    expect(current_path).to eq new_user_session_path
    expect(page).to have_content("You must be signed in to complete this action")
    visit edit_chat_log_path(1)
    expect(current_path).to eq new_user_session_path
    expect(page).to have_content("You must be signed in to complete this action")
  end
  it "cannot access Reports when not logged in" do
    visit root_path
    expect(page).to have_content("Sign in")
    expect(page).to have_content("Register")
    visit reports_path
    expect(current_path).to eq new_user_session_path
    expect(page).to have_content("You must be signed in to complete this action")
    visit new_report_path(:msg_id => 1)
    expect(current_path).to eq new_user_session_path
    expect(page).to have_content("You must be signed in to complete this action")
  end
  it "cannot access Settings when logged in" do
    visit root_path
    expect(page).to have_content("Sign in")
    expect(page).to have_content("Register")
    visit account_overview_path
    expect(current_path).to eq new_user_session_path
    expect(page).to have_content("You must be signed in to complete this action")
    visit account_settings_path
    expect(current_path).to eq new_user_session_path
    expect(page).to have_content("You must be signed in to complete this action")
    visit account_ui_path
    expect(current_path).to eq new_user_session_path
    expect(page).to have_content("You must be signed in to complete this action")
    visit account_notifications_path
    expect(current_path).to eq new_user_session_path
    expect(page).to have_content("You must be signed in to complete this action")
    visit account_security_path
    expect(current_path).to eq new_user_session_path
    expect(page).to have_content("You must be signed in to complete this action")

    # 2FA - TOTP
    visit account_security_2fa_totp_path
    expect(current_path).to eq new_user_session_path
    expect(page).to have_content("You must be signed in to complete this action")

    # 2FA - Recovery Codes
    visit account_security_2fa_recovery_codes_path
    expect(current_path).to eq new_user_session_path
    expect(page).to have_content("You must be signed in to complete this action")

    # 2FA - U2F
    visit account_security_2fa_u2f_keys_path
    expect(current_path).to eq new_user_session_path
    expect(page).to have_content("You must be signed in to complete this action")
    visit account_security_2fa_u2f_add_path
    expect(current_path).to eq new_user_session_path
    expect(page).to have_content("You must be signed in to complete this action")
  end
end
