require 'rails_helper'
require 'time'
include AuthenticationHelpers
include ChatLogSearchHelpers

RSpec.describe "ChatLogsSearch", type: :request do
  before :each do
    name = 'Test User 1'
    email = 'testuser@domain.test'
    password = 'CorrectHorseBatteryStaple'
    sign_up_with(name, email, password, password)
    expect(page).to have_content("Welcome! You have signed up successfully.")
    expect(page).to have_content("Terms of Service")
    click_on "Continue"
    expect(page).to have_content("Thank you for accepting the terms of service!")
    visit advanced_search_chat_logs_path
  end

  describe "User can" do
    it "execute a basic search which is case insensitive" do
      # Will return results
      basic_search_with(query: '(closed parenthesis') # same fase as result
      expect(page).to have_content("(closed parenthesis for regex testing)")
      expect(page).not_to have_content("Next ›")
      basic_search_with(query: '(closed PARENTHESIS') # different case than result
      expect(page).to have_content("(closed parenthesis for regex testing)")
      expect(page).not_to have_content("Next ›")

      # Will not return results
      basic_search_with(query: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eu lorem non neque commodo aliquet.')
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("Next ›")
    end
    it "execute a basic search with a blank query" do
      basic_search_with(query: '')
      expect(page).to have_content("No search terms given; executed empty search")
      expect(page).to have_content("/forward Slash test/")
      expect(page).to have_content("unclosed ending forward slash for regex testing/")
      expect(page).to have_content("25 letter channel name for testing")
      expect(page).to have_content("/unclosed forward slash with MiXeD cAsE for regex testing")
      expect(page).to have_link("Next ›", href: "/chat_logs?page=2&query=")
      expect(page).to have_link("Last »", href: "/chat_logs?page=13&query=")
    end
    it "execute an advanced search with no fields filled in" do
      advanced_search_with(channel: '', sender: '', message: '', case_sensitive: '', start_time: '', end_time: '', start_id: '', end_id: '', per_page: '')
      expect(page).to have_content("No valid parameters; executed empty search")
      expect(page).to have_content("/forward Slash test/")
      expect(page).to have_content("unclosed ending forward slash for regex testing/")
      expect(page).to have_content("25 letter channel name for testing")
      expect(page).to have_content("/unclosed forward slash with MiXeD cAsE for regex testing")
      expect(page).to have_link("Next ›", href: "/chat_logs?page=2")
      expect(page).to have_link("Last »", href: "/chat_logs?page=13")
    end
    it "execute an advanced search with a channel" do
      # Search with non-existent channel
      advanced_search_with(channel: '#notachannel')
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("No valid parameters; executed empty search")
      expect(page).not_to have_content("Next ›")

      # Search a 3 letter channel
      advanced_search_with(channel: '#tla')
      expect(page).to have_content("three letter channel name for testing")
      expect(page).not_to have_content("No valid parameters; executed empty search")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")

      # Search a 25 letter channel (& underscores)
      advanced_search_with(channel: '#channel12345678901234567_')
      expect(page).to have_content("25 letter channel name for testing")
      expect(page).not_to have_content("No valid parameters; executed empty search")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")

      # Search with underscores
      advanced_search_with(channel: '#luna_moona')
      expect(page).to have_content("(closed parenthesis for regex testing)")
      expect(page).to have_content("/forward slashes for regex testing/")
      expect(page).to have_content("/unclosed forward slash for regex testing")
      expect(page).to have_content("/forward slashes with MiXeD cAsE for regex testing/")
      expect(page).to have_content("/unclosed forward slash with MiXeD cAsE for regex testing")
      expect(page).not_to have_content("No valid parameters; executed empty search")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
    end
    it "execute an advanced search with a sender" do
      # Search for messages from a non-existent user
      advanced_search_with(sender: 'aaaaaaa')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("Next ›")

      # Search for messages from a user that exists
      advanced_search_with(sender: 'luna_moona')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).to have_content("luna_moona")
      expect(page).to have_content("#luna_moona")
      expect(page).not_to have_content("#monotonetim")
      expect(page).to have_content("25 letter channel name for testing")
      expect(page).to have_content("/unclosed forward slash with MiXeD cAsE for regex testing")
      expect(page).to have_content("/forward slashes for regex testing/")
      expect(page).not_to have_content("it was linked in a discord server")
      expect(page).to have_link("Next ›", href: "/chat_logs?page=2&sender=luna_moona")
      expect(page).to have_link("Last »", href: "/chat_logs?page=2&sender=luna_moona")
      click_on "Next ›"
      expect(page).to have_content("#luna_moona")
      expect(page).to have_content("#monotonetim")
      expect(page).to have_content("it was linked in a discord server")
    end
    it "execute an advanced search with a case insensitive message query" do
      # Lower case message with case insensitive
      advanced_search_with(message: 'i honestly')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#stereotonetim")
      expect(page).to have_content("I honestly have never seen this. i'm not too shocked, I've been expecting this for about a year or so.")

      # Lower case unescaped non-regex message with case insensitive
      # To make sure I do not re-introduce the bug in issue 74 again
      advanced_search_with(message: '/test')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#tla")
      expect(page).to have_content("three letter channel name for test/testing")
      expect(page).to have_current_path("/chat_logs?advanced_query=%2Ftest&case_sensitive=false")

      # Matching case message with case insensitive
      advanced_search_with(message: 'I honestly')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#stereotonetim")
      expect(page).to have_content("I honestly have never seen this. i'm not too shocked, I've been expecting this for about a year or so.")

      # Upper case message with case insensitive
      advanced_search_with(message: 'I HONESTLY')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#stereotonetim")
      expect(page).to have_content("I honestly have never seen this. i'm not too shocked, I've been expecting this for about a year or so.")

      # Mixed case message with case insensitive
      advanced_search_with(message: 'I HoNeStLy')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#stereotonetim")
      expect(page).to have_content("I honestly have never seen this. i'm not too shocked, I've been expecting this for about a year or so.")
    end
    it "execute an advanced search with a case insensitive escaped regex message query" do
      # Escaped forward slash at the start of the message
      advanced_search_with(message: '\/ForwarD')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#luna_moona")
      expect(page).not_to have_content("forward slash Test/")
      expect(page).to have_content("/forward Slash test/")
      expect(page).to have_content("/Forward slash test")
      expect(page).to have_content("/forward slashes with MiXeD cAsE for regex testing/")
      expect(page).to have_content("/forward slashes for regex testing/")

      # Escaped forward slash at the end of the message
      advanced_search_with(message: 'TeSt\/')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#luna_moona")
      expect(page).to have_content("forward slash Test/")
      expect(page).to have_content("/forward Slash test/")
      expect(page).not_to have_content("/Forward slash test")
      expect(page).not_to have_content("/forward slashes with MiXeD cAsE for regex testing/")
      expect(page).not_to have_content("/forward slashes for regex testing/")

      # Escaped forward slash at both the start and the end of the message
      advanced_search_with(message: '\/forward slAsh Test\/')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#luna_moona")
      expect(page).not_to have_content("forward slash Test/")
      expect(page).to have_content("/forward Slash test/")
      expect(page).not_to have_content("/Forward slash test")
      expect(page).not_to have_content("/forward slashes with MiXeD cAsE for regex testing/")
      expect(page).not_to have_content("/forward slashes for regex testing/")
    end
    it "execute an advanced search with a case sensitive escaped regex message query" do
      # Escaped forward slash at the start of the message, no results
      advanced_search_with(message: '\/ForwarD', case_sensitive: true)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).not_to have_content("#luna_moona")
      expect(page).not_to have_content("forward slash test/")
      expect(page).not_to have_content("/forward slash test/")
      expect(page).not_to have_content("/forward slash test")
      expect(page).not_to have_content("/forward slashes with MiXeD cAsE for regex testing/")
      expect(page).not_to have_content("/forward slashes for regex testing/")

      # Escaped forward slash at the start of the message, with results
      advanced_search_with(message: '\/Forward', case_sensitive: true)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#luna_moona")
      expect(page).not_to have_content("forward slash Test/")
      expect(page).not_to have_content("/forward Slash test/")
      expect(page).to have_content("/Forward slash test")
      expect(page).not_to have_content("/forward slashes with MiXeD cAsE for regex testing/")
      expect(page).not_to have_content("/forward slashes for regex testing/")

      # Escaped forward slash at the end of the message, no results
      advanced_search_with(message: 'TeSt\/', case_sensitive: true)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).not_to have_content("#luna_moona")
      expect(page).not_to have_content("forward slash Test/")
      expect(page).not_to have_content("/forward Slash test/")
      expect(page).not_to have_content("/Forward slash test")
      expect(page).not_to have_content("/forward slashes with MiXeD cAsE for regex testing/")
      expect(page).not_to have_content("/forward slashes for regex testing/")

      # Escaped forward slash at the end of the message, with results
      advanced_search_with(message: 'test\/', case_sensitive: true)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#luna_moona")
      expect(page).not_to have_content("forward slash Test/")
      expect(page).to have_content("/forward Slash test/")
      expect(page).not_to have_content("/Forward slash test")
      expect(page).not_to have_content("/forward slashes with MiXeD cAsE for regex testing/")
      expect(page).not_to have_content("/forward slashes for regex testing/")

      # Escaped forward slash at both the start and the end of the message, no results
      advanced_search_with(message: '\/foward SlasH test\/', case_sensitive: true)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).not_to have_content("#luna_moona")
      expect(page).not_to have_content("forward slash Test/")
      expect(page).not_to have_content("/forward Slash test/")
      expect(page).not_to have_content("/Forward slash test")
      expect(page).not_to have_content("/forward slashes with MiXeD cAsE for regex testing/")
      expect(page).not_to have_content("/forward slashes for regex testing/")

      # Escaped forward slash at both the start and the end of the message, with results
      advanced_search_with(message: '\/forward Slash test\/', case_sensitive: true)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#luna_moona")
      expect(page).not_to have_content("forward slash Test/")
      expect(page).to have_content("/forward Slash test/")
      expect(page).not_to have_content("/Forward slash test")
      expect(page).not_to have_content("/forward slashes with MiXeD cAsE for regex testing/")
      expect(page).not_to have_content("/forward slashes for regex testing/")
    end
    it "execute an advanced search with a case sensitive message query" do
      # Lower case message with case sensitive
      advanced_search_with(message: 'i honestly', case_sensitive: true)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).not_to have_content("#stereotonetim")
      expect(page).not_to have_content("I honestly have never seen this. i'm not too shocked, I've been expecting this for about a year or so.")

      # Matching case message with case sensitive
      advanced_search_with(message: 'I honestly', case_sensitive: true)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#stereotonetim")
      expect(page).to have_content("I honestly have never seen this. i'm not too shocked, I've been expecting this for about a year or so.")

      # Upper case message with case sensitive
      advanced_search_with(message: 'I HONESTLY', case_sensitive: true)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).not_to have_content("#stereotonetim")
      expect(page).not_to have_content("I honestly have never seen this. i'm not too shocked, I've been expecting this for about a year or so.")

      # Mixed case message with case sensitive
      advanced_search_with(message: 'I HoNeStLy', case_sensitive: true)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).not_to have_content("#stereotonetim")
      expect(page).not_to have_content("I honestly have never seen this. i'm not too shocked, I've been expecting this for about a year or so.")
    end
    it "execute an advanced search with a case insensitive regex query" do
      # Lower case regex with case insensitive
      advanced_search_with(message: '/tim(oh|guest420)/')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#stereotonetim")
      expect(page).to have_content("That Bulbasaur looks high as shit timGuest420")

      # Upper case regex with case insensitive
      advanced_search_with(message: '/TIM(OH|GUEST420)/')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#stereotonetim")
      expect(page).to have_content("That Bulbasaur looks high as shit timGuest420")

      # Matching case regex with case insensitive
      advanced_search_with(message: '/tim(Oh|Guest420)/')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#stereotonetim")
      expect(page).to have_content("That Bulbasaur looks high as shit timGuest420")

      # Mixed case regex with case insensitive
      advanced_search_with(message: '/TiM(oH|GUesT420)/')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#stereotonetim")
      expect(page).to have_content("That Bulbasaur looks high as shit timGuest420")
    end
    it "execute an advanced search with a case sensitive regex query" do
      # Lower case regex with case sensitive
      advanced_search_with(message: '/tim(oh|guest420)/', case_sensitive: true)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).not_to have_content("#stereotonetim")
      expect(page).not_to have_content("That Bulbasaur looks high as shit timGuest420")

      # Upper case regex with case sensitive
      advanced_search_with(message: '/TIM(OH|GUEST420)/', case_sensitive: true)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).not_to have_content("#stereotonetim")
      expect(page).not_to have_content("That Bulbasaur looks high as shit timGuest420")

      # Matching case regex with case sensitive
      advanced_search_with(message: '/tim(Oh|Guest420)/', case_sensitive: true)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#stereotonetim")
      expect(page).to have_content("That Bulbasaur looks high as shit timGuest420")

      # Mixed case regex with case sensitive
      advanced_search_with(message: '/TiM(oH|GUesT420)/', case_sensitive: true)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).not_to have_content("#stereotonetim")
      expect(page).not_to have_content("That Bulbasaur looks high as shit timGuest420")
    end
    it "execute an advanced search with a case insensitive query with escaped regex" do
      # Lower case escaped regex with case insensitive
      advanced_search_with(message: '\/unclosed forward slash with mixed case for regex testing')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#luna_moona")
      expect(page).to have_content("/unclosed forward slash with MiXeD cAsE for regex testing")

      # Upper case escaped regex with case insensitive
      advanced_search_with(message: '\/UNCLOSED FORWARD SLASH WITH MIXED CASE FOR REGEX TESTING')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#luna_moona")
      expect(page).to have_content("/unclosed forward slash with MiXeD cAsE for regex testing")

      # Matching case escaped regex with case insensitive
      advanced_search_with(message: '\/unclosed forward slash with MiXeD cAsE for regex testing')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#luna_moona")
      expect(page).to have_content("/unclosed forward slash with MiXeD cAsE for regex testing")

      # Mixed case escaped regex with case insensitive
      advanced_search_with(message: '\/UnClOsEd forWARD SlasH With MiXeD cAsE for REGEX testing')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#luna_moona")
      expect(page).to have_content("/unclosed forward slash with MiXeD cAsE for regex testing")
    end
    it "execute an advanced search with a case sensitive query with escaped regex" do
      # Lower case escaped regex with case sensitive
      advanced_search_with(message: '\/unclosed forward slash with mixed case for regex testing', case_sensitive: true)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).not_to have_content("#luna_moona")
      expect(page).not_to have_content("/unclosed forward slash with MiXeD cAsE for regex testing")

      # Upper case escaped regex with case sensitive
      advanced_search_with(message: '\/UNCLOSED FORWARD SLASH WITH MIXED CASE FOR REGEX TESTING', case_sensitive: true)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).not_to have_content("#luna_moona")
      expect(page).not_to have_content("/unclosed forward slash with MiXeD cAsE for regex testing")

      # Matching case escaped regex with case sensitive
      advanced_search_with(message: '\/unclosed forward slash with MiXeD cAsE for regex testing', case_sensitive: true)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#luna_moona")
      expect(page).to have_content("/unclosed forward slash with MiXeD cAsE for regex testing")

      # Mixed case escaped regex with case sensitive
      advanced_search_with(message: '\/UnClOsEd forWARD SlasH With MiXeD cAsE for REGEX testing', case_sensitive: true)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).not_to have_content("#luna_moona")
      expect(page).not_to have_content("/unclosed forward slash with MiXeD cAsE for regex testing")
    end
    it "execute an advanced search with a starting time lower than the current system time" do
      # Will return some results
      advanced_search_with(end_time: '2017-11-13 06:10')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).to have_link("Next ›", href: "/chat_logs?end_time=1510553400000&page=2")
      expect(page).to have_link("Last »", href: "/chat_logs?end_time=1510553400000&page=9")
      expect(page).not_to have_content("#luna_moona")
      expect(page).to have_content("#stereotonetim")
      expect(page).to have_content("Bench is limited to 3 this round.")
      expect(page).to have_content("That Bulbasaur looks high as shit timGuest420")

      # Will not return any results
      advanced_search_with(end_time: '2016-11-13 06:50')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).not_to have_content("#luna_moona")
      expect(page).not_to have_content("#stereotonetim")
    end
    it "execute an advanced search with a starting time greater than the current system time" do
      # This will never return any results
      year = Time.now.year + 5
      advanced_search_with(start_time: "#{year}-01-01 09:00")
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).not_to have_content("#luna_moona")
      expect(page).not_to have_content("#stereotonetim")
    end
    it "execute an advanced search with an ending time lower than the current system time" do
      # Will return some results
      advanced_search_with(start_time: '2017-11-13 06:10')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).to have_link("Next ›", href: "/chat_logs?page=2&start_time=1510553400000")
      expect(page).to have_link("Last »", href: "/chat_logs?page=5&start_time=1510553400000")
      expect(page).to have_content("#channel12345678901234567_")
      expect(page).to have_content("#luna_moona")
      expect(page).not_to have_content("#stereotonetim")
      expect(page).to have_content("/unclosed forward slash with MiXeD cAsE for regex testing")
      click_on "Next ›"
      expect(page).to have_content("#luna_moona")
      expect(page).to have_content("#stereotonetim")
      expect(page).to have_content("(closed parenthesis for regex testing)")

      # Will not return any results
      advanced_search_with(start_time: '2018-11-13 06:50')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).not_to have_content("#channel12345678901234567_")
      expect(page).not_to have_content("#luna_moona")
      expect(page).not_to have_content("#stereotonetim")
    end
    it "execute an advanced search with an ending time greater than the current system time" do
      # This will always return some results
      end_time = "#{Time.now.year + 5}-01-01 09:00" # Add 5 years to the current time
      end_time_epoch = Time.parse("#{end_time} +00:00").to_i * 1000
      advanced_search_with(end_time: end_time)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).to have_link("Next ›", href: "/chat_logs?end_time=#{end_time_epoch}&page=2")
      expect(page).to have_link("Last »", href: "/chat_logs?end_time=#{end_time_epoch}&page=13")
      expect(page).to have_content("#channel12345678901234567_")
      expect(page).to have_content("#luna_moona")
      expect(page).not_to have_content("#stereotonetim")
      expect(page).to have_content("/unclosed forward slash with MiXeD cAsE for regex testing")
      click_on "Next ›"
      expect(page).to have_content("#luna_moona")
      expect(page).to have_content("#stereotonetim")
      expect(page).to have_content("(closed parenthesis for regex testing)")
      expect(page).to have_content("gr... blindfold??")
    end
    it "execute an advanced search with a starting and ending time less than the current system time" do
      # This will always return some results
      advanced_search_with(start_time: '2017-11-13 06:00', end_time: '2017-11-13 06:10')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).to have_link("Next ›", href: "/chat_logs?end_time=1510553400000&page=2&start_time=1510552800000")
      expect(page).to have_link("Last »", href: "/chat_logs?end_time=1510553400000&page=3&start_time=1510552800000")
      expect(page).to have_content("#stereotonetim")
      expect(page).to have_content("Bench is limited to 3 this round.")
      expect(page).to have_content("That Bulbasaur looks high as shit timGuest420")
      expect(page).to have_content("you have a lvl 55 but are fighting with a 15? incYtho")

      # This will not return any results
      advanced_search_with(start_time: '2012-02-18 09:16', end_time: '2014-05-12 09:16')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).not_to have_content("#stereotonetim")
      expect(page).not_to have_content("Bench is limited to 3 this round.")
      expect(page).not_to have_content("That Bulbasaur looks high as shit timGuest420")
      expect(page).not_to have_content("you have a lvl 55 but are fighting with a 15? incYtho")
    end
    it "execute an advanced search with a starting and ending time greater than the current system time" do
      # This will never return any results
      year = Time.now.year + 5
      advanced_search_with(start_time: "#{year}-01-01 09:00", end_time: "#{year}-12-01 09:00")
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).not_to have_content("#luna_moona")
      expect(page).not_to have_content("#stereotonetim")
    end
    it "execute an advanced search with a starting ID" do
      # Starting ID 50
      advanced_search_with(start_id: 50)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("No valid parameters; executed empty search")
      expect(page).to have_content("#channel12345678901234567_")
      expect(page).to have_content("#tla")
      expect(page).to have_content("#luna_moona")
      expect(page).not_to have_content("#stereotonetim")
      expect(page).to have_content("/forward Slash test/")
      expect(page).to have_content("25 letter channel name for testing")
      expect(page).to have_content("/unclosed forward slash with MiXeD cAsE for regex testing")
      expect(page).not_to have_content("(closed parenthesis for regex testing)")
      expect(page).not_to have_content("gr... blindfold??")
      expect(page).not_to have_content("Wigglytuff too OP")
      expect(page).to have_link("Next ›", href: "/chat_logs?page=2&start_id=50")
      expect(page).to have_link("Last »", href: "/chat_logs?page=9&start_id=50")

      # Starting ID 120
      advanced_search_with(start_id: 120)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("No valid parameters; executed empty search")
      expect(page).to have_content("#channel12345678901234567_")
      expect(page).to have_content("#tla")
      expect(page).to have_content("#luna_moona")
      expect(page).not_to have_content("#stereotonetim")
      expect(page).to have_content("25 letter channel name for testing")
      expect(page).to have_content("/unclosed forward slash with MiXeD cAsE for regex testing")
      expect(page).to have_link("Next ›", href: "/chat_logs?page=2&start_id=120")
      expect(page).to have_link("Last »", href: "/chat_logs?page=2&start_id=120")
      click_on "Next ›"
      expect(page).to have_content("#luna_moona")
      expect(page).not_to have_content("#stereotonetim")
      expect(page).to have_content("(closed parenthesis for regex testing)")
      expect(page).not_to have_content("no, you clearly just made up that restriction off the top of your head")
      expect(page).not_to have_content("gr... blindfold??")
      expect(page).not_to have_content("Wigglytuff too OP")
    end
    it "execute an advanced search with a ending ID" do
      # Ending ID 50
      advanced_search_with(end_id: 50)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("No valid parameters; executed empty search")
      expect(page).not_to have_content("#channel12345678901234567_")
      expect(page).not_to have_content("#tla")
      expect(page).not_to have_content("#luna_moona")
      expect(page).to have_content("#stereotonetim")
      expect(page).not_to have_content("25 letter channel name for testing")
      expect(page).not_to have_content("/unclosed forward slash with MiXeD cAsE for regex testing")
      expect(page).not_to have_content("(closed parenthesis for regex testing)")
      expect(page).to have_content("heyooooo")
      expect(page).to have_content("I can't believe I missed it! timMinus")
      expect(page).to have_content("Never saw it coming")
      expect(page).to have_content("I've been summoned")
      expect(page).to have_link("Next ›", href: "/chat_logs?end_id=50&page=2")
      expect(page).to have_link("Last »", href: "/chat_logs?end_id=50&page=5")

      # Ending ID 120
      advanced_search_with(end_id: 120)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("No valid parameters; executed empty search")
      expect(page).not_to have_content("#channel12345678901234567_")
      expect(page).not_to have_content("#tla")
      expect(page).to have_content("#luna_moona")
      expect(page).to have_content("#stereotonetim")
      expect(page).not_to have_content("25 letter channel name for testing")
      expect(page).not_to have_content("/unclosed forward slash with MiXeD cAsE for regex testing")
      expect(page).to have_content("(closed parenthesis for regex testing)")
      expect(page).to have_content("no, you clearly just made up that restriction off the top of your head")
      expect(page).to have_content("gr... blindfold??")
      expect(page).to have_content("Wigglytuff too OP")
      expect(page).to have_link("Next ›", href: "/chat_logs?end_id=120&page=2")
      expect(page).to have_link("Last »", href: "/chat_logs?end_id=120&page=12")
    end
    it "execute an advanced search with a starting and ending ID" do
      advanced_search_with(start_id: 50, end_id: 70)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("No valid parameters; executed empty search")
      expect(page).not_to have_content("#channel12345678901234567_")
      expect(page).not_to have_content("#tla")
      expect(page).not_to have_content("#luna_moona")
      expect(page).to have_content("#stereotonetim")
      expect(page).not_to have_content("25 letter channel name for testing")
      expect(page).not_to have_content("(closed parenthesis for regex testing)")
      expect(page).not_to have_content("gr... blindfold??")
      expect(page).to have_content("I honestly have never seen this. i'm not too shocked, I've been expecting this for about a year or so.")
      expect(page).to have_content("timNofun")
      expect(page).to have_content("congrats")
      expect(page).to have_content("is your cold better or worse today?")
      expect(page).to have_link("Next ›", href: "/chat_logs?end_id=70&page=2&start_id=50")
      expect(page).to have_link("Last »", href: "/chat_logs?end_id=70&page=3&start_id=50")
    end
    it "execute an advanced search with a custom results per page" do
      # 10 per page
      advanced_search_with(per_page: 10)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("No valid parameters; executed empty search")
      expect(page).to have_content("#channel12345678901234567_")
      expect(page).to have_content("#luna_moona")
      expect(page).not_to have_content("#stereotonetim")
      expect(page).not_to have_content("#monotonetim")
      expect(page).to have_content("25 letter channel name for testing")
      expect(page).to have_content("/unclosed forward slash with MiXeD cAsE for regex testing")
      expect(page).to have_link("Next ›", href: "/chat_logs?page=2&per_page=10")
      expect(page).to have_link("Last »", href: "/chat_logs?page=13&per_page=10")
      click_on "Next ›"
      expect(page).to have_content("#luna_moona")
      expect(page).to have_content("#stereotonetim")
      expect(page).not_to have_content("#monotonetim")
      expect(page).to have_content("(closed parenthesis for regex testing)")
      expect(page).not_to have_content("eat now or go to bed")
      expect(page).not_to have_content("Wait, is this near the end of the game?")
      expect(page).not_to have_content("at least the music is still good")
      expect(page).not_to have_content("That Bulbasaur looks high as shit timGuest420")

      # 50 per page
      advanced_search_with(per_page: 50)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("No valid parameters; executed empty search")
      expect(page).to have_content("#channel12345678901234567_")
      expect(page).to have_content("#luna_moona")
      expect(page).to have_content("#stereotonetim")
      expect(page).not_to have_content("#monotonetim")
      expect(page).to have_content("/Forward slash test")
      expect(page).to have_content("25 letter channel name for testing")
      expect(page).to have_content("/unclosed forward slash with MiXeD cAsE for regex testing")
      expect(page).to have_content("(closed parenthesis for regex testing)")
      expect(page).to have_content("eat now or go to bed")
      expect(page).to have_content("Wait, is this near the end of the game?")
      expect(page).to have_content("at least the music is still good")
      expect(page).not_to have_content("That Bulbasaur looks high as shit timGuest420")
      expect(page).to have_link("Next ›", href: "/chat_logs?page=2&per_page=50")
      expect(page).to have_link("Last »", href: "/chat_logs?page=3&per_page=50")
      click_on "Next ›"
      expect(page).not_to have_content("#luna_moona")
      expect(page).to have_content("#stereotonetim")
      expect(page).to have_content("#monotonetim")
      expect(page).not_to have_content("(closed parenthesis for regex testing)")
      expect(page).not_to have_content("eat now or go to bed")
      expect(page).not_to have_content("Wait, is this near the end of the game?")
      expect(page).not_to have_content("at least the music is still good")
      expect(page).to have_content("That Bulbasaur looks high as shit timGuest420")
      expect(page).to have_content("Cant wait for the sequel")
      expect(page).to have_content("first!")
      expect(page).to have_content("he has a twitch offline chat which is invite only")
      expect(page).not_to have_content("2B when")
      expect(page).not_to have_content("it was linked in a discord server")

      # 100 per page
      advanced_search_with(per_page: 100)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("No valid parameters; executed empty search")
      expect(page).to have_content("#channel12345678901234567_")
      expect(page).to have_content("#luna_moona")
      expect(page).to have_content("#stereotonetim")
      expect(page).to have_content("#monotonetim")
      expect(page).to have_content("25 letter channel name for testing")
      expect(page).to have_content("/unclosed forward slash with MiXeD cAsE for regex testing")
      expect(page).to have_content("(closed parenthesis for regex testing)")
      expect(page).to have_content("eat now or go to bed")
      expect(page).to have_content("Wait, is this near the end of the game?")
      expect(page).to have_content("at least the music is still good")
      expect(page).to have_content("That Bulbasaur looks high as shit timGuest420")
      expect(page).to have_content("it's a card game where the rules are made up and the levels don't matter.")
      expect(page).to have_content("its the guy from Korn")
      expect(page).to have_content("I can't believe I missed it! timMinus")
      expect(page).to have_content("I've been summoned")
      expect(page).not_to have_content("thats an odd choice")
      expect(page).to have_link("Next ›", href: "/chat_logs?page=2&per_page=100")
      expect(page).to have_link("Last »", href: "/chat_logs?page=2&per_page=100")
      click_on "Next ›"
      expect(page).not_to have_content("#luna_moona")
      expect(page).not_to have_content("#stereotonetim")
      expect(page).to have_content("#monotonetim")
      expect(page).not_to have_content("I've been summoned")
      expect(page).to have_content("thats an odd choice")
      expect(page).to have_content("it was linked in a discord server")
      expect(page).to have_content("doesn't alcohol contain sugar")
      expect(page).to have_link("« First", href: "/chat_logs?per_page=100")
      expect(page).to have_link("‹ Prev", href: "/chat_logs?per_page=100")
    end
    it "execute an advanced search with multiple fields filled in" do
      advanced_search_with(sender: 'luna_moona', per_page: 11)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).to have_link("Next ›", href: "/chat_logs?page=2&per_page=11&sender=luna_moona")
      expect(page).to have_link("Last »", href: "/chat_logs?page=2&per_page=11&sender=luna_moona")
      expect(page).to have_content("#tla")
      expect(page).to have_content("#channel12345678901234567_")
      expect(page).to have_content("#luna_moona")
      expect(page).not_to have_content("#monotonetim")
      expect(page).to have_content("forward slash Test/")
      expect(page).to have_content("three letter channel name for testing")
      expect(page).to have_content("/unclosed forward slash for regex testing")
      expect(page).not_to have_content("nobody you would know")
      expect(page).not_to have_content("...we didnt need to know that")
      click_on "Next ›"
      expect(page).to have_content("« First")
      expect(page).to have_content("‹ Prev")
      expect(page).not_to have_content("Next ›")
      expect(page).not_to have_content("Last »")
      expect(page).not_to have_content("#tla")
      expect(page).not_to have_content("#channel12345678901234567_")
      expect(page).not_to have_content("#luna_moona")
      expect(page).to have_content("#monotonetim")
      expect(page).to have_content("nobody you would know")
      expect(page).to have_content("it was linked in a discord server")
      expect(page).to have_content("...we didnt need to know that")

      advanced_search_with(message: "/tim(Nofun|Oh|Plus|Minus|Guest420)/", case_sensitive: true, start_time: "2017-11-13 05:57")
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#stereotonetim")
      expect(page).to have_content("That Bulbasaur looks high as shit timGuest420")
      expect(page).to have_content("timNofun")
      expect(page).to have_content("timPlus")
      expect(page).not_to have_content("timOh")
      expect(page).to have_content("I can't believe I missed it! timMinus")

      advanced_search_with(channel: '#monotonetim', message: 'tim')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("#monotonetim")
      expect(page).to have_content("oldmario")
      expect(page).to have_content("luna_moona")
      expect(page).to have_content("tim chat is best chat")
      expect(page).to have_content("twitter.com/MonotoneTim")

      advanced_search_with(channel: '#luna_moona', sender: 'luna_moona')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).not_to have_content("#channel12345678901234567_")
      expect(page).not_to have_content("#tla")
      expect(page).not_to have_content("#monotonetim")
      expect(page).to have_content("#luna_moona")
      expect(page).not_to have_content("25 letter channel name for testing")
      expect(page).not_to have_content("three letter channel name for testing")
      expect(page).not_to have_content("nobody you would know")
      expect(page).not_to have_content("it was linked in a discord server")
      expect(page).to have_content("/unclosed forward slash with MiXeD cAsE for regex testing")
      expect(page).to have_content("/forward slashes with MiXeD cAsE for regex testing/")
      expect(page).to have_content("/unclosed forward slash for regex testing")
      expect(page).to have_content("/forward slashes for regex testing/")
      expect(page).to have_content("(closed parenthesis for regex testing)")
    end
  end

  describe "User cannot" do
    it "execute a basic search with regex" do
      basic_search_with(query: "/tim(Nofun|Oh|Plus|Minus|Guest420)/")
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).not_to have_content("timNofun")
      expect(page).not_to have_content("timOh")
      expect(page).not_to have_content("timPlus")
      expect(page).not_to have_content("timMinus")
      expect(page).not_to have_content("timGuest420")
    end
    it "search for a username with basic search" do
      basic_search_with(query: "luna_moona")
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).not_to have_content("luna_moona")

      basic_search_with(query: "luna")
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("Next ›")
      expect(page).not_to have_content("luna")
    end
    it "search for a channel with basic search" do
      basic_search_with(query: "#stereotonetim")
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("Next ›")
    end
    # For all of the advanced search items, expect flash "Failed to execute search, see errors below"
    it "execute an advanced search with a channel with an invalid format" do
      # No preceding # with >3 character channel name
      advanced_search_with(channel: 'luna_moona')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")

      # No preceding # with 3 character channel name
      advanced_search_with(channel: 'tla')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")

      # No preceding # with <3 character channel name
      advanced_search_with(channel: 'ez')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")

      # Less than 3 character channel name after the #
      advanced_search_with(channel: '#ez')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")

      # Channel name with a space
      advanced_search_with(channel: '#fake channel')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")

      # Channel name with a dash
      advanced_search_with(channel: '#fake-channel')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")

      # Channel name >25 characters
      advanced_search_with(channel: '#dddddddddddddddddddddddddd')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")

      # Channel name with mixed case
      advanced_search_with(channel: '#fAkE_ChanneL')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")

      # Channel name with all upper case letters
      advanced_search_with(channel: '#STEREOTONETIM')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")
    end
    it "execute an advanced search with a sender with an invalid format" do
      # Sender name with <3 characters
      advanced_search_with(sender: 'ez')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Sender name is in an invalid format - must contain 3-25 lowercase letters, numbers, or underscores")

      # Sender name with a space
      advanced_search_with(sender: 'fake sender')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Sender name is in an invalid format - must contain 3-25 lowercase letters, numbers, or underscores")

      # Sender name with a dash
      advanced_search_with(sender: 'fake-sender')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Sender name is in an invalid format - must contain 3-25 lowercase letters, numbers, or underscores")

      # Sender name with >26 characters
      advanced_search_with(sender: 'dddddddddddddddddddddddddd')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Sender name is in an invalid format - must contain 3-25 lowercase letters, numbers, or underscores")

      # Sender name with mixed case
      advanced_search_with(sender: 'FaKe_sender')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Sender name is in an invalid format - must contain 3-25 lowercase letters, numbers, or underscores")

      # Sender name with all upper case letters
      advanced_search_with(sender: 'FAKE_SENDER')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Sender name is in an invalid format - must contain 3-25 lowercase letters, numbers, or underscores")
    end
    it "execute an advanced search with a message with improperly escaped regex" do
      advanced_search_with(message: '/forward slashes for regex testing\/')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("Invalid regex. Your search has been run with regex disabled. Please fix your expression and try again.")
      expect(page).to have_content("No results found")
      expect(page).not_to have_content("/forward slashes for regex testing/")
      expect(page).not_to have_content("#luna_moona")
      expect(page).not_to have_content("Next ›")
    end
    it "execute an advanced search with a message with invalid regex" do
      advanced_search_with(message: '/\(closed parenthesis for regex testing)/')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("Invalid regex. Your search has been run with regex disabled. Please fix your expression and try again.")
      expect(page).not_to have_content("No results found")
      expect(page).to have_content("(closed parenthesis for regex testing)")
      expect(page).to have_content("#luna_moona")
      expect(page).not_to have_content("Next ›")

      advanced_search_with(message: '/(closed parenthesis for regex testing\)/')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("Invalid regex. Your search has been run with regex disabled. Please fix your expression and try again.")
      expect(page).not_to have_content("No results found")
      expect(page).to have_content("(closed parenthesis for regex testing)")
      expect(page).to have_content("#luna_moona")
      expect(page).not_to have_content("Next ›")
    end
    it "execute an advanced search with a starting time in an incorrect format" do
      # No date, valid time
      advanced_search_with(start_time: '13:40')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # No time, valid date
      advanced_search_with(start_time: '2018-02-27')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Valid time, invalid date - user's account is YYYY-MM-DD (system default), entered as MM-DD-YYYY
      advanced_search_with(start_time: '02-27-2018 13:40')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Valid date, invalid time - 24 hour time, entered with AM/PM
      advanced_search_with(start_time: '2018-02-27 01:40 PM')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Valid date, invalid time - entered with seconds
      advanced_search_with(start_time: '2018-02-27 13:40:51')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Valid date, invalid time - 24 hour time, entered as a negative number
      advanced_search_with(start_time: '2018-02-27 -13:40')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Invalid date and time
      advanced_search_with(start_time: '02-27-2018 -01:40:51 PM')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Invalid date and time - random text
      advanced_search_with(start_time: 'war. war never changes.')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")
    end
    it "execute an advanced search with an ending time in an incorrect format" do
      # No date, valid time
      advanced_search_with(end_time: '13:40')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # No time, valid date
      advanced_search_with(end_time: '2018-02-27')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Valid time, invalid date - user's account is YYYY-MM-DD (system default), entered as MM-DD-YYYY
      advanced_search_with(end_time: '02-27-2018 13:40')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Valid date, invalid time - 24 hour time, entered with AM/PM
      advanced_search_with(end_time: '2018-02-27 01:40 PM')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Valid date, invalid time - entered with seconds
      advanced_search_with(end_time: '2018-02-27 13:40:51')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Valid date, invalid time - 24 hour time, entered as a negative number
      advanced_search_with(end_time: '2018-02-27 -13:40')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Invalid date and time
      advanced_search_with(end_time: '02-27-2018 -01:40:51 PM')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Invalid date and time - random text
      advanced_search_with(end_time: 'war. war never changes.')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")
    end
    it "execute an advanced search with a starting and ending time in an incorrect format" do
      # No date, valid time
      advanced_search_with(start_time: '12:40', end_time: '13:40')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # No time, valid date
      advanced_search_with(start_time: '2018-02-21', end_time: '2018-02-27')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Valid time, invalid date - user's account is YYYY-MM-DD (system default), entered as MM-DD-YYYY
      advanced_search_with(start_time: '02-21-2018 12:40', end_time: '02-27-2018 13:40')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Valid date, invalid time - 24 hour time, entered with AM/PM
      advanced_search_with(start_time: '2018-02-21, 12:40 PM', end_time: '2018-02-27 01:40 PM')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Valid date, invalid time - entered with seconds
      advanced_search_with(start_time: '2018-02-21 12:40:31', end_time: '2018-02-27 13:40:51')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Valid date, invalid time - 24 hour time, entered as a negative number
      advanced_search_with(start_time: '2018-02-21 -12:40', end_time: '2018-02-27 -13:40')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Invalid date and time
      advanced_search_with(start_time: '02-21-2018 -00:40:51 AM', end_time: '02-27-2018 -01:40:51 PM')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Invalid date and time - random text
      advanced_search_with(start_time: '3.14159265359', end_time: 'war. war never changes.')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")
    end
    it "execute an advanced search with a starting time in an incorrect format and valid ending time" do
      # No date, valid time
      advanced_search_with(start_time: '12:40', end_time: '2018-02-27 13:40')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # No time, valid date
      advanced_search_with(start_time: '2018-02-21', end_time: '2018-02-27 13:40')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Valid time, invalid date - user's account is YYYY-MM-DD (system default), entered as MM-DD-YYYY
      advanced_search_with(start_time: '02-21-2018 12:40', end_time: '2018-02-27 13:40')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Valid date, invalid time - 24 hour time, entered with AM/PM
      advanced_search_with(start_time: '2018-02-21, 12:40 PM', end_time: '2018-02-27 13:40')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Valid date, invalid time - entered with seconds
      advanced_search_with(start_time: '2018-02-21 12:40:31', end_time: '2018-02-27 13:40')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Valid date, invalid time - 24 hour time, entered as a negative number
      advanced_search_with(start_time: '2018-02-21 -12:40', end_time: '2018-02-27 13:40')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Invalid date and time
      advanced_search_with(start_time: '02-21-2018 -00:40:51 AM', end_time: '2018-02-27 13:40')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Invalid date and time - random text
      advanced_search_with(start_time: '3.14159265359', end_time: '2018-02-27 13:40')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for starting time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")
    end
    it "execute an advanced search with a valid starting time and ending time in an incorrect format" do
      # No date, valid time
      advanced_search_with(start_time: '2018-02-21 12:40', end_time: '13:40')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # No time, valid date
      advanced_search_with(start_time: '2018-02-21 12:40', end_time: '2018-02-27')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Valid time, invalid date - user's account is YYYY-MM-DD (system default), entered as MM-DD-YYYY
      advanced_search_with(start_time: '2018-02-21 12:40', end_time: '02-27-2018 13:40')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Valid date, invalid time - 24 hour time, entered with AM/PM
      advanced_search_with(start_time: '2018-02-21 12:40', end_time: '2018-02-27 01:40 PM')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Valid date, invalid time - entered with seconds
      advanced_search_with(start_time: '2018-02-21 12:40', end_time: '2018-02-27 13:40:51')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Valid date, invalid time - 24 hour time, entered as a negative number
      advanced_search_with(start_time: '2018-02-21 12:40', end_time: '2018-02-27 -13:40')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Invalid date and time
      advanced_search_with(start_time: '2018-02-21 12:40', end_time: '02-27-2018 -01:40:51 PM')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")

      # Invalid date and time - random text
      advanced_search_with(start_time: '2018-02-21 12:40', end_time: 'war. war never changes.')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid format for ending time; parameter ignored (please use \"YYYY-MM-DD HH:mm\" in the future)")
    end
    it "execute an advanced search with an ending time less than the starting time" do
      advanced_search_with(start_time: '2017-11-13 06:10', end_time: '2017-11-13 06:00')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Starting time must come before ending time")
    end
    it "execute an advanced search with a starting ID less the ending ID" do
      advanced_search_with(start_id: 20, end_id: 10)
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Starting ID cannot be greater than ending ID")
    end
    it "execute an advanced search with a starting ID less than 1" do
      # Starting ID 0
      advanced_search_with(start_id: 0)
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Starting ID must be greater than 0")

      # Starting ID -100
      advanced_search_with(start_id: -100)
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Starting ID must be greater than 0")

      # Starting ID -2147483649 (current min for message ID fields is -2^31,
      # or -2147483648)
      advanced_search_with(start_id: -2147483649)
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Starting ID must be greater than 0")
    end
    it "execute an advanced search with a starting ID with a decimal" do
      # Positive decimal starting ID
      advanced_search_with(start_id: 3.14159)
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Starting ID must be an integer value greater than 0")

      # Negative decimal starting ID
      advanced_search_with(start_id: -3.14159)
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Starting ID must be an integer value greater than 0")
    end
    it "execute an advanced search with a starting ID with a non-numeric value" do
      advanced_search_with(start_id: 'ggggggg')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Starting ID must be an integer value greater than 0")
    end
    it "execute an advanced search with a starting ID greater than 2147483647" do
      # 2^31 = 2147483648, the current max for the message ID fields
      advanced_search_with(start_id: 2147483648)
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Starting ID must be less than or equal to 2147483647")
    end
    it "execute an advanced search with an ending ID less than 1" do
      # Ending ID 0
      advanced_search_with(end_id: 0)
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Ending ID must be greater than 0")

      # Ending ID -100
      advanced_search_with(end_id: -100)
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Ending ID must be greater than 0")

      # Ending ID -2147483649 (current min for message ID fields is -2^31,
      # or -2147483648)
      advanced_search_with(end_id: -2147483649)
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Ending ID must be greater than 0")
    end
    it "execute an advanced search with an ending ID with a decimal" do
      # Positive decimal ending ID
      advanced_search_with(end_id: 3.14159)
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Ending ID must be an integer value greater than 0")

      # Negative decimal ending ID
      advanced_search_with(end_id: -3.14159)
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Ending ID must be an integer value greater than 0")
    end
    it "execute an advanced search with an ending ID with a non-numeric value" do
      advanced_search_with(end_id: 'ggggggg')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Ending ID must be an integer value greater than 0")
    end
    it "execute an advanced search with an ending ID greater than 2147483647" do
      # 2^31 = 2147483648, the current max for the message ID fields
      advanced_search_with(end_id: 2147483648)
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Ending ID must be less than or equal to 2147483647")
    end
    it "execute an advanced search with non-numeric results per page" do
      advanced_search_with(per_page: 'ggggggg')
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid search results: must be a number from 10 to 1000")
    end
    it "execute an advanced search with a decimal results per page" do
      # Positive resutls per page
      advanced_search_with(per_page: 3.7)
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid search results: must be a number from 10 to 1000")

      # Negative resutls per page
      advanced_search_with(per_page: -3.7)
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid search results: must be a number from 10 to 1000")
    end
    it "execute an advanced search with results per page less than 10" do
      advanced_search_with(per_page: 5)
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid search results: must be a number from 10 to 1000")
    end
    it "execute an advanced search with results per page greater than 1000" do
      advanced_search_with(per_page: 2000)
      expect(page).to have_current_path(advanced_search_chat_logs_path)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid search results: must be a number from 10 to 1000")
    end
    it "execute an advanced search on an invalid page" do
      advanced_search_with(message: 'tim')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No results found")
      expect(page).not_to have_content("No valid parameters; executed empty search")
      expect(page).not_to have_content("#luna_moona")
      expect(page).to have_content("#stereotonetim")
      expect(page).to have_content("tim did you get sonic forces?")
      expect(page).to have_content("Tim can you explain this game, it's mighty confusing")
      expect(page).to have_content("That Bulbasaur looks high as shit timGuest420")
      expect(page).to have_link("Next ›", href: "/chat_logs?advanced_query=tim&case_sensitive=false&page=2")
      expect(page).to have_link("Last »", href: "/chat_logs?advanced_query=tim&case_sensitive=false&page=3")
      visit chat_logs_path(advanced_query: 'tim', case_sensitive: false, page: 4)
        # Go to page past where there are search results
      expect(page).to have_content("No results found")
    end
  end
end
