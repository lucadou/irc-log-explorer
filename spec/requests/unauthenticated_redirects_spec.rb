require 'rails_helper'

RSpec.describe "UnauthenticatedRedirects", type: :request do
  before :each do
    # Ensure user is not authenticated
    visit root_path
    expect(page).to have_content("Sign in")
    expect(page).to have_content("Register")
    expect(page).not_to have_content("Sign out")
  end

  describe "A logged out user can" do
    it "access the forgot password screen" do
      visit new_user_password_path
      expect(page).to have_content("Forgot your password?")
      expect(page).not_to have_content("You must be signed in to complete this action")
      expect(page).not_to have_content("Use a traditional account")
    end
    it "access the login page" do
      visit new_user_session_path
      expect(page).not_to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "access the register page" do
      visit new_user_registration_path
      expect(page).not_to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Create a traditional account")
    end
    it "view the terms of service" do
      visit policy_tos_path
      expect(page).not_to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Terms of Service")
    end
    it "view the root page" do
      visit root_path
      expect(page).not_to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Welcome!")
    end
  end

  describe "A logged out user cannot" do
    it "browse chat logs" do
      visit chat_logs_path
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "do a basic chat log search" do
      visit chat_logs_path(query: 'hello world')
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "do an advanced chat log search" do
      visit advanced_search_chat_logs_path
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "view a chat log" do
      visit chat_log_path(13)
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "edit a chat log" do
      visit edit_chat_log_path(13)
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "report a chat log" do
      visit new_report_path(msg_id: 13)
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "access the 2FA TOTP login prompt" do
      visit account_auth_2fa_totp_path
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "access the 2FA U2F login prompt" do
      visit account_auth_2fa_u2f_prompt_path
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "access the finish SSO page" do
      visit finish_sso_path
      expect(page).to have_content("You must login with a supported Oauth provider to view this page")
      expect(page).not_to have_content("Use a traditional account")
      expect(page).to have_content("Welcome!")
      expect(page).to have_current_path(root_path)
    end
    it "access SSO callbacks" do
      # Discord
      visit user_discord_omniauth_callback_path
      expect(page).to have_content("Use a traditional account")

      # Facebook
      visit user_facebook_omniauth_callback_path
      expect(page).to have_content("Use a traditional account")

      # GitHub
      visit user_github_omniauth_callback_path
      expect(page).to have_content("Use a traditional account")

      # GitLab
      visit user_gitlab_omniauth_callback_path
      expect(page).to have_content("Use a traditional account")

      # Google
      visit user_google_oauth2_omniauth_callback_path
      expect(page).to have_content("Use a traditional account")

      # Twitter
      visit user_twitter_omniauth_callback_path
      expect(page).to have_content("Use a traditional account")

      # Twitch
      visit user_twitch_omniauth_callback_path
      expect(page).to have_content("Use a traditional account")
    end
    it "access the password reset email screen" do
      visit edit_user_password_path
      expect(page).to have_content("You can't access this page without coming from a password reset email. If you do come from a password reset email, please make sure you used the full URL provided")
      expect(page).to have_content("Use a traditional account")
    end
    it "access account overview" do
      visit account_overview_path
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "access basic account settings" do
      # Both of these go to the same URLs
      visit account_settings_path
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")

      visit user_path
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "access account appearance & localization settings" do
      visit account_ui_path
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "access account notifications settings" do
      visit account_notifications_path
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "access account reports" do
      visit reports_path
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "access account security settings" do
      visit account_security_path
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "enable TOTP" do
      visit account_security_2fa_totp_path
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "manage TOTP with U2F" do
      visit account_security_2fa_totp_u2f_path
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "generate recovery codes" do
      visit account_security_2fa_recovery_codes_path
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "add a U2F token" do
      visit account_security_2fa_u2f_add_path
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "view U2F tokens" do
      visit account_security_2fa_u2f_keys_path
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "edit a U2F token" do
      # Even if the token ID doesn't exist, it should redirect to the login page
      visit account_security_2fa_u2f_edit_path(1)
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "delete a U2F token" do
      # Even if the token ID doesn't exist, it should redirect to the login page
      visit account_security_2fa_u2f_delete_with_totp_path(1)
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")

      visit account_security_2fa_u2f_delete_with_u2f_path(1)
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "access admin panel home" do
      visit admin_path
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "access admin reports" do
      visit admin_reports_path
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "view an individual report" do
      visit report_path(1)
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "edit a report" do
      visit edit_report_path(1)
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "resolve a report" do
      visit resolve_report_path(1)
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "access admin searches" do
      visit admin_searches_path
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "view an individual search" #do
    #  Functionality not implemented yet
    #  visit admin_searches_path
    #  expect(page).to have_content("You must be signed in to complete this action")
    #  expect(page).to have_content("Use a traditional account")
    #end
    it "access the admin system log" do
      visit admin_syslog_path
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "access admin system settings" do
      visit admin_sysprefs_path
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "access the user management panel" do
      visit admin_users_path
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "view the details of a user" do
      visit admin_manage_user_path(1)
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "ban a user" do
      # No test for unbans because those use the same URLs
      visit new_admin_user_ban_path(1)
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
    it "edit a ban" do
      # Ban edits are the same URL with the "type: adjust" param
      visit new_admin_user_ban_path(1, type: :adjust)
      expect(page).to have_content("You must be signed in to complete this action")
      expect(page).to have_content("Use a traditional account")
    end
  end
end
