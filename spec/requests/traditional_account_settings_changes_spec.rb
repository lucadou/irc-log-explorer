require 'rails_helper'
include AuthenticationHelpers
include SettingsHelpers

RSpec.describe "TraditionalAccountSettingsChanges", type: :request do
  before :each do
    @name = 'Test User 1'
    @email = 'testuser@domain.test'
    @password = 'CorrectHorseBatteryStaple'
    sign_up_with(@name, @email, @password, @password)
    expect(page).to have_content("Welcome! You have signed up successfully.")
    expect(page).to have_content("Terms of Service")
    click_on "Continue"
    expect(page).to have_content("Thank you for accepting the terms of service!")
    visit account_settings_path
  end

  describe "User can" do
    it "change their name to a valid name" do
      set_basic_settings_with('Test User 2', @email, current_password: @password)
      expect(page).to have_content("Your account has been updated successfully.")
      expect(page).to have_field('Nickname', with: 'Test User 2')
    end
    it "change their email to a valid email" do
      set_basic_settings_with(@name, 'testuser2@domain.test', current_password: @password)
      expect(page).to have_content("Your account has been updated successfully.")
      expect(page).to have_field('Email', with: 'testuser2@domain.test')
      switch_to_user('testuser2@domain.test', @password, expecting_after: ['Signed in successfully'])
      expect(page).to have_content("Signed in successfully")
    end
    it "add a backup email with a valid address" do
      set_basic_settings_with(@name, @email, backup_email: 'backupuser2@domain.test', current_password: @password)
      expect(page).to have_content("Your account has been updated successfully.")
      expect(page).to have_field('Backup Email', with: 'backupuser2@domain.test')
    end
    it "change their password to a valid pasword" do
      set_basic_settings_with(@name, @email, new_password: 'RealDenverCoder9', new_password_confirmation: 'RealDenverCoder9', current_password: @password)
      expect(page).to have_content("Your account has been updated successfully.")
      switch_to_user(@email, 'RealDenverCoder9', expecting_after: ['Signed in successfully'])
      expect(page).to have_content("Signed in successfully")
    end
    it "can delete their account" do
      expect(page).to have_content("Delete Account")
      click_on("Delete My Account")
      expect(current_path).to eq(root_path)
      expect(page).to have_content("Your account has been deleted.")
      sign_in_with(@email, @password, expecting_success: false)
      expect(current_path).to eq(new_user_session_path)
      expect(page).to have_content("Invalid Email or password")
    end
  end

  describe "User cannot" do
    it "change their name to a blank string" do
      set_basic_settings_with('', @email, current_password: @password)
      expect(page).to have_content("1 error prohibited this user from being saved:")
      expect(page).to have_content("Name cannot be left blank")
    end
    it "change their email address to a blank string" do
      set_basic_settings_with(@name, '', current_password: @password)
      expect(page).to have_content("1 error prohibited this user from being saved:")
      expect(page).to have_content("Email cannot be left blank")
    end
    it "change their email to an invalid email address" do
      # Ensure TLD with length of 1 character cannot be submitted (since
      # those do not exist)
      set_basic_settings_with(@name, 'a@a.a', current_password: @password)
      expect(page).to have_content("1 error prohibited this user from being saved:")
      expect(page).to have_content("Email must be a valid address")

      # Check with long email with a username and hostname, but no TLD
      set_basic_settings_with(@name, 'aaaaaaaaaa@aaaaaaa', current_password: @password)
      expect(page).to have_content("1 error prohibited this user from being saved:")
      expect(page).to have_content("Email must be a valid address")

      # Check with long email with a username and TLD but no hostname
      set_basic_settings_with(@name, 'aaaaaaaaaa@.aaaaaaaaa', current_password: @password)
      expect(page).to have_content("1 error prohibited this user from being saved:")
      expect(page).to have_content("Email must be a valid address")

      # Check with long email with a hostname and TLD but no username
      set_basic_settings_with(@name, '@aaaaaaaaaa.aaaaaaaaa', current_password: @password)
      expect(page).to have_content("1 error prohibited this user from being saved:")
      expect(page).to have_content("Email must be a valid address")

      # Check with long email with no @ symbol
      set_basic_settings_with(@name, 'aaaaaaaaaaATaaaaaaaaa.aaaaaaaaa', current_password: @password)
      expect(page).to have_content("1 error prohibited this user from being saved:")
      expect(page).to have_content("Email must be a valid address")
    end
    it "add backup email with an invalid email address" do
      # Ensure TLD with length of 1 character cannot be submitted (since
      # those do not exist)
      set_basic_settings_with(@name, @email, backup_email: 'a@a.a', current_password: @password)
      expect(page).to have_content("1 error prohibited this user from being saved:")
      expect(page).to have_content("Backup email must be a valid address")

      # Check with long email with a username and hostname, but no TLD
      set_basic_settings_with(@name, @email, backup_email: 'aaaaaaaaaa@aaaaaaa', current_password: @password)
      expect(page).to have_content("1 error prohibited this user from being saved:")
      expect(page).to have_content("Backup email must be a valid address")

      # Check with long email with a username and TLD but no hostname
      set_basic_settings_with(@name, @email, backup_email: 'aaaaaaaaaa@.aaaaaaaaa', current_password: @password)
      expect(page).to have_content("1 error prohibited this user from being saved:")
      expect(page).to have_content("Backup email must be a valid address")

      # Check with long email with a hostname and TLD but no username
      set_basic_settings_with(@name, @email, backup_email: '@aaaaaaaaaa.aaaaaaaaa', current_password: @password)
      expect(page).to have_content("1 error prohibited this user from being saved:")
      expect(page).to have_content("Backup email must be a valid address")

      # Check with long email with no @ symbol
      set_basic_settings_with(@name, @email, backup_email: 'aaaaaaaaaaATaaaaaaaaa.aaaaaaaaa', current_password: @password)
      expect(page).to have_content("1 error prohibited this user from being saved:")
      expect(page).to have_content("Backup email must be a valid address")
    end
    it "add a backup email that is the same as their primary email" do
      set_basic_settings_with(@name, @email, backup_email: @email, current_password: @password)
      expect(page).to have_content("1 error prohibited this user from being saved:")
      expect(page).to have_content("Backup email cannot be the same as your primary email")
    end
    it "change their password with valid current password and short but matching new password and confirmation" do
      set_basic_settings_with(@name, @email, new_password: 'mypass', new_password_confirmation: 'mypass', current_password: @password)
      expect(page).to have_content("1 error prohibited this user from being saved:")
      expect(page).to have_content("Password is too short (minimum is 16 characters)")
    end
    it "change their password with valid current password and valid but non-matching new password and confirmation" do
      set_basic_settings_with(@name, @email, new_password: 'CorrectHorseBatteryStaple', new_password_confirmation: 'CorrectHorseBatteryStaple2', current_password: @password)
      expect(page).to have_content("1 error prohibited this user from being saved:")
      expect(page).to have_content("Password confirmation doesn't match Password")
    end
    it "change their password with valid and matching new password and confirmation but no current password" do
      set_basic_settings_with(@name, @email, new_password: 'CorrectHorseBatteryStaple2', new_password_confirmation: 'CorrectHorseBatteryStaple2', current_password: '')
      expect(page).to have_content("1 error prohibited this user from being saved:")
      expect(page).to have_content("Current password can't be blank")
    end
    it "change their password with valid and matching new password and confirmation but invalid current password" do
      set_basic_settings_with(@name, @email, new_password: 'CorrectHorseBatteryStaple2', new_password_confirmation: 'CorrectHorseBatteryStaple2', current_password: 'InvalidPassword')
      expect(page).to have_content("1 error prohibited this user from being saved:")
      expect(page).to have_content("Current password is invalid")
    end
    it "change any settings without existing password" do
      # This has all valid values to change, except the current password
      # is empty, so none will save
      set_basic_settings_with('Test User 2', 'testuser2@domain.test', backup_email: 'testuser2-backup@domain.test', new_password: 'CorrectHorseBatteryStaple2', new_password_confirmation: 'CorrectHorseBatteryStaple2', current_password: '')
      expect(page).to have_content("1 error prohibited this user from being saved:")
      expect(page).to have_content("Current password can't be blank")
    end
    it "change any settings with incorrect existing password" do
      # This has all valid values to change, except the current password
      # is incorrect, so none will save
      set_basic_settings_with('Test User 2', 'testuser2@domain.test', backup_email: 'testuser2-backup@domain.test', new_password: 'CorrectHorseBatteryStaple2', new_password_confirmation: 'CorrectHorseBatteryStaple2', current_password: 'InvalidPassword')
      expect(page).to have_content("1 error prohibited this user from being saved:")
      expect(page).to have_content("Current password is invalid")
    end
  end
end
