require 'rails_helper'
include AuthenticationHelpers
include ReportsHelpers

RSpec.describe "UserReports", type: :request do
  before :each do
    name = 'Test User 1'
    email = 'testuser@domain.test'
    password = 'CorrectHorseBatteryStaple'
    sign_up_with(name, email, password, password)
    expect(page).to have_content("Welcome! You have signed up successfully.")
    expect(page).to have_content("Terms of Service")
    click_on "Continue"
    expect(page).to have_content("Thank you for accepting the terms of service!")
    visit chat_logs_path
  end

  describe "User can" do
    it "report a message they have not reported before" do
      # Verify a user can navigate to the new reports page, start to finish
      id = 126
      visit chat_log_path(id)
      expect(page).to have_selector(:link_or_button, "Report Message")
      # link_or_button explanation: https://stackoverflow.com/a/14377540
      click_on "Report Message"
      expect(page).not_to have_content("You have already reported this message. To view the status of your report, go to the Reports tab in the Settings menu")
      create_report_with(msg_id: id, reason: '25 letters too many', navigate_to_page: false, expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing'])
      expect(page).to have_current_path(reports_path(highlight: Report.last.id))
      expect(page).not_to have_content("Invalid report reason (must be between 1 and 255 characters)")
      expect(page).to have_content("Report was successfully created")
      expect(page).to have_content("#channel12345678901234567_")
      expect(page).to have_content("25 letter channel name for testing")
      expect(page).to have_content("25 letters too many")
      expect(page).not_to have_content("Next ›")
    end
    it "report a message they have reported once before and be warned it has already been reported once" do
      id = 126
      create_report_with(msg_id: id, reason: 'Report 1', expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing'])
      visit chat_log_path(id)
      expect(page).to have_selector(:link_or_button, "Report Message")
      # link_or_button explanation: https://stackoverflow.com/a/14377540
      click_on "Report Message"
      create_report_with(msg_id: id, reason: 'Report 2', navigate_to_page: false, expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing', 'You have already reported this message. To view the status of your report, go to the Reports tab in the Settings menu'])
      expect(page).to have_current_path(reports_path(highlight: Report.last.id))
      expect(page).not_to have_content("Invalid report reason (must be between 1 and 255 characters)")
      expect(page).to have_content("Report was successfully created")
      expect(page).to have_content("#channel12345678901234567_")
      expect(page).to have_content("25 letter channel name for testing")
      expect(page).to have_content("Report 1")
      expect(page).to have_content("Report 2")
      expect(page).not_to have_content("Next ›")
    end
    it "report a message they have reported multiple times before and be warned it has already been reported more than once" do
      id = 126
      create_report_with(msg_id: id, reason: 'Report 1', expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing'])
      create_report_with(msg_id: id, reason: 'Report 2', expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing', 'You have already reported this message. To view the status of your report, go to the Reports tab in the Settings menu'])
      visit chat_log_path(id)
      expect(page).to have_selector(:link_or_button, "Report Message")
      # link_or_button explanation: https://stackoverflow.com/a/14377540
      click_on "Report Message"
      create_report_with(msg_id: id, reason: 'Report 3', navigate_to_page: false, expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing', 'You have already reported this message 2 times. To view the status of your reports, go to the Reports tab in the Settings menu'])
      expect(page).to have_current_path(reports_path(highlight: Report.last.id))
      expect(page).not_to have_content("Invalid report reason (must be between 1 and 255 characters)")
      expect(page).to have_content("Report was successfully created")
      expect(page).to have_content("#channel12345678901234567_")
      expect(page).to have_content("25 letter channel name for testing")
      expect(page).to have_content("Report 1")
      expect(page).to have_content("Report 2")
      expect(page).to have_content("Report 3")
      expect(page).not_to have_content("Next ›")
    end
    it "report messages with a reason between 1 and 255 characters" do
      # This just tests the limits of the report length, as values between
      # 1 and 255 are checked at plenty of places above

      # 1 character reason
      id = 126
      create_report_with(msg_id: id, reason: 'E', expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing'])
      expect(page).to have_current_path(reports_path(highlight: Report.last.id))
      expect(page).not_to have_content("Invalid report reason (must be between 1 and 255 characters)")
      expect(page).to have_content("Report was successfully created")
      expect(page).to have_content("#channel12345678901234567_")
      expect(page).to have_content("25 letter channel name for testing")
      expect(page).to have_content("E")
      expect(page).not_to have_content("Next ›")
      # 255 characters reason
      create_report_with(msg_id: id, reason: 'E' * 255, expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing'])
      expect(page).to have_current_path(reports_path(highlight: Report.last.id))
      expect(page).not_to have_content("Invalid report reason (must be between 1 and 255 characters)")
      expect(page).to have_content("Report was successfully created")
      expect(page).to have_content("#channel12345678901234567_")
      expect(page).to have_content("25 letter channel name for testing")
      expect(page).to have_content("E")
      expect(page).to have_content("E" * 255)
      expect(page).not_to have_content("Next ›")
    end
    it "view reports they have created" do
      id = 126
      create_report_with(msg_id: id, reason: 'Report 1', expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing'])
      create_report_with(msg_id: id, reason: 'Report 2', expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing', 'You have already reported this message. To view the status of your report, go to the Reports tab in the Settings menu'])
      create_report_with(msg_id: id, reason: 'Report 3', expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing', 'You have already reported this message 2 times. To view the status of your reports, go to the Reports tab in the Settings menu'])
      visit reports_path
      expect(page).not_to have_content("Report was successfully created")
      expect(page).to have_content("#channel12345678901234567_")
      expect(page).to have_content("25 letter channel name for testing")
      expect(page).to have_content("Report 1")
      expect(page).to have_content("Report 2")
      expect(page).to have_content("Report 3")
      expect(page).not_to have_content("Next ›")
    end
    it "delete a report that has not been resolved" do
      id = 126
      create_report_with(msg_id: id, reason: 'Report 1', expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing'])
      create_report_with(msg_id: id, reason: 'Report 2', expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing', 'You have already reported this message. To view the status of your report, go to the Reports tab in the Settings menu'])
      create_report_with(msg_id: id, reason: 'Report 3', expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing', 'You have already reported this message 2 times. To view the status of your reports, go to the Reports tab in the Settings menu'])
      visit reports_path
      expect(page).not_to have_content("Report was successfully created")
      expect(page).to have_content("#channel12345678901234567_")
      expect(page).to have_content("25 letter channel name for testing")
      expect(page).to have_content("Report 1")
      expect(page).to have_content("Report 2")
      expect(page).to have_content("Report 3")
      last_report = Report.last.id
      expect(page.find(:css, "span[@id='report-#{last_report}-status']")).to have_content("No")
        # Ensure report has not been resolved
      delete_report(report_id: last_report)
      expect(page).to_not have_selector("report-#{last_report}-status")
      expect(page).to have_content("Report was successfully deleted")
      expect(page).to have_content("Report 1")
      expect(page).to have_content("Report 2")
      expect(page).not_to have_content("Report 3")

      # Delete other reports
      delete_report(report_id: last_report - 1)
      expect(page).to have_content("Report was successfully deleted")
      expect(page).to have_content("Report 1")
      expect(page).not_to have_content("Report 2")
      expect(page).not_to have_content("Report 3")
      delete_report(report_id: last_report - 2)
      expect(page).to have_content("Report was successfully deleted")
      expect(page).not_to have_content("Report 1")
      expect(page).not_to have_content("Report 2")
      expect(page).not_to have_content("Report 3")
    end
  end

  describe "User cannot" do
    it "report a message with no reason (a reason 0 characters long)" do
      id = 126
      create_report_with(msg_id: id, reason: '', expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing'])
      expect(page).to have_current_path(new_report_path(msg_id: id))
      expect(page).not_to have_current_path(reports_path(highlight: Report.last.id))
      expect(page).to have_content("Invalid report reason (must be between 1 and 255 characters)")
    end
    it "report a message with a reason >255 characters long" do
      id = 126
      create_report_with(msg_id: id, reason: 'E' * 256, expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing'])
      expect(page).to have_current_path(new_report_path(msg_id: id))
      expect(page).not_to have_current_path(reports_path(highlight: Report.last.id))
      expect(page).to have_content("Invalid report reason (must be between 1 and 255 characters)")
    end
    it "report a message that does not exist" do
      # Positive ID higher than any existing chat log
      id = 999
      visit new_report_path(msg_id: id)
      expect(page).to have_current_path(chat_logs_path)
      expect(page).not_to have_current_path(new_report_path(msg_id: id))
      expect(page).to have_content("Chat log with ID #{id} was not found")
      # ID of 0
      id = 0
      visit new_report_path(msg_id: id)
      expect(page).to have_current_path(chat_logs_path)
      expect(page).not_to have_current_path(new_report_path(msg_id: id))
      expect(page).to have_content("Chat log with ID #{id} was not found")
      # Negative ID
      id = -1
      visit new_report_path(msg_id: id)
      expect(page).to have_current_path(chat_logs_path)
      expect(page).not_to have_current_path(new_report_path(msg_id: id))
      expect(page).to have_content("Chat log with ID #{id} was not found")
    end
    it "report a message with a non-integer ID" do
      # Decimal message ID
      id = 12.7
      visit new_report_path(msg_id: id)
      expect(page).to have_current_path(chat_logs_path)
      expect(page).not_to have_current_path(new_report_path(msg_id: id))
      expect(page).to have_content("Chat Logs")
      expect(page).not_to have_content("Report Message")
      expect(page).to have_content("Chat log with ID #{id} was not found")
      # Non-numeric message ID
      id = 'seven'
      visit new_report_path(msg_id: id)
      expect(page).to have_current_path(chat_logs_path)
      expect(page).not_to have_current_path(new_report_path(msg_id: id))
      expect(page).to have_content("Chat log with ID #{id} was not found")
      # Blank message ID
      id = ''
      visit new_report_path(msg_id: id)
      expect(page).to have_current_path(chat_logs_path)
      expect(page).not_to have_current_path(new_report_path(msg_id: id))
      expect(page).to have_content("Chat log with ID #{id}was not found")
        # Browsers collapse whitespace, so even though in the generated HTML
        # it would be "Chat log with ID  was not found", it renders to the
        # user as "Chat log with ID was not found"
    end
    it "edit a report they created" do
      id = 126
      create_report_with(msg_id: id, reason: 'Report 1', expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing'])
      expect(page).to have_content("Report was successfully created")
      expect(page).to have_content("Report 1")
      last_report = Report.last.id
      expect(page.find(:css, "span[@id='report-#{last_report}-status']")).to have_content("No")
      visit edit_report_path(last_report)
      expect(page).to have_current_path(root_path)
      expect(page).to have_content("You are not authorized to view this page")
    end
    it "edit a report someone else created" do
      # Create report
      id = 126
      create_report_with(msg_id: id, reason: 'Report 1', expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing'])
      expect(page).to have_content("Report was successfully created")
      expect(page).to have_content("Report 1")
      last_report = Report.last.id
      expect(page.find(:css, "span[@id='report-#{last_report}-status']")).to have_content("No")
      # Sign out
      sign_out_with
      # Create another account
      sign_up_with('Other User', 'other-user@domain.example', 'hunter2hunter2hunter2', 'hunter2hunter2hunter2')
      expect(page).to have_content("Welcome! You have signed up successfully.")
      expect(page).to have_content("Terms of Service")
      click_on "Continue"
      expect(page).to have_content("Thank you for accepting the terms of service!")
      # Try to access edit page, expect it to fail
      visit edit_report_path(last_report)
      expect(page).to have_current_path(root_path)
      expect(page).to have_content("You are not authorized to view this page")
    end
    it "edit a report that does not exist" do
      # Integer report ID
      last_report = Report.last.id + 12
      visit edit_report_path(last_report)
      expect(page).to have_current_path(root_path)
      expect(page).to have_content("You are not authorized to view this page")
      # Non-integer report ID excluded because decimals are not routed by
      # Rails without regex matching, and using a float will result in a
      # non-routable path, so I am not making a test for it.
      # Non-numeric report ID
      last_report = 'eee'
      visit edit_report_path(last_report)
      expect(page).to have_current_path(root_path)
      expect(page).to have_content("You are not authorized to view this page")
    end
    it "delete a report someone else created" do
      # Create report
      id = 126
      create_report_with(msg_id: id, reason: 'Report 1', expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing'])
      expect(page).to have_content("Report was successfully created")
      expect(page).to have_content("Report 1")
      last_report = Report.last.id
      expect(page.find(:css, "span[@id='report-#{last_report}-status']")).to have_content("No")
      # Sign out
      sign_out_with
      # Create another account
      sign_up_with('Other User', 'other-user@domain.example', 'hunter2hunter2hunter2', 'hunter2hunter2hunter2')
      expect(page).to have_content("Welcome! You have signed up successfully.")
      expect(page).to have_content("Terms of Service")
      click_on "Continue"
      expect(page).to have_content("Thank you for accepting the terms of service!")
      # Try to delete the report, expect it to fail
      page.driver.submit(:delete, report_path(last_report), {})
      expect(page).to have_current_path(root_path)
      expect(page).to have_content("You are not authorized to view this page")
    end
    it "delete a report that does not exist" do
      # Integer report ID
      last_report = Report.last.id + 999
      page.driver.submit(:delete, report_path(last_report), {})
      expect(page).to have_current_path(root_path)
      expect(page).to have_content("You are not authorized to view this page")
      # No float report ID for the same reason I didn't test message edit path
      # Non-numeric message ID
      last_report = 'eee'
      page.driver.submit(:delete, report_path(last_report), {})
      expect(page).to have_current_path(root_path)
      expect(page).to have_content("You are not authorized to view this page")
    end
    it "delete a report they created after it has been resolved" do
      # Create report
      id = 126
      create_report_with(msg_id: id, reason: 'Report 1', expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing'])
      expect(page).to have_content("Report was successfully created")
      expect(page).to have_content("Report 1")
      last_report = Report.last.id
      expect(page.find(:css, "span[@id='report-#{last_report}-status']")).to have_content("No")

      # Resolve report
      resolve_report(last_report, resolving_action: 'EEEEEE')

      # Try to delete the report, expect it to fail
      visit reports_path
      expect(page.find(:css, "span[@id='report-#{last_report}-status']")).to have_content("Yes - EEEEEE")
      page.driver.submit(:delete, report_path(last_report), {})
      expect(page).to have_current_path(reports_path)
      expect(page).to have_content("Error: Cannot delete resolved report")
    end
    it "view the details of a report they created" do
      id = 126
      create_report_with(msg_id: id, reason: 'Report 1', expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing'])
      expect(page).to have_content("Report was successfully created")
      expect(page).to have_content("Report 1")
      last_report = Report.last.id
      expect(page.find(:css, "span[@id='report-#{last_report}-status']")).to have_content("No")
      visit report_path(last_report)
      expect(page).to have_current_path(root_path)
      expect(page).to have_content("You are not authorized to view this page")
    end
    it "view the details of a report someone else created" do
      # Create report
      id = 126
      create_report_with(msg_id: id, reason: 'Report 1', expecting_before: ['#channel12345678901234567_', '25 letter channel name for testing'])
      expect(page).to have_content("Report was successfully created")
      expect(page).to have_content("Report 1")
      last_report = Report.last.id
      expect(page.find(:css, "span[@id='report-#{last_report}-status']")).to have_content("No")
      # Sign out
      sign_out_with
      # Create another account
      sign_up_with('Other User', 'other-user@domain.example', 'hunter2hunter2hunter2', 'hunter2hunter2hunter2')
      expect(page).to have_content("Welcome! You have signed up successfully.")
      expect(page).to have_content("Terms of Service")
      click_on "Continue"
      expect(page).to have_content("Thank you for accepting the terms of service!")
      # Try to access view report page, expect it to fail
      visit report_path(last_report)
      expect(page).to have_current_path(root_path)
      expect(page).to have_content("You are not authorized to view this page")
    end
    it "view the details of a report that does not exist" do
      # Integer report ID
      last_report = Report.last.id + 999
      visit report_path(last_report)
      expect(page).to have_current_path(root_path)
      expect(page).to have_content("You are not authorized to view this page")
      # No float report ID for the same reason I didn't test message edit path
      # Non-numeric message ID
      last_report = 'eee'
      visit report_path(last_report)
      expect(page).to have_current_path(root_path)
      expect(page).to have_content("You are not authorized to view this page")
    end
  end
end
