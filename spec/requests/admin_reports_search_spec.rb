require 'rails_helper'
include AuthenticationHelpers
include ReportsHelpers
#include ReportsHelpers
# need to:
# add some reports to the test_seeds file
# change to login with premade admin user account
# test functionality from the user's reports view

RSpec.describe "AdminReportsSearch", type: :request do
  before :each do
    name_1 = 'Test User 1'
    email_1 = 'testuser@domain.test'
    password_1 = 'CorrectHorseBatteryStaple'
    sign_up_with(name_1, email_1, password_1, password_1)
    sign_in_with(email_1, password_1)
    (1..20).each do |n|
      create_report_with(msg_id: n, reason: "Reason #{n}")
    end
    sign_out_with()
    name_2 = 'Test User 2'
    email_2 = 'testuser2@domain.test'
    password_2 = 'CorrectHorseBatteryStaple2'
    sign_up_with(name_2, email_2, password_2, password_2)
    sign_in_with(email_2, password_2)
    (10..30).each do |n|
      create_report_with(msg_id: n, reason: "ReaSoN #{n}")
    end
    (120..124).each do |n|
      create_report_with(msg_id: n, reason: "/reAsOn #{n}")
    end
    [124, 126].each do |n|
      create_report_with(msg_id: n, reason: "/rEason #{n}/")
    end
    (126..130).each do |n|
      create_report_with(msg_id: n, reason: "reasOn #{n}/")
    end
    switch_to_user(email_1, password_1)
    (126..130).each do |n|
      create_report_with(msg_id: n, reason: "rEasOn #{n}")
    end
    name_3 = 'Test User 3'
    email_3 = 'testuser3@domain.test'
    password_3 = 'CorrectHorseBatteryStaple3'
    sign_out_with()
    sign_up_with(name_3, email_3, password_3, password_3)
    admin_email = 'admin@domain.example'
    admin_password = 'passwordpassword1'
    sign_in_with(admin_email, admin_password)
    @basic_user_1 = {username: email_1, password: password_1}
    @basic_user_2 = {username: email_2, password: password_2}
    @basic_user_3 = {username: email_3, password: password_3}
    @admin_user = {username: admin_email, password: admin_password}
    visit admin_reports_path
  end

  describe "Admin can" do
    it "see reports from all users" do
      visit admin_reports_path
      expect(page).not_to have_content("Failed to execute search, see errors below")
      # See reports from Test User 1
      expect(page).to have_content("rEasOn 130")
      expect(page).to have_content("rEasOn 128")
      expect(page).to have_content("rEasOn 126")

      # See reports from Test User 2
      expect(page).to have_content("reasOn 130/")
      expect(page).to have_content("reasOn 128/")
      expect(page).to have_content("reasOn 127/")
      expect(page).to have_link("Next ›", href: admin_reports_path(page: 2))
      expect(page).to have_link("Last »", href: admin_reports_path(page: 10))
    end
    it "search all reports with no fields filled in" do
      search_admin_reports_with()
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("rEasOn 130")
      expect(page).to have_content("rEasOn 128")
      expect(page).to have_content("rEasOn 126")
      expect(page).to have_content("reasOn 130/")
      expect(page).to have_content("reasOn 128/")
      expect(page).to have_content("reasOn 127/")
      expect(page).to have_link("Next ›", href: "/admin/reports?advanced_query=&channel=&end_time=&page=2&per_page=&reason=&resolved=all&resolver_id=&sender=&start_time=&user_id=")
      expect(page).to have_link("Last »", href: "/admin/reports?advanced_query=&channel=&end_time=&page=10&per_page=&reason=&resolved=all&resolver_id=&sender=&start_time=&user_id=")
    end
    it "search all reports with a report status" do
      # Resolve some reports
      last_report = Report.last.id
      (last_report - 15..last_report).step(3) do |n|
        resolve_report(n, resolving_action: "Resolved #{n}", switch_to_user: nil, switch_back_user: nil)
      end

      # Resolved reports
      search_admin_reports_with(report_status: 'Resolved')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      (last_report - 15..last_report).step(3) do |n|
        expect(page).to have_content("Resolved #{n}")
        expect(page).to have_link("Show", href: report_path(n))
        expect(page).not_to have_link("Show", href: report_path(n - 1))
        expect(page).not_to have_link("Show", href: report_path(n - 2))
      end

      # Unresolved reports
      search_admin_reports_with(report_status: 'Unresolved')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      (last_report - 15..last_report).step(3) do |n|
        expect(page).not_to have_content("Resolved #{n}")
        expect(page).not_to have_link("Show", href: report_path(n))
      end
      (last_report - 12..last_report).step(3) do |n|
        # I have to have separate loops because the 17th and 16th reports will
        # not show up - that would be 12 reports when it's 10 per page.
        expect(page).to have_link("Show", href: report_path(n - 1))
        expect(page).to have_link("Show", href: report_path(n - 2))
      end
    end
    it "search all reports with a reporting user ID" do
      # Search for valid user ID
      user_1 = User.where(email: @basic_user_1[:username]).first
      search_admin_reports_with(report_user_id: user_1.id)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      (126..130).each do |n|
        expect(page).to have_content("rEasOn #{n}")
      end
      (17..20).each do |n|
        expect(page).to have_content("Reason #{n}")
      end
      expect(page).to have_link("Next ›", href: "/admin/reports?advanced_query=&channel=&end_time=&page=2&per_page=&reason=&resolved=all&resolver_id=&sender=&start_time=&user_id=#{user_1.id}")
      expect(page).to have_link("Last »", href: "/admin/reports?advanced_query=&channel=&end_time=&page=3&per_page=&reason=&resolved=all&resolver_id=&sender=&start_time=&user_id=#{user_1.id}")

      # Searh for valid user email
      user_2 = User.where(email: @basic_user_2[:username]).first
      search_admin_reports_with(report_user_id: user_2.email)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      (126..130).each do |n|
        expect(page).to have_content("reasOn #{n}/")
      end
      [124, 126].each do |n|
        expect(page).to have_content("/rEason #{n}/")
      end
      (123..124).each do |n|
        expect(page).to have_content("/reAsOn #{n}")
      end
      expect(page).to have_link("Next ›", href: "/admin/reports?advanced_query=&channel=&end_time=&page=2&per_page=&reason=&resolved=all&resolver_id=&sender=&start_time=&user_id=#{ERB::Util.url_encode(user_2.email)}")
      expect(page).to have_link("Last »", href: "/admin/reports?advanced_query=&channel=&end_time=&page=4&per_page=&reason=&resolved=all&resolver_id=&sender=&start_time=&user_id=#{ERB::Util.url_encode(user_2.email)}")

      # Search for user who has not reported anything
      user_3 = User.where(email: @basic_user_3[:username]).first
      search_admin_reports_with(report_user_id: user_3.id)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No reports")
      expect(page).not_to have_content("Next ›")
    end
    it "search all reports with a message channel that exists" do
      # Channel with reported messages
      search_admin_reports_with(msg_channel: '#channel12345678901234567_')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("#luna_moona")
      expect(page).not_to have_content("#tla")
      expect(page).to have_content("rEasOn 126")
      expect(page).to have_content("reasOn 126/")
      expect(page).to have_content("/rEason 126/")

      # Channel with no reported messages
      search_admin_reports_with(msg_channel: '#tla')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No reports")
      expect(page).not_to have_content("Next ›")
    end
    it "search all reports with a message channel that does not exist" do
      # Channel is a partial match of one that exists and has reported messages
      search_admin_reports_with(msg_channel: '#channel1234567890')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No reports")
      expect(page).not_to have_content("Next ›")

      # Channel is not a partial match and has no messages
      search_admin_reports_with(msg_channel: '#nonexistentchannel')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No reports")
      expect(page).not_to have_content("Next ›")
    end
    it "search all reports with a message sender that exists" do
      # Message sender has reported messages
      search_admin_reports_with(msg_sender: 'oldmario')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      [13, 15, 18, 24, 28].each do |n|
        # These are some message IDs I selected, it's not wholly inclusive
        expect(page).to have_content("ReaSoN #{n}")
      end
      expect(page).to have_link("Next ›", href: "/admin/reports?advanced_query=&channel=&end_time=&page=2&per_page=&reason=&resolved=all&resolver_id=&sender=oldmario&start_time=&user_id=")
      expect(page).to have_link("Last »", href: "/admin/reports?advanced_query=&channel=&end_time=&page=3&per_page=&reason=&resolved=all&resolver_id=&sender=oldmario&start_time=&user_id=")

      # Message sender has no reported messages
      search_admin_reports_with(msg_sender: 'rickeon')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No reports")
      expect(page).not_to have_content("Next ›")
    end
    it "search all reports with a message sender that does not exist" do
      # Sender is a partial match of one that exists and has reported messages
      search_admin_reports_with(msg_sender: 'oldmar')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No reports")
      expect(page).not_to have_content("Next ›")

      # Sender is not a partial match and has no messages
      search_admin_reports_with(msg_sender: 'notarealuser')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No reports")
      expect(page).not_to have_content("Next ›")
    end
    it "search all reports with a case insensitive report reason without regex" do
      # Normal search that will have results
      search_admin_reports_with(report_reason: 'TexT 2')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      (20..29).each do |n|
        expect(page).to have_content("Sample text #{n}")
      end
      expect(page).to have_link("Next ›", href: "/admin/reports?advanced_query=&channel=&end_time=&page=2&per_page=&reason=TexT+2&resolved=all&resolver_id=&sender=&start_time=&user_id=")
      expect(page).to have_link("Last »", href: "/admin/reports?advanced_query=&channel=&end_time=&page=2&per_page=&reason=TexT+2&resolved=all&resolver_id=&sender=&start_time=&user_id=")

      # Normal search that will not have any results
      search_admin_reports_with(report_reason: 'Nonexistent Reason')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No reports")
      expect(page).not_to have_content("Next ›")

      # Excessively large search (ensure >255 character searches work)
      search_admin_reports_with(report_reason: 'e' * 512)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No reports")
      expect(page).not_to have_content("Next ›")
    end
    it "search all reports with a case sensitive report reason with regex" do
      # Search that will have results
      search_admin_reports_with(report_reason: '/[Rr]e[Aa]/')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No reports")
      (126..130).each do |n|
        expect(page).to have_content("reasOn #{n}/")
      end
      (120..124).each do |n|
        expect(page).to have_content("/reAsOn #{n}")
      end
      expect(page).to have_link("Next ›", href: "/admin/reports?advanced_query=&channel=&end_time=&page=2&per_page=&reason=#{ERB::Util.url_encode('/[Rr]e[Aa]/')}&resolved=all&resolver_id=&sender=&start_time=&user_id=")
      expect(page).to have_link("Last »", href: "/admin/reports?advanced_query=&channel=&end_time=&page=6&per_page=&reason=#{ERB::Util.url_encode('/[Rr]e[Aa]/')}&resolved=all&resolver_id=&sender=&start_time=&user_id=")

      # Search that will not have any results
      search_admin_reports_with(report_reason: '/\/Re[Aa]/')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No reports")
      expect(page).not_to have_content("Next ›")
    end
    it "search all reports with a case insensitive report reason with escaped regex" do
      # Search that will have results
      search_admin_reports_with(report_reason: '\/reason')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      [124, 126].each do |n|
        expect(page).to have_content("/rEason #{n}/")
      end
      (120..124).each do |n|
        expect(page).to have_content("/reAsOn #{n}")
      end
      expect(page).not_to have_content("No reports")
      expect(page).not_to have_content("Next ›")

      # Search that will not have any results
      search_admin_reports_with(report_reason: 'random search terms\/')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No reports")
      expect(page).not_to have_content("Next ›")
    end
    it "search all reports with a case insensitive message text without regex" do
      # Normal search that will have some results
      search_admin_reports_with(msg_text: 'tim')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No reports")
      [11, 13].each do |n|
        expect(page).to have_content("ReaSoN #{n}")
        expect(page).to have_content("Reason #{n}")
      end
      [21, 28, 32].each do |n|
        expect(page).to have_content("Sample text #{n}")
      end
      expect(page).to have_link("Next ›", href: "/admin/reports?advanced_query=tim&channel=&end_time=&page=2&per_page=&reason=&resolved=all&resolver_id=&sender=&start_time=&user_id=")
      expect(page).to have_link("Last »", href: "/admin/reports?advanced_query=tim&channel=&end_time=&page=2&per_page=&reason=&resolved=all&resolver_id=&sender=&start_time=&user_id=")

      # Normal search that will have no results
      search_admin_reports_with(msg_text: 'Has Anyone Really Been Far')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No reports")
      expect(page).not_to have_content("Next ›")

      # Long search to ensure >255 characters does not cause errors
      search_admin_reports_with(msg_text: 'e' * 512)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No reports")
      expect(page).not_to have_content("Next ›")
    end
    it "search all reports with a case sensitive message text with regex" do
      # Search that will have some results
      search_admin_reports_with(msg_text: '/tim(Plus|Minus|Oh)/', expecting_after: ['timPlus', 'I can\'t believe I missed it! timMinus', 'timOh'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No reports")
      expect(page).not_to have_content("Next ›")
      [14, 15, 18, 19].each do |n|
        expect(page).to have_content("Sample text #{n}")
      end

      # Search that will have no results
      search_admin_reports_with(msg_text: '/tim(plus|minus|oh)/')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No reports")
      expect(page).not_to have_content("Next ›")
    end
    it "search all reports with a case insensitive message text with escaped regex" do
      # Escaped starting forward slash
      search_admin_reports_with(msg_text: '\/forward', expecting_after: ['/forward Slash test/', '/Forward slash test', '/forward Slash test/', '/Forward slash test', '/forward slashes with MiXeD cAsE for regex testing/', '/forward slashes for regex testing/'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No reports")
      expect(page).not_to have_content("Next ›")
      [121, 123].each do |n|
        expect(page).to have_content("/reAsOn #{n}")
      end
      [128, 129].each do |n|
        expect(page).to have_content("reasOn #{n}/")
        expect(page).to have_content("rEasOn #{n}")
      end

      # Escaped starting and ending forward slashes
      search_admin_reports_with(msg_text: '\/forward slash test\/', expecting_after: ['/forward Slash test/', 'rEasOn 129', 'reasOn 129/'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No reports")
      expect(page).not_to have_content("Next ›")

      # Escaped ending forward slash
      search_admin_reports_with(msg_text: 'test\/', expecting_after: ['forward slash Test/', '/forward Slash test/', 'forward slash Test/', '/forward Slash test/'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No reports")
      expect(page).not_to have_content("ReaSoN 23")
      expect(page).not_to have_content("http://browserbench.org/Speedometer/")
      expect(page).not_to have_content("Next ›")
      [129, 130].each do |n|
        expect(page).to have_content("rEasOn #{n}")
        expect(page).to have_content("reasOn #{n}/")
      end
      # These would show up if you searched for "testing\/"
      expect(page).not_to have_content("unclosed ending forward slash for regex testing/")
      expect(page).not_to have_content("rEasOn 127")
      expect(page).not_to have_content("/forward slashes with MiXeD cAsE for regex testing/")
      expect(page).not_to have_content("/reAsOn 123")
      expect(page).not_to have_content("/forward slashes for regex testing/")
      expect(page).not_to have_content("/reAsOn 121")
    end
    it "search all reports with a resolving user ID" do
      # Create a new admin user
      admin_user_2 = {:name => 'Admin 2', :email => 'admin2@domain.example', :password => 'passwordpassword2', :password_confirmation => 'passwordpassword2', :role => 'admin'}
      User.create!(admin_user_2)
      admin_2 = User.where(email: admin_user_2[:email]).first
      UserPreference.create!({user_id: admin_2.id})

      switch_to_user(admin_user_2[:email], admin_user_2[:password])
      # Resolve some reports with new admin user
      last_report = Report.last.id
      (last_report - 3..last_report).each do |n|
        resolve_report(n, resolving_action: "Resolved #{n}", switch_to_user: nil, switch_back_user: nil)
      end
      # Resolve some reports with old admin user
      switch_to_user(@admin_user[:username], @admin_user[:password])
      last_report = Report.last.id
      (last_report - 6..last_report - 4).each do |n|
        resolve_report(n, resolving_action: "Resolved #{n}", switch_to_user: nil, switch_back_user: nil)
      end

      # Search for valid user ID who has resolved something (old admin)
      admin_1 = User.where(email: @admin_user[:username]).first
      search_admin_reports_with(report_resolver_id: admin_1.id, expecting_after: ['rEasOn 126', 'reasOn 130/', 'reasOn 129/', 'Sample text 3'])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No reports")
      expect(page).not_to have_content("Next ›")
      (last_report - 3..last_report).each do |n|
        # New admin resolved reports
        expect(page).not_to have_content("Yes - Resolved #{n}")
      end
      (last_report - 6..last_report - 4).each do |n|
        # Old admin resolved reports
        expect(page).to have_content("Yes - Resolved #{n}")
      end
      (127..130).each do |n|
        # New admin resolved reports
        expect(page).not_to have_content("rEasOn #{n}")
      end

      # Search for valid user email who has resolved something
      search_admin_reports_with(report_resolver_id: admin_user_2[:email])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No reports")
      expect(page).not_to have_content("Next ›")
      (last_report - 3..last_report).each do |n|
        # Old admin resolved reports
        expect(page).to have_content("Yes - Resolved #{n}")
      end
      (last_report - 6..last_report - 4).each do |n|
        # New admin resolved reports
        expect(page).not_to have_content("Yes - Resolved #{n}")
      end
      (127..130).each do |n|
        # New admin resolved reports
        expect(page).to have_content("rEasOn #{n}")
      end

      # Search for user who has not resolved anything
      search_admin_reports_with(report_resolver_id: @basic_user_1[:username])
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("No reports")
      expect(page).not_to have_content("Next ›")
    end
    it "search all reports with a starting reported time before the current system time" do
      # Adjust a timestamp
      report = Report.find(Report.last.id - 5)
      old_report_time = report.created_at
      report.created_at = Time.now.utc - 5.minutes
      report.save!

      # Search for some reports
      search_admin_reports_with(start_time: (Time.now.utc - 3.minutes).strftime('%Y-%m-%d %H:%M'))
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No reports")
      expect(page).to have_content("/rEason 126/")
      expect(page).to have_content("rEasOn 130")
      expect(page).not_to have_content("reasOn 130")
      (126..129).each do |n|
        expect(page).to have_content("rEasOn #{n}")
        expect(page).to have_content("reasOn #{n}")
      end
      expect(page).to have_link("Next ›", href: "/admin/reports?advanced_query=&channel=&end_time=&page=2&per_page=&reason=&resolved=all&resolver_id=&sender=&start_time=#{(Time.now.utc - 3.minutes).strftime('%Y-%m-%d')}+#{ERB::Util.url_encode((Time.now.utc - 3.minutes).strftime('%H:%M'))}&user_id=")
      expect(page).to have_link("Last »", href: "/admin/reports?advanced_query=&channel=&end_time=&page=6&per_page=&reason=&resolved=all&resolver_id=&sender=&start_time=#{(Time.now.utc - 3.minutes).strftime('%Y-%m-%d')}+#{ERB::Util.url_encode((Time.now.utc - 3.minutes).strftime('%H:%M'))}&user_id=")

      # Revert timestamp change
      report.created_at = old_report_time
      report.save!

      # Search for all reports
      search_admin_reports_with(start_time: (Time.now.utc - 3.minutes).strftime('%Y-%m-%d %H:%M'))
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No reports")
      expect(page).not_to have_content("/rEason 126/")
      (126..130).each do |n|
        expect(page).to have_content("rEasOn #{n}")
        expect(page).to have_content("reasOn #{n}")
      end
      expect(page).to have_link("Next ›", href: "/admin/reports?advanced_query=&channel=&end_time=&page=2&per_page=&reason=&resolved=all&resolver_id=&sender=&start_time=#{(Time.now.utc - 3.minutes).strftime('%Y-%m-%d')}+#{ERB::Util.url_encode((Time.now.utc - 3.minutes).strftime('%H:%M'))}&user_id=")
      expect(page).to have_link("Last »", href: "/admin/reports?advanced_query=&channel=&end_time=&page=6&per_page=&reason=&resolved=all&resolver_id=&sender=&start_time=#{(Time.now.utc - 3.minutes).strftime('%Y-%m-%d')}+#{ERB::Util.url_encode((Time.now.utc - 3.minutes).strftime('%H:%M'))}&user_id=")
    end
    it "search all reports with a ending reported time before the current system time" do
      # Adjust a timestamp
      report = Report.find(Report.last.id - 5)
      old_report_time = report.created_at
      report.created_at = Time.now.utc - 4.minutes
      report.save!

      # Search for some reports
      #search_admin_reports_with(report_reason: 'reason 130')
      #puts "Search for 'reason 130':\n#{page.html}"
      search_admin_reports_with(end_time: (Time.now.utc - 3.minutes).strftime('%Y-%m-%d %H:%M'))
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No reports")
      #puts "debug: old_report_time=#{old_report_time.utc} new=#{report.created_at.utc} reason=#{report.report_reason} current time=#{Time.now.utc}"
      #puts page.html
      expect(page).to have_content("reasOn 130/")
      (27..35).each do |n|
        expect(page).to have_content("Sample text #{n}")
      end
      expect(page).not_to have_content("Sample text 26")
      expect(page).to have_link("Next ›", href: "/admin/reports?advanced_query=&channel=&end_time=#{(Time.now.utc - 3.minutes).strftime('%Y-%m-%d')}+#{ERB::Util.url_encode((Time.now.utc - 3.minutes).strftime('%H:%M'))}&page=2&per_page=&reason=&resolved=all&resolver_id=&sender=&start_time=&user_id=")
      expect(page).to have_link("Last »", href: "/admin/reports?advanced_query=&channel=&end_time=#{(Time.now.utc - 3.minutes).strftime('%Y-%m-%d')}+#{ERB::Util.url_encode((Time.now.utc - 3.minutes).strftime('%H:%M'))}&page=4&per_page=&reason=&resolved=all&resolver_id=&sender=&start_time=&user_id=")

      # Revert timestamp change
      report.created_at = old_report_time
      report.save!

      # Search for all reports
      search_admin_reports_with(end_time: (Time.now.utc - 3.minutes).strftime('%Y-%m-%d %H:%M'))
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No reports")
      expect(page).not_to have_content("reasOn 130/")
      (26..35).each do |n|
        expect(page).to have_content("Sample text #{n}")
      end
      expect(page).to have_link("Next ›", href: "/admin/reports?advanced_query=&channel=&end_time=#{(Time.now.utc - 3.minutes).strftime('%Y-%m-%d')}+#{ERB::Util.url_encode((Time.now.utc - 3.minutes).strftime('%H:%M'))}&page=2&per_page=&reason=&resolved=all&resolver_id=&sender=&start_time=&user_id=")
      expect(page).to have_link("Last »", href: "/admin/reports?advanced_query=&channel=&end_time=#{(Time.now.utc - 3.minutes).strftime('%Y-%m-%d')}+#{ERB::Util.url_encode((Time.now.utc - 3.minutes).strftime('%H:%M'))}&page=4&per_page=&reason=&resolved=all&resolver_id=&sender=&start_time=&user_id=")
    end
    it "search all reports with a starting and ending reported time before the current system time" do
      # Adjust a few timestamps
      last_report = Report.last.id
      (last_report - 3..last_report).each do |n|
        report = Report.find(n)
        report.created_at = report.created_at - 4.minutes
        report.save!
      end

      # Search for some reports
      search_admin_reports_with(start_time: (Time.now.utc - 6.minutes).strftime('%Y-%m-%d %H:%M'), end_time: (Time.now.utc - 2.minutes).strftime('%Y-%m-%d %H:%M'))
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No reports")
      expect(page).not_to have_content("Next ›")
      (127..130).each do |n|
        expect(page).to have_content("rEasOn #{n}")
      end
      (121..126).each do |n|
        expect(page).not_to have_content("rEasOn #{n}")
      end
    end
    it "search all reports with a custom results per page" do
      # 11 per page
      search_admin_reports_with(per_page: 11)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No reports")
      (126..130).each do |n|
        expect(page).to have_content("rEasOn #{n}")
      end
      (126..130).each do |n|
        expect(page).to have_content("reasOn #{n}/")
      end
      expect(page).to have_content("/rEason 126/")
      expect(page).not_to have_content("/rEason 124/")
      (120..124).each do |n|
        expect(page).not_to have_content("/reAsOn #{n}")
      end
      (18..30).each do |n|
        expect(page).not_to have_content("ReaSoN #{n}")
      end
      expect(page).to have_link("Next ›", href: "/admin/reports?advanced_query=&channel=&end_time=&page=2&per_page=11&reason=&resolved=all&resolver_id=&sender=&start_time=&user_id=")
      expect(page).to have_link("Last »", href: "/admin/reports?advanced_query=&channel=&end_time=&page=9&per_page=11&reason=&resolved=all&resolver_id=&sender=&start_time=&user_id=")


      # 15 per page
      search_admin_reports_with(per_page: 15)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No reports")
      (126..130).each do |n|
        expect(page).to have_content("rEasOn #{n}")
      end
      (126..130).each do |n|
        expect(page).to have_content("reasOn #{n}/")
      end
      [124, 126].each do |n|
        expect(page).to have_content("/rEason #{n}/")
      end
      (122..124).each do |n|
        expect(page).to have_content("/reAsOn #{n}")
      end
      (120..121).each do |n|
        expect(page).not_to have_content("/reAsOn #{n}")
      end
      (18..30).each do |n|
        expect(page).not_to have_content("ReaSoN #{n}")
      end
      expect(page).to have_link("Next ›", href: "/admin/reports?advanced_query=&channel=&end_time=&page=2&per_page=15&reason=&resolved=all&resolver_id=&sender=&start_time=&user_id=")
      expect(page).to have_link("Last »", href: "/admin/reports?advanced_query=&channel=&end_time=&page=7&per_page=15&reason=&resolved=all&resolver_id=&sender=&start_time=&user_id=")

      # 30 per page
      search_admin_reports_with(per_page: 30)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No reports")
      (126..130).each do |n|
        expect(page).to have_content("rEasOn #{n}")
      end
      (126..130).each do |n|
        expect(page).to have_content("reasOn #{n}/")
      end
      [124, 126].each do |n|
        expect(page).to have_content("/rEason #{n}/")
      end
      (120..124).each do |n|
        expect(page).to have_content("/reAsOn #{n}")
      end
      (18..30).each do |n|
        expect(page).to have_content("ReaSoN #{n}")
      end
      (10..17).each do |n|
        expect(page).not_to have_content("ReaSoN #{n}")
      end
      expect(page).to have_link("Next ›", href: "/admin/reports?advanced_query=&channel=&end_time=&page=2&per_page=30&reason=&resolved=all&resolver_id=&sender=&start_time=&user_id=")
      expect(page).to have_link("Last »", href: "/admin/reports?advanced_query=&channel=&end_time=&page=4&per_page=30&reason=&resolved=all&resolver_id=&sender=&start_time=&user_id=")

      # 1000 per page
      search_admin_reports_with(per_page: 1000)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No reports")
      expect(page).not_to have_content("Next ›")
      (126..130).each do |n|
        expect(page).to have_content("rEasOn #{n}")
      end
      (126..130).each do |n|
        expect(page).to have_content("reasOn #{n}/")
      end
      [124, 126].each do |n|
        expect(page).to have_content("/rEason #{n}/")
      end
      (120..124).each do |n|
        expect(page).to have_content("/reAsOn #{n}")
      end
      (10..30).each do |n|
        expect(page).to have_content("ReaSoN #{n}")
      end
      expect(page).to have_content("Sample text")
    end
    it "search all reports with multiple fields filled in" do
      # Modify some report creation dates
      last_report = Report.last.id
      current_report = 30
      (last_report - 30..last_report).each do |n|
        report = Report.find(n)
        report.created_at = Time.now.utc - current_report.minutes
        current_report -= 1
      end

      # Resolve a few reports
      (last_report - 30..last_report).step(4) do |n|
        resolve_report(n, resolving_action: "ReSolveD/ #{n}", switch_to_user: nil, switch_back_user: nil)
      end
      (last_report - 28..last_report).step(4) do |n|
        resolve_report(n, resolving_action: "/resoLveD #{n}", switch_to_user: nil, switch_back_user: nil)
      end

      # Status, date end, per page
      search_admin_reports_with(report_status: 'Resolved', end_time: (Time.now.utc - 9.minutes).strftime('%Y-%m-%d %H:%M'), per_page: 12)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No reports")
      expect(page).not_to have_content("Next ›")
      expect(page).to have_content("Sample text 3")
      expect(page).to have_content("Tim can you explain this game, it's mighty confusing")
      expect(page).to have_content("Yes - No action taken")

      # Message channel, per page
      search_admin_reports_with(msg_channel: '#monotonetim', per_page: 18)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No reports")
      (13..30).each do |n|
        expect(page).to have_content("ReaSoN #{n}")
      end
      expect(page).to have_link("Next ›", href: "/admin/reports?advanced_query=&channel=%23monotonetim&end_time=&page=2&per_page=18&reason=&resolved=all&resolver_id=&sender=&start_time=&user_id=")
      expect(page).to have_link("Last »", href: "/admin/reports?advanced_query=&channel=%23monotonetim&end_time=&page=3&per_page=18&reason=&resolved=all&resolver_id=&sender=&start_time=&user_id=")

      # Message sender, message text
      search_admin_reports_with(msg_channel: '#luna_moona', per_page: 18)
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No reports")
      expect(page).not_to have_content("Next ›")
      (127..130).each do |n|
        expect(page).to have_content("rEasOn #{n}")
      end
      (127..130).each do |n|
        expect(page).to have_content("reasOn #{n}/")
      end
      (120..124).each do |n|
        expect(page).to have_content("/reAsOn #{n}")
      end

      # Status, message sender
      search_admin_reports_with(report_status: 'Unresolved', msg_sender: 'luna_moona')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No reports")
      expect(page).not_to have_content("oldmario")
      [127, 129].each do |n|
        expect(page).to have_content("rEasOn #{n}")
      end
      [126, 128, 130].each do |n|
        expect(page).to have_content("reasOn #{n}/")
      end
      [121, 123].each do |n|
        expect(page).to have_content("/reAsOn #{n}")
      end
      [16, 20].each do |n|
        expect(page).to have_content("ReaSoN #{n}")
      end
      expect(page).to have_link("Next ›", href: "/admin/reports?advanced_query=&channel=&end_time=&page=2&per_page=&reason=&resolved=unresolved&resolver_id=&sender=luna_moona&start_time=&user_id=")
      expect(page).to have_link("Last »", href: "/admin/reports?advanced_query=&channel=&end_time=&page=2&per_page=&reason=&resolved=unresolved&resolver_id=&sender=luna_moona&start_time=&user_id=")

      # Date start, message text (regex)
      search_admin_reports_with(start_time: (Time.now.utc - 15.minutes).strftime('%Y-%m-%d %H:%M'), msg_text: '/(slash|Slash)/')
      expect(page).not_to have_content("Failed to execute search, see errors below")
      expect(page).not_to have_content("No reports")
      (127..130).each do |n|
        expect(page).to have_content("rEasOn #{n}")
      end
      (127..130).each do |n|
        expect(page).to have_content("reasOn #{n}/")
      end
      expect(page).to have_content("/reAsOn 124")
      expect(page).to have_link("Next ›", href: "/admin/reports?advanced_query=%2F%28slash%7CSlash%29%2F&channel=&end_time=&page=2&per_page=&reason=&resolved=all&resolver_id=&sender=&start_time=#{(Time.now.utc - 15.minutes).strftime('%Y-%m-%d')}+#{ERB::Util.url_encode((Time.now.utc - 15.minutes).strftime('%H:%M'))}&user_id=")
      expect(page).to have_link("Last »", href: "/admin/reports?advanced_query=%2F%28slash%7CSlash%29%2F&channel=&end_time=&page=2&per_page=&reason=&resolved=all&resolver_id=&sender=&start_time=#{(Time.now.utc - 15.minutes).strftime('%Y-%m-%d')}+#{ERB::Util.url_encode((Time.now.utc - 15.minutes).strftime('%H:%M'))}&user_id=")
    end
  end

  describe "Admin cannot" do
    it "search all reports with an invalid report status" do
      # There is no good way to force a new value into the dropdown menu
      # without using JS to modify the page, but since this is a simple GET,
      # I can just set a custom report status in the URL
      visit admin_reports_path(utf8: URI.encode('✓'), resolved: 'aaaAAAAAAaaaaaaaa', expecting_before: ['rEasOn 130', 'rEasOn 126', 'reasOn 130/', 'reasOn 127'], expecting_after: ['rEasOn 130', 'rEasOn 126', 'reasOn 130/', 'reasOn 127'])
        # Without the URI.encode, I get a URI::InvalidURIError due to having
        # non-ASCII characters. This ocurs even if I do utf8: '\u2713', so
        # there is no way around using URI.encode.
        # https://stackoverflow.com/a/50158549
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid report status, please select an option from the dropdown menu")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Check to make sure changing capitalization of valid statuses results
      # in an error
      visit admin_reports_path(utf8: URI.encode('✓'), resolved: 'Resolved')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid report status, please select an option from the dropdown menu")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Check to make sure an absurdly long status displays an error message
      visit admin_reports_path(utf8: URI.encode('✓'), resolved: 'e' * 256)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid report status, please select an option from the dropdown menu")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with an invalid report reason regex" do
      # Unopened parenthesis
      search_admin_reports_with(report_reason: '/\(closed parenthesis for regex testing)/')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid regex in reporting reason, please double check your query. Error details: unmatched close parenthesis")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Unclosed parenthesis
      search_admin_reports_with(report_reason: '/(closed parenthesis for regex testing\)/')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid regex in reporting reason, please double check your query. Error details: end pattern with unmatched parenthesis")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Unclosed square brackets
      search_admin_reports_with(report_reason: '/[closed parenthesis for regex testing\)/')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid regex in reporting reason, please double check your query. Error details: premature end of char-class")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with an invalid reporting user email ID" do
      # Invalid format - ASCII
      search_admin_reports_with(report_user_id: 'aaaaaaaaaaa')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid reporting user ID, no user was found with the given ID or email address")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Invalid format - partial email addresses
      search_admin_reports_with(report_user_id: 'a@a')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid reporting user ID, no user was found with the given ID or email address")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with a nonexistent reporting user email ID" do
      # No user exists with that email address
      search_admin_reports_with(report_user_id: 'notauser@domain.example', expecting_before: ['rEasOn 130', 'rEasOn 126', 'reasOn 130/', 'reasOn 127'], expecting_after: ['rEasOn 130', 'rEasOn 126', 'reasOn 130/', 'reasOn 127'])
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid reporting user ID, no user was found with the given ID or email address")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with a nonexistent reporting user numerical ID" do
      # No user exists with that ID
      search_admin_reports_with(report_user_id: User.last.id + 10, expecting_before: ['rEasOn 130', 'rEasOn 126', 'reasOn 130/', 'reasOn 127'], expecting_after: ['rEasOn 130', 'rEasOn 126', 'reasOn 130/', 'reasOn 127'])
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid reporting user ID, no user was found with the given ID or email address")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with an invalid reporting user numerical ID" do
      # Decimal ID
      search_admin_reports_with(report_user_id: 9.5)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid reporting user ID, no user was found with the given ID or email address")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with a reporting user numerical ID greater than 2147483647" do
      # User ID > 2^63-1 - make sure no weird errors occur
      search_admin_reports_with(report_user_id: 9223372036854775810)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid reporting user ID, no user was found with the given ID or email address")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with a zero reporting user numerical ID" do
      search_admin_reports_with(report_user_id: 0)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid reporting user ID, no user was found with the given ID or email address")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with a negative reporting user numerical ID" do
      search_admin_reports_with(report_user_id: -9)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid reporting user ID, no user was found with the given ID or email address")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with a reporting user numerical ID less than -2147483648" do
      # User ID < -2^63 - make sure no weird errors occur
      search_admin_reports_with(report_user_id: -9223372036854775810)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid reporting user ID, no user was found with the given ID or email address")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with an invalid message channel format" do
      # No preceding # with >3 character channel name
      search_admin_reports_with(msg_channel: 'luna_moona')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # No preceding # with 3 character channel name
      search_admin_reports_with(msg_channel: 'tla')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # No preceding # with <3 character channel name
      search_admin_reports_with(msg_channel: 'ez')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Less than 3 character channel name after the #
      search_admin_reports_with(msg_channel: '#ez')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Channel name with a space
      search_admin_reports_with(msg_channel: '#fake channel')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Channel name with a dash
      search_admin_reports_with(msg_channel: '#fake-channel')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Channel name >25 characters
      search_admin_reports_with(msg_channel: '#dddddddddddddddddddddddddd')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Channel name with mixed case
      search_admin_reports_with(msg_channel: '#fAkE_ChanneL')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Channel name with all upper case letters
      search_admin_reports_with(msg_channel: '#STEREOTONETIM')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with an invalid message sender format" do
      # Sender name with <3 characters
      search_admin_reports_with(msg_sender: 'ez')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Sender name is in an invalid format - must contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Sender name with a space
      search_admin_reports_with(msg_sender: 'fake sender')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Sender name is in an invalid format - must contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Sender name with a dash
      search_admin_reports_with(msg_sender: 'fake-sender')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Sender name is in an invalid format - must contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Sender name with >26 characters
      search_admin_reports_with(msg_sender: 'dddddddddddddddddddddddddd')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Sender name is in an invalid format - must contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Sender name with mixed case
      search_admin_reports_with(msg_sender: 'FaKe_sender')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Sender name is in an invalid format - must contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Sender name with all upper case letters
      search_admin_reports_with(msg_sender: 'FAKE_SENDER')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Sender name is in an invalid format - must contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with an invalid message text regex" do
      # Unopened parenthesis
      search_admin_reports_with(msg_text: '/\(closed parenthesis for regex testing)/')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid regex in message text, please double check your query. Error details: unmatched close parenthesis")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Unclosed parenthesis
      search_admin_reports_with(msg_text: '/(closed parenthesis for regex testing\)/')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid regex in message text, please double check your query. Error details: end pattern with unmatched parenthesis")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Unclosed square brackets
      search_admin_reports_with(msg_text: '/[closed parenthesis for regex testing\)/')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid regex in message text, please double check your query. Error details: premature end of char-class")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with an invalid resolving user email ID" do
      # No user exists with that email address
      search_admin_reports_with(report_resolver_id: 'aaaaaaaaaaa')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid resolving user ID, no user was found with the given ID or email address")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with a nonexistent resolving user email ID" do
      # No user exists with that email address
      search_admin_reports_with(report_resolver_id: 'notarealuser@domain.example')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid resolving user ID, no user was found with the given ID or email address")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with a nonexistent resolving user numerical ID" do
      # No user exists with that ID
      search_admin_reports_with(report_resolver_id: User.last.id + 10)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid resolving user ID, no user was found with the given ID or email address")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with an invalid resolving user numerical ID" do
      # Decimal ID
      search_admin_reports_with(report_resolver_id: 9.5)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid resolving user ID, no user was found with the given ID or email address")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with a resolving user numerical ID greater than 2147483647" do
      # User ID > 2^63-1 - make sure no weird errors occur
      search_admin_reports_with(report_resolver_id: 9223372036854775810)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid resolving user ID, no user was found with the given ID or email address")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with a zero resolving user numerical ID" do
      search_admin_reports_with(report_resolver_id: 0)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid resolving user ID, no user was found with the given ID or email address")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with a negative resolving user numerical ID" do
      search_admin_reports_with(report_resolver_id: -9)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid resolving user ID, no user was found with the given ID or email address")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with a resolving user numerical ID less than -2147483648" do
      # User ID < -2^63 - make sure no weird errors occur
      search_admin_reports_with(report_resolver_id: -9223372036854775810)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid resolving user ID, no user was found with the given ID or email address")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with a starting reported time after the current system time" do
      year = Time.now.year + 5
      search_admin_reports_with(start_time: "#{year}-01-01 09:00", expecting_before: ['rEasOn 130', 'rEasOn 126', 'reasOn 130/', 'reasOn 127'], expecting_after: ['rEasOn 130', 'rEasOn 126', 'reasOn 130/', 'reasOn 127'])
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Starting time cannot be greater than the current system time")
      expect(page).not_to have_content("Ending time cannot be greater than the current system time")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with an ending reported time after the current system time" do
      year = Time.now.year + 5
      search_admin_reports_with(end_time: "#{year}-12-01 09:00", expecting_before: ['rEasOn 130', 'rEasOn 126', 'reasOn 130/', 'reasOn 127'], expecting_after: ['rEasOn 130', 'rEasOn 126', 'reasOn 130/', 'reasOn 127'])
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).not_to have_content("Starting time cannot be greater than the current system time")
      expect(page).to have_content("Ending time cannot be greater than the current system time")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with a starting and ending reported time after the current system time" do
      year = Time.now.year + 5
      search_admin_reports_with(start_time: "#{year}-01-01 09:00", end_time: "#{year}-12-01 09:00", expecting_before: ['rEasOn 130', 'rEasOn 126', 'reasOn 130/', 'reasOn 127'], expecting_after: ['rEasOn 130', 'rEasOn 126', 'reasOn 130/', 'reasOn 127'])
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Starting time cannot be greater than the current system time")
      expect(page).to have_content("Ending time cannot be greater than the current system time")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with an starting time after the ending time" do
      search_admin_reports_with(start_time: '2017-11-13 06:10', end_time: '2017-11-13 06:00', expecting_before: ['rEasOn 130', 'rEasOn 126', 'reasOn 130/', 'reasOn 127'], expecting_after: ['rEasOn 130', 'rEasOn 126', 'reasOn 130/', 'reasOn 127'])
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Ending time must be greater than the starting time")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with an invalid reported time start format" do
      # No date, valid time
      search_admin_reports_with(start_time: '13:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # No time, valid date
      search_admin_reports_with(start_time: '2018-02-27')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Valid time, invalid date - user's account is YYYY-MM-DD (system default), entered as MM-DD-YYYY
      search_admin_reports_with(start_time: '02-27-2018 13:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Valid date, invalid time - 24 hour time, entered with AM/PM
      search_admin_reports_with(start_time: '2018-02-27 01:40 PM')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Valid date, invalid time - entered with seconds
      search_admin_reports_with(start_time: '2018-02-27 13:40:51')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Valid date, invalid time - 24 hour time, entered as a negative number
      search_admin_reports_with(start_time: '2018-02-27 -13:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Invalid date and time
      search_admin_reports_with(start_time: '02-27-2018 -01:40:51 PM')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Invalid date and time - random text
      search_admin_reports_with(start_time: 'war. war never changes.')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with an invalid reported time end format" do
      # No date, valid time
      search_admin_reports_with(end_time: '13:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # No time, valid date
      search_admin_reports_with(end_time: '2018-02-27')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Valid time, invalid date - user's account is YYYY-MM-DD (system default), entered as MM-DD-YYYY
      search_admin_reports_with(end_time: '02-27-2018 13:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Valid date, invalid time - 24 hour time, entered with AM/PM
      search_admin_reports_with(end_time: '2018-02-27 01:40 PM')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Valid date, invalid time - entered with seconds
      search_admin_reports_with(end_time: '2018-02-27 13:40:51')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Valid date, invalid time - 24 hour time, entered as a negative number
      search_admin_reports_with(end_time: '2018-02-27 -13:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Invalid date and time
      search_admin_reports_with(end_time: '02-27-2018 -01:40:51 PM')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Invalid date and time - random text
      search_admin_reports_with(end_time: 'war. war never changes.')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with invalid reported time start and end formats" do
      # No date, valid time
      search_admin_reports_with(start_time: '12:40', end_time: '13:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # No time, valid date
      search_admin_reports_with(start_time: '2018-02-21', end_time: '2018-02-27')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Valid time, invalid date - user's account is YYYY-MM-DD (system default), entered as MM-DD-YYYY
      search_admin_reports_with(start_time: '02-21-2018 12:40', end_time: '02-27-2018 13:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Valid date, invalid time - 24 hour time, entered with AM/PM
      search_admin_reports_with(start_time: '2018-02-21, 12:40 PM', end_time: '2018-02-27 01:40 PM')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Valid date, invalid time - entered with seconds
      search_admin_reports_with(start_time: '2018-02-21 12:40:31', end_time: '2018-02-27 13:40:51')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Valid date, invalid time - 24 hour time, entered as a negative number
      search_admin_reports_with(start_time: '2018-02-21 -12:40', end_time: '2018-02-27 -13:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Invalid date and time
      search_admin_reports_with(start_time: '02-21-2018 -00:40:51 AM', end_time: '02-27-2018 -01:40:51 PM')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Invalid date and time - random text
      search_admin_reports_with(start_time: '3.14159265359', end_time: 'war. war never changes.')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with an valid start time and invalid end time" do
      year = Time.now.year + 5

      # Valid start time, end time format is valid but after current system time
      search_admin_reports_with(start_time: '2018-02-21 12:40', end_time: "#{year}-02-27 13:40")
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Ending time cannot be greater than the current system time")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Valid start time, end time format is invalid but before current system time
      search_admin_reports_with(start_time: '2018-02-21 12:40', end_time: '2018-02-27 -13:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Valid start time, end time format is invalid and after current system time
      search_admin_reports_with(start_time: '2018-02-21 12:40', end_time: "#{year}-02-27 -13:40")
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid ending date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with an invalid start time and valid end time" do
      year = Time.now.year + 5

      # Valid end time, start time format is valid but after current system time
      search_admin_reports_with(start_time: "#{year}-02-21 13:40", end_time: '2018-02-27 12:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("2 errors prohibited your search from being executed")
      expect(page).to have_content("Starting time cannot be greater than the current system time")
      expect(page).to have_content("Ending time must be greater than the starting time")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Valid end time, start time format is invalid but before current system time
      search_admin_reports_with(start_time: '2018-02-21 -13:40', end_time: '2018-02-27 12:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # Valid end time, start time format is invalid and after current system time
      search_admin_reports_with(start_time: "#{year}-02-21 -13:40", end_time: '2018-02-27 12:40')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid starting date, please use the format \"YYYY-MM-DD HH:mm\"")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with per page below 10" do
      # 9 per page
      search_admin_reports_with(per_page: 9)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # 1 per page
      search_admin_reports_with(per_page: 1)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # 0 per page
      search_admin_reports_with(per_page: 0)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # -1 per page
      search_admin_reports_with(per_page: -1)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # -30 per page
      search_admin_reports_with(per_page: -30)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # -2147483649 per page (test to ensure values below -2^31 do not mess
      # things up)
      search_admin_reports_with(per_page: -2147483649)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with per page above 1000" do
      # 1001 per page
      search_admin_reports_with(per_page: 1001)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # 15000 per page
      search_admin_reports_with(per_page: 15000)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # 2147483648 per page (test to ensure values above 2^31-1 do not mess
      # things up)
      search_admin_reports_with(per_page: 2147483648)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with a decimal results per page" do
      # 2147483648.999 per page
      search_admin_reports_with(per_page: 2147483648.999)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # 1000.7 per page
      search_admin_reports_with(per_page: 1000.7)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # 9.1 per page
      search_admin_reports_with(per_page: 9.1)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # -17.33 per page
      search_admin_reports_with(per_page: -17.33)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")

      # -2147483649.95 per page
      search_admin_reports_with(per_page: -2147483649.95)
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with a non-numeric results per page" do
      search_admin_reports_with(per_page: 'aaaAAAAAAaaaaaaaa99aaaaaa')
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("1 error prohibited your search from being executed")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
    it "search all reports with multiple fields filled in incorrectly" do
      year = Time.now.year + 5
      visit "/admin/reports?utf8=#{URI.encode('✓')}&resolved=no&reason=%2F%5Bregex+is+fun%5C%29%2F&user_id=notarealuser%40domain.example&channel=ur+a+big+guy&advanced_query=%2F%7Bregex+is+fun%7D%29%5C%29%2F&sender=4u&resolver_id=2147483649.956&start_time=#{year}-02-20+12%3A40&end_time=#{year}-01-27+12%3A40&per_page=9.5"
        # Let's break this down...
          # Report status: "no"
          # Reporting reason: "/[regex is fun\)/"
          # Reporting user ID: "notarealuser@domain.example"
          # Message text: "/{regex is fun})\)/"
          # Message channel: "ur a big guy"
          # Message sender: "4u"
          # Resolving user ID: 2147483649.956
          # Date reported start: "{5 years ahead}-02-20 12:40"
          # Date reported end: "{5 years ahead}-01-27 12:40"
          # Per page: 9.5
        # This all gives us a whopping 11 errors:
      expect(page).to have_content("Failed to execute search, see errors below")
      expect(page).to have_content("11 errors prohibited your search from being executed")
      expect(page).to have_content("Invalid report status, please select an option from the dropdown menu")
      expect(page).to have_content("Invalid regex in reporting reason, please double check your query. Error details: premature end of char-class")
      expect(page).to have_content("Invalid reporting user ID, no user was found with the given ID or email address")
      expect(page).to have_content("Invalid resolving user ID, no user was found with the given ID or email address")
      expect(page).to have_content("Channel name is in an invalid format - must start with a # and contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("Invalid regex in message text, please double check your query. Error details: unmatched close parenthesis")
      expect(page).to have_content("Sender name is in an invalid format - must contain 3-25 lowercase letters, numbers, or underscores")
      expect(page).to have_content("Starting time cannot be greater than the current system time")
      expect(page).to have_content("Ending time cannot be greater than the current system time")
      expect(page).to have_content("Ending time must be greater than the starting time")
      expect(page).to have_content("Invalid results per page: must be a number from 10 to 1000")
      expect(page).to have_content("A blank search has been executed")
      expect(page).to have_content("Next ›")
    end
  end
end
