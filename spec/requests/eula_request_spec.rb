require 'rails_helper'
include AuthenticationHelpers
include EulaHelpers

RSpec.describe "Eulas", type: :request do
  before :each do
    @admin = {email: 'admin@domain.example',
              password: 'passwordpassword1'}
    sign_in_with(@admin[:email], @admin[:password])
    @user1 = {email: 'test@domain.example',
              password: 'passwordpassword2'}
  end

  describe "Admin" do
    it "can view the default TOS" do
      visit policy_tos_path
      expect(page).to have_content("You have already accepted the terms of service.")
      expect(page).to have_content("No TOS/EULA document was found in the application.")
      expect(page).to have_content("Please contact the administrators to have one added.")
    end
    it "can create a new TOS and have it automatically accepted" do
      visit admin_eulas_path
      expect(page).to have_content("Terms of Services")
      click_on "Publish New TOS"
      create_eula_with("Test EULA", "This is a test TOS", navigate_to_page: false)
      expect(page).to have_content("TOS published")
      expect(page).to have_content("To prevent you from being logged out, your account has automatically been updated to accept the TOS")
      expect(page).to have_content("Sign out")
      visit policy_tos_path
      expect(page).to have_content("You have already accepted the terms of service")
      expect(page).to have_content("User Agreement")
      expect(page).to have_content("This is a test TOS")
      expect(page).to have_content("I agree to the terms and conditions above")
    end
    it "cannot create TOSs with invalid length notes" do
      create_eula_with("", "Valid length EULA")
      expect(page).to have_content("New Terms of Service")
      expect(page).to have_content("1 error prohibited this TOS from being saved:")
      expect(page).to have_content("Note must be between 1 and 255 characters")
      create_eula_with("A" * 256, "Valid length EULA")
      expect(page).to have_content("New Terms of Service")
      expect(page).to have_content("1 error prohibited this TOS from being saved:")
      expect(page).to have_content("Note must be between 1 and 255 characters")
    end
    it "cannot create TOSs with invalid length EULA texts" do
      create_eula_with("Valid length note", "")
      expect(page).to have_content("New Terms of Service")
      expect(page).to have_content("1 error prohibited this TOS from being saved:")
      expect(page).to have_content("Agreement text must be between 1 and 100000 characters")
      create_eula_with("Valid length note", "A" * 100001)
      expect(page).to have_content("New Terms of Service")
      expect(page).to have_content("1 error prohibited this TOS from being saved:")
      expect(page).to have_content("Agreement text must be between 1 and 100000 characters")
    end
    it "cannot create TOSs with invalid length notes and EULA texts" do
      create_eula_with("", "")
      expect(page).to have_content("New Terms of Service")
      expect(page).to have_content("2 errors prohibited this TOS from being saved:")
      expect(page).to have_content("Note must be between 1 and 255 characters")
      expect(page).to have_content("Agreement text must be between 1 and 100000 characters")
      create_eula_with("A" * 256, "A" * 100001)
      expect(page).to have_content("New Terms of Service")
      expect(page).to have_content("2 errors prohibited this TOS from being saved:")
      expect(page).to have_content("Note must be between 1 and 255 characters")
      expect(page).to have_content("Agreement text must be between 1 and 100000 characters")
    end
    it "can search TOSs with non-regex note" do
      create_eula_with("helo", "A")
      create_eula_with("hello", "A")
      create_eula_with("helllo", "A")

      search_eulas_with(note: 'hel')
      expect(page).to have_content("helo")
      expect(page).to have_content("hello")
      expect(page).to have_content("helllo")

      search_eulas_with(note: 'hell')
      expect(page).to_not have_content("helo")
      expect(page).to have_content("hello")
      expect(page).to have_content("helllo")

      # Test case-insensitivity
      search_eulas_with(note: 'HEL')
      expect(page).to have_content("helo")
      expect(page).to have_content("hello")
      expect(page).to have_content("helllo")

      search_eulas_with(note: 'HELL')
      expect(page).to_not have_content("helo")
      expect(page).to have_content("hello")
      expect(page).to have_content("helllo")
    end
    it "can search TOSs with regex note" do
      create_eula_with("helo", "A")
      create_eula_with("hello", "A")
      create_eula_with("helllo", "A")

      search_eulas_with(note: '/he[l]{2}o/')
      expect(page).to_not have_content("helo")
      expect(page).to have_content("hello")
      expect(page).to_not have_content("helllo")

      search_eulas_with(note: '/he[l]{1,2}o/')
      expect(page).to have_content("helo")
      expect(page).to have_content("hello")
      expect(page).to_not have_content("helllo")

      search_eulas_with(note: '/HE[l]{2,}O/')
      expect(page).to_not have_content("helo")
      expect(page).to have_content("hello")
      expect(page).to have_content("helllo")
    end
    it "can search TOSs with non-regex EULA text" do
      create_eula_with("EULA A", "helo")
      create_eula_with("EULA B", "hello")
      create_eula_with("EULA C", "helllo")

      search_eulas_with(text: "hel")
      expect(page).to have_content("EULA A")
      expect(page).to have_content("EULA B")
      expect(page).to have_content("EULA C")

      search_eulas_with(text: "hell")
      expect(page).to_not have_content("EULA A")
      expect(page).to have_content("EULA B")
      expect(page).to have_content("EULA C")

      search_eulas_with(text: "HEL")
      expect(page).to have_content("EULA A")
      expect(page).to have_content("EULA B")
      expect(page).to have_content("EULA C")

      search_eulas_with(text: "HELL")
      expect(page).to_not have_content("EULA A")
      expect(page).to have_content("EULA B")
      expect(page).to have_content("EULA C")
    end
    it "can search TOSs with regex EULA text" do
      create_eula_with("EULA A", "helo")
      create_eula_with("EULA B", "hello")
      create_eula_with("EULA C", "helllo")

      search_eulas_with(text: '/he[l]{2}o/')
      expect(page).to_not have_content("EULA A")
      expect(page).to have_content("EULA B")
      expect(page).to_not have_content("EULA C")

      search_eulas_with(text: '/he[l]{1,2}o/')
      expect(page).to have_content("EULA A")
      expect(page).to have_content("EULA B")
      expect(page).to_not have_content("EULA C")

      search_eulas_with(text: '/HE[l]{2,}O/')
      expect(page).to_not have_content("EULA A")
      expect(page).to have_content("EULA B")
      expect(page).to have_content("EULA C")
    end
    it "can search TOSs by publisher ID" do
      admin_id = User.where(email: @admin[:email]).first.id
      # There is no guarantee that admin_id will remain 1, hence this finder function
      create_eula_with("EULA A", "helo")
      create_eula_with("EULA B", "hello")
      create_eula_with("EULA C", "helllo")

      visit admin_eulas_path
      expect(page).to have_content("Default nag-screen EULA")
      expect(page).to have_content("ID 0 (System)")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA A")
      expect(page).to have_content("EULA B")
      expect(page).to have_content("EULA C")

      search_eulas_with(publisher: 0)
      expect(page).to have_content("Default nag-screen EULA")
      expect(page).to have_content("ID 0 (System)")
      expect(page).to_not have_content("Admin (ID #{admin_id})")
      expect(page).to_not have_content("EULA A")
      expect(page).to_not have_content("EULA B")
      expect(page).to_not have_content("EULA C")

      search_eulas_with(publisher: admin_id)
      expect(page).to_not have_content("Default nag-screen EULA")
      expect(page).to_not have_content("ID 0 (System)")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA A")
      expect(page).to have_content("EULA B")
      expect(page).to have_content("EULA C")
    end
    it "can search TOSs by publisher email address" do
      admin_id = User.where(email: @admin[:email]).first.id
      create_eula_with("EULA A", "helo")
      create_eula_with("EULA B", "hello")
      create_eula_with("EULA C", "helllo")

      visit admin_eulas_path
      expect(page).to have_content("Default nag-screen EULA")
      expect(page).to have_content("ID 0 (System)")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA A")
      expect(page).to have_content("EULA B")
      expect(page).to have_content("EULA C")

      search_eulas_with(publisher: @admin[:email])
      expect(page).to_not have_content("Default nag-screen EULA")
      expect(page).to_not have_content("ID 0 (System)")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA A")
      expect(page).to have_content("EULA B")
      expect(page).to have_content("EULA C")
    end
    it "cannot search TOSs with nonexistent publisher ID" do
      admin_id = User.where(email: @admin[:email]).first.id
      create_eula_with("EULA A", "helo")
      create_eula_with("EULA B", "hello")
      create_eula_with("EULA C", "helllo")

      visit admin_eulas_path
      expect(page).to have_content("Default nag-screen EULA")
      expect(page).to have_content("ID 0 (System)")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA A")
      expect(page).to have_content("EULA B")
      expect(page).to have_content("EULA C")

      search_eulas_with(publisher: admin_id + 1)
      expect(page).to have_content("1 error prohibited your search from being executed:")
      expect(page).to have_content("Publisher not found")
      expect(page).to have_content("A blank search has been executed.")
      expect(page).to have_content("Default nag-screen EULA")
      expect(page).to have_content("ID 0 (System)")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA A")
      expect(page).to have_content("EULA B")
      expect(page).to have_content("EULA C")
    end
    it "cannot search TOSs with nonexistent publisher email address" do
      admin_id = User.where(email: @admin[:email]).first.id
      create_eula_with("EULA A", "helo")
      create_eula_with("EULA B", "hello")
      create_eula_with("EULA C", "helllo")

      visit admin_eulas_path
      expect(page).to have_content("Default nag-screen EULA")
      expect(page).to have_content("ID 0 (System)")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA A")
      expect(page).to have_content("EULA B")
      expect(page).to have_content("EULA C")

      search_eulas_with(publisher: 'a@a.co')
      expect(page).to have_content("1 error prohibited your search from being executed:")
      expect(page).to have_content("Publisher not found")
      expect(page).to have_content("A blank search has been executed.")
      expect(page).to have_content("Default nag-screen EULA")
      expect(page).to have_content("ID 0 (System)")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA A")
      expect(page).to have_content("EULA B")
      expect(page).to have_content("EULA C")
    end
    it "can search TOSs with start and end dates" do
      # This test takes time because the start/end time do not use seconds,
      # so I have to wait an entire minute -twice- to ensure I can have the
      # results I expect
      print('Long-running test: takes up to 3 minutes')
      admin_id = User.where(email: @admin[:email]).first.id
      dt_format = ApplicationHelper.user_datetime_format(user: User.find(admin_id),
                                                         seconds: false)

      # Sleep to start of next minute + 5 seconds
      sleep(((Time.now + 1.minute).beginning_of_minute - Time.now).seconds + 5)
      create_eula_with("EULA A", "helo")
      start_time = Time.now.utc # .utc required to shift into default TZ
      # Sleep to start of next minute + 10 seconds
      sleep(((Time.now + 1.minute).beginning_of_minute - Time.now).seconds + 10)
      create_eula_with("EULA B", "hello")
      end_time = Time.now.utc # .utc required to shift into default TZ
      # Sleep to start of next minute + 15 seconds
      sleep(((Time.now + 1.minute).beginning_of_minute - Time.now).seconds + 15)
      create_eula_with("EULA C", "helllo")

      visit admin_eulas_path
      expect(page).to have_content("Default nag-screen EULA")
      expect(page).to have_content("ID 0 (System)")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA A")
      expect(page).to have_content("EULA B")
      expect(page).to have_content("EULA C")

      search_eulas_with(start_time: start_time.strftime(dt_format))
      expect(page).to_not have_content("prohibited your search from being executed:")
      expect(page).to_not have_content("Default nag-screen EULA")
      expect(page).to_not have_content("ID 0 (System)")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA A")
      expect(page).to have_content("EULA B")
      expect(page).to have_content("EULA C")

      search_eulas_with(end_time: start_time.strftime(dt_format))
      expect(page).to_not have_content("prohibited your search from being executed:")
      expect(page).to have_content("Default nag-screen EULA")
      expect(page).to have_content("ID 0 (System)")
      expect(page).to_not have_content("Admin (ID #{admin_id})")
      expect(page).to_not have_content("EULA A")
      expect(page).to_not have_content("EULA B")
      expect(page).to_not have_content("EULA C")

      search_eulas_with(start_time: end_time.strftime(dt_format))
      expect(page).to_not have_content("prohibited your search from being executed:")
      expect(page).to_not have_content("Default nag-screen EULA")
      expect(page).to_not have_content("ID 0 (System)")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to_not have_content("EULA A")
      expect(page).to have_content("EULA B")
      expect(page).to have_content("EULA C")

      search_eulas_with(end_time: end_time.strftime(dt_format))
      expect(page).to_not have_content("prohibited your search from being executed:")
      expect(page).to have_content("Default nag-screen EULA")
      expect(page).to have_content("ID 0 (System)")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA A")
      expect(page).to_not have_content("EULA B")
      expect(page).to_not have_content("EULA C")

      search_eulas_with(start_time: start_time.strftime(dt_format),
                        end_time: end_time.strftime(dt_format))
      expect(page).to_not have_content("prohibited your search from being executed:")
      expect(page).to_not have_content("Default nag-screen EULA")
      expect(page).to_not have_content("ID 0 (System)")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA A")
      expect(page).to_not have_content("EULA B")
      expect(page).to_not have_content("EULA C")
    end
    it "cannot search TOSs with invalid start and end dates" do
      admin_id = User.where(email: @admin[:email]).first.id
      dt_format = ApplicationHelper.user_datetime_format(user: User.find(admin_id),
                                                         seconds: false)
      create_eula_with("EULA A", "helo")
      start_time = Time.now.utc # .utc required to shift into default TZ
      create_eula_with("EULA B", "hello")
      end_time = Time.now.utc + 1.minute # .utc required to shift into default TZ
      create_eula_with("EULA C", "helllo")

      visit admin_eulas_path
      expect(page).to_not have_content("prohibited your search from being executed:")
      expect(page).to have_content("Default nag-screen EULA")
      expect(page).to have_content("ID 0 (System)")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA A")
      expect(page).to have_content("EULA B")
      expect(page).to have_content("EULA C")

      # Conflicting start/end dates
      search_eulas_with(start_time: end_time.strftime(dt_format),
                        end_time: start_time.strftime(dt_format))
      expect(page).to have_content("1 error prohibited your search from being executed:")
      expect(page).to have_content("Date published end must come before date published start")
      expect(page).to have_content("A blank search has been executed.")
      expect(page).to have_content("Default nag-screen EULA")
      expect(page).to have_content("ID 0 (System)")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA A")
      expect(page).to have_content("EULA B")
      expect(page).to have_content("EULA C")

      # Invalid format start/end dates
      search_eulas_with(start_time: '2020-12-28 17 31')
      expect(page).to have_content("1 error prohibited your search from being executed:")
      expect(page).to have_content("Date published start is in an invalid format")
      expect(page).to have_content("A blank search has been executed.")
      expect(page).to have_content("Default nag-screen EULA")
      expect(page).to have_content("ID 0 (System)")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA A")
      expect(page).to have_content("EULA B")
      expect(page).to have_content("EULA C")

      search_eulas_with(end_time: '2020-12-28 17:63')
      expect(page).to have_content("1 error prohibited your search from being executed:")
      expect(page).to have_content("Date published end is in an invalid format")
      expect(page).to have_content("A blank search has been executed.")
      expect(page).to have_content("Default nag-screen EULA")
      expect(page).to have_content("ID 0 (System)")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA A")
      expect(page).to have_content("EULA B")
      expect(page).to have_content("EULA C")

      search_eulas_with(start_time: '2020-12-28 17 31',
                        end_time: '2020-12-28 17:63')
      expect(page).to have_content("2 errors prohibited your search from being executed:")
      expect(page).to have_content("Date published start is in an invalid format")
      expect(page).to have_content("Date published end is in an invalid format")
      expect(page).to have_content("A blank search has been executed.")
      expect(page).to have_content("Default nag-screen EULA")
      expect(page).to have_content("ID 0 (System)")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA A")
      expect(page).to have_content("EULA B")
      expect(page).to have_content("EULA C")
    end
    it "can search TOSs with per page set" do
      admin_id = User.where(email: @admin[:email]).first.id

      (0..30).each do |n|
        create_eula_with("EULA #{n}", "A" * (n + 1))
      end

      visit admin_eulas_path
      expect(page).to_not have_content("prohibited your search from being executed:")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA 30")
      expect(page).to have_content("EULA 25")
      expect(page).to have_content("EULA 21")
      expect(page).to_not have_content("EULA 20")
      expect(page).to_not have_content("EULA 15")
      expect(page).to_not have_content("EULA 10")
      expect(page).to_not have_content("EULA 9")
      expect(page).to_not have_content("EULA 5")
      expect(page).to_not have_content("EULA 0")
      expect(page).to_not have_content("Default nag-screen EULA")
      expect(page).to_not have_content("ID 0 (System)")
      expect(page).to have_link("Next ›", href: "/admin/tos?page=2")
      expect(page).to have_link("Last »", href: "/admin/tos?page=4")

      search_eulas_with(per_page: 15)
      expect(page).to_not have_content("prohibited your search from being executed:")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA 30")
      expect(page).to have_content("EULA 25")
      expect(page).to have_content("EULA 21")
      expect(page).to have_content("EULA 20")
      expect(page).to_not have_content("EULA 15")
      expect(page).to_not have_content("EULA 10")
      expect(page).to_not have_content("EULA 9")
      expect(page).to_not have_content("EULA 5")
      expect(page).to_not have_content("EULA 0")
      expect(page).to_not have_content("Default nag-screen EULA")
      expect(page).to_not have_content("ID 0 (System)")
      expect(page).to have_link("Next ›", href: "/admin/tos?end_time=&eula_text=&note=&page=2&per_page=15&publisher=&start_time=")
      expect(page).to have_link("Last »", href: "/admin/tos?end_time=&eula_text=&note=&page=3&per_page=15&publisher=&start_time=")
      click_link "Next ›"
      expect(page).to_not have_content("prohibited your search from being executed:")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to_not have_content("EULA 30")
      expect(page).to_not have_content("EULA 25")
      expect(page).to_not have_content("EULA 21")
      expect(page).to_not have_content("EULA 20")
      expect(page).to have_content("EULA 15")
      expect(page).to have_content("EULA 10")
      expect(page).to have_content("EULA 9")
      expect(page).to have_content("EULA 5")
      expect(page).to_not have_content("EULA 0")
      expect(page).to_not have_content("Default nag-screen EULA")
      expect(page).to_not have_content("ID 0 (System)")
      expect(page).to have_link("« First", href: "/admin/tos?end_time=&eula_text=&note=&per_page=15&publisher=&start_time=")
      expect(page).to have_link("‹ Prev", href: "/admin/tos?end_time=&eula_text=&note=&per_page=15&publisher=&start_time=")
      expect(page).to have_link("Next ›", href: "/admin/tos?end_time=&eula_text=&note=&page=3&per_page=15&publisher=&start_time=")
      expect(page).to have_link("Last »", href: "/admin/tos?end_time=&eula_text=&note=&page=3&per_page=15&publisher=&start_time=")

      search_eulas_with(text: "/[A]{10,}/", per_page: 15)
      expect(page).to_not have_content("prohibited your search from being executed:")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA 30")
      expect(page).to have_content("EULA 25")
      expect(page).to have_content("EULA 21")
      expect(page).to have_content("EULA 20")
      expect(page).to_not have_content("EULA 15")
      expect(page).to_not have_content("EULA 10")
      expect(page).to_not have_content("EULA 9")
      expect(page).to_not have_content("EULA 5")
      expect(page).to_not have_content("EULA 0")
      expect(page).to_not have_content("Default nag-screen EULA")
      expect(page).to_not have_content("ID 0 (System)")
      expect(page).to have_link("Next ›", href: "/admin/tos?end_time=&eula_text=%2F%5BA%5D%7B10%2C%7D%2F&note=&page=2&per_page=15&publisher=&start_time=")
      expect(page).to have_link("Last »", href: "/admin/tos?end_time=&eula_text=%2F%5BA%5D%7B10%2C%7D%2F&note=&page=2&per_page=15&publisher=&start_time=")
    end
    it "cannot search TOSs with invalid per page" do
      admin_id = User.where(email: @admin[:email]).first.id

      (0..30).each do |n|
        create_eula_with("EULA #{n}", "A" * (n + 1))
      end

      visit admin_eulas_path
      expect(page).to_not have_content("prohibited your search from being executed:")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA 30")
      expect(page).to have_content("EULA 25")
      expect(page).to have_content("EULA 21")
      expect(page).to_not have_content("EULA 20")
      expect(page).to_not have_content("EULA 15")
      expect(page).to_not have_content("EULA 10")
      expect(page).to_not have_content("EULA 9")
      expect(page).to_not have_content("EULA 5")
      expect(page).to_not have_content("EULA 0")
      expect(page).to_not have_content("Default nag-screen EULA")
      expect(page).to_not have_content("ID 0 (System)")
      expect(page).to have_link("Next ›", href: "/admin/tos?page=2")
      expect(page).to have_link("Last »", href: "/admin/tos?page=4")

      search_eulas_with(per_page: 9)
      expect(page).to have_content("1 error prohibited your search from being executed:")
      expect(page).to have_content("Search results per page is invalid (must be a whole number between 10 and 1000)")
      expect(page).to have_content("A blank search has been executed.")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA 30")
      expect(page).to have_content("EULA 25")
      expect(page).to have_content("EULA 21")
      expect(page).to_not have_content("EULA 20")
      expect(page).to_not have_content("EULA 15")
      expect(page).to_not have_content("EULA 10")
      expect(page).to_not have_content("EULA 9")
      expect(page).to_not have_content("EULA 5")
      expect(page).to_not have_content("EULA 0")
      expect(page).to_not have_content("Default nag-screen EULA")
      expect(page).to_not have_content("ID 0 (System)")
      expect(page).to have_link("Next ›", href: "/admin/tos?end_time=&eula_text=&note=&page=2&per_page=9&publisher=&start_time=")
      expect(page).to have_link("Last »", href: "/admin/tos?end_time=&eula_text=&note=&page=4&per_page=9&publisher=&start_time=")

      search_eulas_with(per_page: 10001)
      expect(page).to have_content("1 error prohibited your search from being executed:")
      expect(page).to have_content("Search results per page is invalid (must be a whole number between 10 and 1000)")
      expect(page).to have_content("A blank search has been executed.")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA 30")
      expect(page).to have_content("EULA 25")
      expect(page).to have_content("EULA 21")
      expect(page).to_not have_content("EULA 20")
      expect(page).to_not have_content("EULA 15")
      expect(page).to_not have_content("EULA 10")
      expect(page).to_not have_content("EULA 9")
      expect(page).to_not have_content("EULA 5")
      expect(page).to_not have_content("EULA 0")
      expect(page).to_not have_content("Default nag-screen EULA")
      expect(page).to_not have_content("ID 0 (System)")
      expect(page).to have_link("Next ›", href: "/admin/tos?end_time=&eula_text=&note=&page=2&per_page=10001&publisher=&start_time=")
      expect(page).to have_link("Last »", href: "/admin/tos?end_time=&eula_text=&note=&page=4&per_page=10001&publisher=&start_time=")
    end
    it "can sort TOSs" do
      admin_id = User.where(email: @admin[:email]).first.id

      (0..30).each do |n|
        create_eula_with("EULA #{n}", "A" * (n + 1))
      end

      visit admin_eulas_path
      expect(page).to_not have_content("prohibited your search from being executed:")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA 30")
      expect(page).to have_content("EULA 25")
      expect(page).to have_content("EULA 21")
      expect(page).to_not have_content("EULA 20")
      expect(page).to_not have_content("EULA 15")
      expect(page).to_not have_content("EULA 10")
      expect(page).to_not have_content("EULA 9")
      expect(page).to_not have_content("EULA 5")
      expect(page).to_not have_content("EULA 0")
      expect(page).to_not have_content("Default nag-screen EULA")
      expect(page).to_not have_content("ID 0 (System)")
      expect(page).to have_link("Next ›", href: "/admin/tos?page=2")
      expect(page).to have_link("Last »", href: "/admin/tos?page=4")

      click_on "Date Published"
      expect(page).to_not have_content("prohibited your search from being executed:")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to_not have_content("EULA 30")
      expect(page).to_not have_content("EULA 25")
      expect(page).to_not have_content("EULA 21")
      expect(page).to_not have_content("EULA 20")
      expect(page).to_not have_content("EULA 15")
      expect(page).to_not have_content("EULA 10")
      expect(page).to_not have_content("EULA 9")
      expect(page).to have_content("EULA 5")
      expect(page).to have_content("EULA 0")
      expect(page).to have_content("Default nag-screen EULA")
      expect(page).to have_content("ID 0 (System)")
      expect(page).to have_link("Next ›", href: "/admin/tos?column=created_at&direction=asc&page=2")
      expect(page).to have_link("Last »", href: "/admin/tos?column=created_at&direction=asc&page=4")
    end
    it "can trigger regex errors for note and EULA text fields", use_transactional_fixtures: false do
      # use_transactional_fixtures: false because this triggers a PG exception,
      # which will poison the connection until transaction rollback.
      # You can read more about this here:
      # https://stackoverflow.com/a/22383251
      # My specific DatabaseCleaner method was copied from here:
      # https://stackoverflow.com/a/10655518
      admin_id = User.where(email: @admin[:email]).first.id

      (0..30).each do |n|
        create_eula_with("EULA #{n}", "A" * (n + 1))
      end

      visit admin_eulas_path
      expect(page).to_not have_content("prohibited your search from being executed:")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA 30")
      expect(page).to have_content("EULA 25")
      expect(page).to have_content("EULA 21")
      expect(page).to_not have_content("EULA 20")
      expect(page).to_not have_content("EULA 15")
      expect(page).to_not have_content("EULA 10")
      expect(page).to_not have_content("EULA 9")
      expect(page).to_not have_content("EULA 5")
      expect(page).to_not have_content("EULA 0")
      expect(page).to_not have_content("Default nag-screen EULA")
      expect(page).to_not have_content("ID 0 (System)")
      expect(page).to have_link("Next ›", href: "/admin/tos?page=2")
      expect(page).to have_link("Last »", href: "/admin/tos?page=4")

      search_eulas_with(note: '/\(closed parenthesis for regex testing)/')
      expect(page).to have_content("1 error prohibited your search from being executed:")
      expect(page).to have_content("Note has invalid regular expressions")
      expect(page).to have_content("A blank search has been executed.")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA 30")
      expect(page).to have_content("EULA 25")
      expect(page).to have_content("EULA 21")
      expect(page).to_not have_content("EULA 20")
      expect(page).to_not have_content("EULA 15")
      expect(page).to_not have_content("EULA 10")
      expect(page).to_not have_content("EULA 9")
      expect(page).to_not have_content("EULA 5")
      expect(page).to_not have_content("EULA 0")
      expect(page).to_not have_content("Default nag-screen EULA")
      expect(page).to_not have_content("ID 0 (System)")
      expect(page).to have_link("Next ›", href: "/admin/tos?end_time=&eula_text=&note=%2F%5C%28closed+parenthesis+for+regex+testing%29%2F&page=2&per_page=&publisher=&start_time=")
      expect(page).to have_link("Last »", href: "/admin/tos?end_time=&eula_text=&note=%2F%5C%28closed+parenthesis+for+regex+testing%29%2F&page=4&per_page=&publisher=&start_time=")

      search_eulas_with(text: '/\(closed parenthesis for regex testing)/')
      expect(page).to have_content("1 error prohibited your search from being executed:")
      expect(page).to have_content("Agreement Text has invalid regular expressions")
      expect(page).to have_content("A blank search has been executed.")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA 30")
      expect(page).to have_content("EULA 25")
      expect(page).to have_content("EULA 21")
      expect(page).to_not have_content("EULA 20")
      expect(page).to_not have_content("EULA 15")
      expect(page).to_not have_content("EULA 10")
      expect(page).to_not have_content("EULA 9")
      expect(page).to_not have_content("EULA 5")
      expect(page).to_not have_content("EULA 0")
      expect(page).to_not have_content("Default nag-screen EULA")
      expect(page).to_not have_content("ID 0 (System)")
      expect(page).to have_link("Next ›", href: "/admin/tos?end_time=&eula_text=%2F%5C%28closed+parenthesis+for+regex+testing%29%2F&note=&page=2&per_page=&publisher=&start_time=")
      expect(page).to have_link("Last »", href: "/admin/tos?end_time=&eula_text=%2F%5C%28closed+parenthesis+for+regex+testing%29%2F&note=&page=4&per_page=&publisher=&start_time=")

      search_eulas_with(note: "/\\(closed parenthesis for regex testing)/",
                        text: "/\\(closed parenthesis for regex testing)/")
      expect(page).to have_content("2 errors prohibited your search from being executed:")
      expect(page).to have_content("Note has invalid regular expressions")
      expect(page).to have_content("Agreement Text has invalid regular expressions")
      expect(page).to have_content("A blank search has been executed.")
      expect(page).to have_content("Admin (ID #{admin_id})")
      expect(page).to have_content("EULA 30")
      expect(page).to have_content("EULA 25")
      expect(page).to have_content("EULA 21")
      expect(page).to_not have_content("EULA 20")
      expect(page).to_not have_content("EULA 15")
      expect(page).to_not have_content("EULA 10")
      expect(page).to_not have_content("EULA 9")
      expect(page).to_not have_content("EULA 5")
      expect(page).to_not have_content("EULA 0")
      expect(page).to_not have_content("Default nag-screen EULA")
      expect(page).to_not have_content("ID 0 (System)")
      expect(page).to have_link("Next ›", href: "/admin/tos?end_time=&eula_text=%2F%5C%28closed+parenthesis+for+regex+testing%29%2F&note=%2F%5C%28closed+parenthesis+for+regex+testing%29%2F&page=2&per_page=&publisher=&start_time=")
      expect(page).to have_link("Last »", href: "/admin/tos?end_time=&eula_text=%2F%5C%28closed+parenthesis+for+regex+testing%29%2F&note=%2F%5C%28closed+parenthesis+for+regex+testing%29%2F&page=4&per_page=&publisher=&start_time=")
    end
  end

  describe "User" do
    it "can accept default TOS on first login" do
      switch_to_user(@user1[:email], @user1[:password], accept_tos: false)
      expect(page).to have_content("You must accept the Terms of Service before continuing.")
      expect(page).to have_content("Terms of Service")
      click_on "Continue"
      expect(page).to have_content("Thank you for accepting the terms of service!")
    end
    it "is prompted to accept new TOS when one is created" do
      admin_id = User.where(email: @admin[:email]).first.id
      next_eula_note = "Test TOS"
      next_eula_text = "Don't do anything stupid"
      switch_to_user(@user1[:email], @user1[:password])

      Eula.new(user_id: admin_id, note: next_eula_note,
               eula_text: next_eula_text, publisher: admin_id).save
      visit chat_logs_path
      expect(page).to have_content("You have been signed out. To use the site, sign in and accept the terms of service.")

      sign_in_with(@user1[:email], @user1[:password], accept_tos: false)
      expect(page).to have_content("You must accept the Terms of Service before continuing.")
      expect(page).to have_content("Terms of Service")
      click_on "Continue"
      expect(page).to have_content("Thank you for accepting the terms of service!")
    end
    it "can accept TOS while not banned" do
      admin_id = User.where(email: @admin[:email]).first.id
      next_eula_note = "Test TOS"
      next_eula_text = "Don't do anything stupid"
      switch_to_user(@user1[:email], @user1[:password])

      Eula.new(user_id: admin_id, note: next_eula_note,
               eula_text: next_eula_text, publisher: admin_id).save
      visit chat_logs_path
      expect(page).to have_content("You have been signed out. To use the site, sign in and accept the terms of service.")

      sign_in_with(@user1[:email], @user1[:password], accept_tos: false)
      expect(page).to have_content("You must accept the Terms of Service before continuing.")
      expect(page).to have_content("Terms of Service")
      expect(page).to have_content(next_eula_text)
      expect(page).to have_content("I agree to the terms and conditions above")
      expect(page).to have_content("I do not agree to the terms and conditions above, but I wish to keep my account")
      expect(page).to have_content("I do not agree to the terms and conditions above, delete my account")
      click_on "Continue"
      expect(page).to have_content("Thank you for accepting the terms of service!")
    end
    it "can accept TOS while banned" do
      admin_id = User.where(email: @admin[:email]).first.id
      user1_id = User.where(email: @user1[:email]).first.id
      next_eula_note = "Test TOS"
      next_eula_text = "Don't do anything stupid"
      next_ban_int_rsn = "Test ban"
      next_ban_ext_rsn = "You know what you did"
      switch_to_user(@user1[:email], @user1[:password])

      Eula.new(user_id: admin_id, note: next_eula_note,
               eula_text: next_eula_text, publisher: admin_id).save
      Ban.new(user_id: user1_id, banning_user: admin_id, permanent_ban: true,
              ban_reversal: false, internal_reason: next_ban_int_rsn,
              external_reason: next_ban_ext_rsn, banned_user: user1_id).save
      visit chat_logs_path
      expect(page).to have_content("You have been signed out. To use the site, sign in and accept the terms of service.")

      sign_in_with(@user1[:email], @user1[:password], accept_tos: false)
      expect(page).to have_content("You must accept the Terms of Service before continuing.")
      expect(page).to have_content("Terms of Service")
      expect(page).to have_content(next_eula_text)
      expect(page).to have_content("I agree to the terms and conditions above")
      expect(page).to have_content("I do not agree to the terms and conditions above")
      expect(page).to_not have_content("I do not agree to the terms and conditions above, but I wish to keep my account")
      expect(page).to_not have_content("I do not agree to the terms and conditions above, delete my account")
      click_on "Continue"
      expect(page).to_not have_content("Thank you for accepting the terms of service!")
      expect(page).to have_content("You have been permanently banned. Reason: #{next_ban_ext_rsn}")
      expect(page).to have_content("You may appeal this decision by contacting the administrators.")
    end
    it "can decline TOS and later accept it while not banned" do
      admin_id = User.where(email: @admin[:email]).first.id
      next_eula_note = "Test TOS"
      next_eula_text = "Don't do anything stupid"
      switch_to_user(@user1[:email], @user1[:password])

      Eula.new(user_id: admin_id, note: next_eula_note,
               eula_text: next_eula_text, publisher: admin_id).save
      visit chat_logs_path
      expect(page).to have_content("You have been signed out. To use the site, sign in and accept the terms of service.")

      sign_in_with(@user1[:email], @user1[:password], accept_tos: false)
      expect(page).to have_content("You must accept the Terms of Service before continuing.")
      expect(page).to have_content("Terms of Service")
      expect(page).to have_content(next_eula_text)
      expect(page).to have_content("I agree to the terms and conditions above")
      expect(page).to have_content("I do not agree to the terms and conditions above, but I wish to keep my account")
      expect(page).to have_content("I do not agree to the terms and conditions above, delete my account")
      within('.radio-buttons-tos') do
        choose "I do not agree to the terms and conditions above, but I wish to keep my account"
      end
      click_on "Continue"
      expect(page).to have_content("You have been signed out. To use the site, sign in and accept the terms of service.")
      expect(page).to have_content("Sign in") # Check if signed out
      expect(page).to have_content("Register")
    end
    it "can decline TOS and later accept it while banned" do
      admin_id = User.where(email: @admin[:email]).first.id
      user1_id = User.where(email: @user1[:email]).first.id
      next_eula_note = "Test TOS"
      next_eula_text = "Don't do anything stupid"
      next_ban_int_rsn = "Test ban"
      next_ban_ext_rsn = "You know what you did"
      switch_to_user(@user1[:email], @user1[:password])

      Eula.new(user_id: admin_id, note: next_eula_note,
               eula_text: next_eula_text, publisher: admin_id).save
      Ban.new(user_id: user1_id, banning_user: admin_id, permanent_ban: true,
              ban_reversal: false, internal_reason: next_ban_int_rsn,
              external_reason: next_ban_ext_rsn, banned_user: user1_id).save
      visit chat_logs_path
      expect(page).to have_content("You have been signed out. To use the site, sign in and accept the terms of service.")

      sign_in_with(@user1[:email], @user1[:password], accept_tos: false)
      expect(page).to have_content("You must accept the Terms of Service before continuing.")
      expect(page).to have_content("Terms of Service")
      expect(page).to have_content(next_eula_text)
      expect(page).to have_content("I agree to the terms and conditions above")
      expect(page).to have_content("I do not agree to the terms and conditions above")
      expect(page).to_not have_content("I do not agree to the terms and conditions above, but I wish to keep my account")
      expect(page).to_not have_content("I do not agree to the terms and conditions above, delete my account")
      within('.radio-buttons-tos') do
        choose "I do not agree to the terms and conditions above"
      end
      click_on "Continue"
      expect(page).to have_content("You have been signed out. To use the site, sign in and accept the terms of service.")
      expect(page).to have_content("Sign in") # Check if signed out
      expect(page).to have_content("Register")
    end
    it "can decline TOS and delete account while not banned" do
      admin_id = User.where(email: @admin[:email]).first.id
      next_eula_note = "Test TOS"
      next_eula_text = "Don't do anything stupid"
      switch_to_user(@user1[:email], @user1[:password])

      Eula.new(user_id: admin_id, note: next_eula_note,
               eula_text: next_eula_text, publisher: admin_id).save
      visit chat_logs_path
      expect(page).to have_content("You have been signed out. To use the site, sign in and accept the terms of service.")

      sign_in_with(@user1[:email], @user1[:password], accept_tos: false)
      expect(page).to have_content("You must accept the Terms of Service before continuing.")
      expect(page).to have_content("Terms of Service")
      expect(page).to have_content(next_eula_text)
      expect(page).to have_content("I agree to the terms and conditions above")
      expect(page).to have_content("I do not agree to the terms and conditions above, but I wish to keep my account")
      expect(page).to have_content("I do not agree to the terms and conditions above, delete my account")
      within('.radio-buttons-tos') do
        choose "I do not agree to the terms and conditions above, delete my account"
      end
      click_on "Continue"
      expect(page).to have_content("Account deleted. Should you want to use the site again, you will have to create a new account.")
      expect(page).to have_content("Sign in") # Check if signed out
      expect(page).to have_content("Register")
    end
    it "cannot decline TOS and delete account while banned" do
      admin_id = User.where(email: @admin[:email]).first.id
      user1_id = User.where(email: @user1[:email]).first.id
      next_eula_note = "Test TOS"
      next_eula_text = "Don't do anything stupid"
      next_ban_int_rsn = "Test ban"
      next_ban_ext_rsn = "You know what you did"
      switch_to_user(@user1[:email], @user1[:password])

      Eula.new(user_id: admin_id, note: next_eula_note,
               eula_text: next_eula_text, publisher: admin_id).save
      Ban.new(user_id: user1_id, banning_user: admin_id, permanent_ban: true,
              ban_reversal: false, internal_reason: next_ban_int_rsn,
              external_reason: next_ban_ext_rsn, banned_user: user1_id).save
      visit chat_logs_path
      expect(page).to have_content("You have been signed out. To use the site, sign in and accept the terms of service.")

      sign_in_with(@user1[:email], @user1[:password], accept_tos: false)
      expect(page).to have_content("You must accept the Terms of Service before continuing.")
      expect(page).to have_content("Terms of Service")
      expect(page).to have_content(next_eula_text)
      expect(page).to have_content("I agree to the terms and conditions above")
      expect(page).to have_content("I do not agree to the terms and conditions above")
      expect(page).to_not have_content("I do not agree to the terms and conditions above, but I wish to keep my account")
      expect(page).to_not have_content("I do not agree to the terms and conditions above, delete my account")
    end
  end
end
