require 'date'

FactoryBot.define do
  factory :user do
    rand_num = Random.rand(10000)
    email_addr = "factory-user#{rand_num}@factory.test"
    while User.where(email: email_addr).exists?
      # Ensure no conflicts can occur
      rand_num = Random.rand(10000)
      email_addr = "factory-user#{rand_num}@factory.test"
    end
    name { "Factory User #{rand_num}" }
    email { email_addr }
    password { "CorrectHorseBatteryStaple" }
      # Only the most secure passwords are allowed here Kappa
    password_confirmation { "CorrectHorseBatteryStaple" }
    
    user_preference # Generate associated Preferences record
    
    trait :tos_accepted do
      eula_accepted_at { DateTime.now() }
    end
    
    trait :admin do
      role { "admin" }
    end
  end
end