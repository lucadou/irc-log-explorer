FactoryBot.define do
  factory :search do
    user_id { 1 }
    type { "" }
    execution_time { "9.99" }
    basic_query { "MyText" }
    advanced_query { "MyText" }
    channel { "MyString" }
    sender { "MyString" }
    case_sensitive { false }
    start_id { "" }
    end_id { "" }
    start_time { "" }
    end_time { "" }
    base_id { "" }
  end
end
