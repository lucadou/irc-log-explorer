module Account2FAHelpers
  def enable_2fa(code: nil, navigate_to_page: true, expecting_before: nil, expecting_after: nil, submit_form: true)
    if navigate_to_page
      visit account_security_2fa_totp_path
    end

    expect(page).to have_content("Enable TOTP")
    expect(page).to have_content("TOTP seed")

    if expecting_before
      expecting_before.each do |item|
        expect(page).to have_content(item)
      end
    end

    totp = nil
    unless code
      # I have to manually determine the TOTP code since user.current_otp
      # is unavailable in tests
      code = page.find('input#totp-seed')[:placeholder]
      totp = ROTP::TOTP.new(code)
      code = totp.now
    end

    fill_in "user_otp_attempt", with: code

    if submit_form
      click_on "Enable TOTP"
    end

    if expecting_after
      expecting_after.each do |item|
        expect(page).to have_content(item)
      end
    end

    return totp
  end

  def disable_2fa(code: nil, navigate_to_page: true, expecting_before: nil, expecting_after: nil, submit_form: true)
    if navigate_to_page
      visit account_security_2fa_totp_path
    end

    expect(page).to have_content("Disable TOTP")
    expect(page).to have_content("TOTP or Recovery Code")

    if expecting_before
      expecting_before.each do |item|
        expect(page).to have_content(item)
      end
    end

    if code.is_a? ROTP::TOTP
      code = code.now
    else
      code = code.to_s
    end

    fill_in "user_otp_attempt", with: code

    if submit_form
      click_on "Disable TOTP"
    end

    if expecting_after
      expecting_after.each do |item|
        expect(page).to have_content(item)
      end
    end
  end

  def get_recovery_codes(navigate_to_page: true, expecting_before: nil, expecting_after: nil)
    if navigate_to_page
      visit account_security_2fa_recovery_codes_path
    end

    expect(page).to have_content("Account Recovery Codes")
    expect(page).to have_content("Your account recovery codes:")
    expect(page).to have_content("Generate new Codes")

    if expecting_before
      expecting_before.each do |item|
        expect(page).to have_content(item)
      end
    end

    recovery_codes = []
    (0..4).each do |n|
      recovery_codes << page.find("li#recovery-code-#{n}").text
    end

    if expecting_after
      expecting_after.each do |item|
        expect(page).to have_content(item)
      end
    end

    return recovery_codes
  end
end
