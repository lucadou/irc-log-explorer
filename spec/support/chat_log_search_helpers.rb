module ChatLogSearchHelpers
  def basic_search_with(query: '', navigate_to_page: true)
    if navigate_to_page
      visit chat_logs_path
    end
    
    fill_in "query", with: query
    
    click_button "Search"
  end
  
  def advanced_search_with(channel: '', sender: '', message: '', case_sensitive: '', start_time: '', end_time: '', start_id: '', end_id: '', per_page: '', navigate_to_page: true)
    if navigate_to_page
      visit advanced_search_chat_logs_path
    end
    
    fill_in "chat_log_channel", with: channel
    fill_in "chat_log_sender", with: sender
    fill_in "chat_log_message", with: message
    if case_sensitive.to_s == 'true'
      # If I just did "if case_sensitive", not specifying case_sensitive when
      # the method is called would cause it to be true (since '' is truthy),
      # and calling this method with case_sensitive="no" would also be
      # evaluated to truthy.
      page.check 'case_sensitive_checkbox'
    end
    fill_in "chat_log_start_time", with: start_time
    fill_in "chat_log_end_time", with: end_time
    fill_in "start_id", with: start_id
    fill_in "end_id", with: end_id
    fill_in "per_page", with: per_page
    
    within(".actions") do
      click_button "Search"
    end
  end
end