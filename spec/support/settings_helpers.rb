module SettingsHelpers
  def set_basic_settings_with(name, email, backup_email: '', new_password: '', new_password_confirmation: '', current_password: '', navigate_to_page: true)
    if navigate_to_page
      visit account_settings_path
    end
    expect(page).to have_content("Change Account Information")
    fill_in "user_name", with: name
    fill_in "user_email", with: email
    fill_in "user_backup_email", with: backup_email
    fill_in "user_password", with: new_password
    fill_in "user_password_confirmation", with: new_password_confirmation
    fill_in "user_current_password", with: current_password if page.has_field?(:user_current_password)
    within(".actions") do
      click_button "Update Information"
    end
  end

  def set_ui_settings_with(sort_col: 'Time', sort_order: 'Descending', results_per_page: 10, theme: 'Light (Default)', date_format: 'YYYY-MM-DD', use_24hr_time: 'Yes', time_zone: '+00:00', navigate_to_page: true, submit_form: true)
    if navigate_to_page
      visit account_ui_path
    end

    expect(page).to have_content("Appearance")
    expect(page).to have_content("Localization")
    select sort_col, from: 'user_preference_default_sort_col'
    within('.radio-buttons-sort-order') do
      choose sort_order
    end
    fill_in "user_preference_search_results", with: results_per_page
    select theme, from: 'user_preference_theme'
    select date_format, from: 'user_preference_date_format'
    within('.radio-buttons-24hr-time') do
      choose use_24hr_time
    end
    fill_in "user_preference_tz_offset", with: time_zone

    if submit_form
      click_button "Save Settings"
    end
  end

  def set_notification_settings_with(pw_change_pri: true, pw_change_bkp: nil, pw_reset_pri: nil, pw_reset_bkp: nil, oauth_pri: true, oauth_bkp: nil, sec_2fa_pri: nil, sec_2fa_bkp: nil, admin_pri: nil, admin_bkp: nil, navigate_to_page: true, submit_form: true)
    if navigate_to_page
      visit account_notifications_path
    end

    if pw_change_pri
      page.check 'user_preference_notify_pwchange_pri'
    elsif !pw_change_pri.nil?
      page.uncheck 'user_preference_notify_pwchange_pri'
    end

    if pw_change_bkp
      page.check 'user_preference_notify_pwchange_bkp'
    elsif !pw_change_bkp.nil?
      page.uncheck 'user_preference_notify_pwchange_bkp'
    end

    if pw_reset_pri
      page.check 'user_preference_notify_pwreset_pri'
    elsif !pw_reset_pri.nil?
      page.uncheck 'user_preference_notify_pwreset_pri'
    end

    if pw_reset_bkp
      page.check 'user_preference_notify_pwreset_bkp'
    elsif !pw_reset_bkp.nil?
      page.uncheck 'user_preference_notify_pwreset_bkp'
    end

    if oauth_pri
      page.check 'user_preference_notify_sso_pri'
    elsif !oauth_pri.nil?
      page.uncheck 'user_preference_notify_sso_pri'
    end

    if oauth_bkp
      page.check 'user_preference_notify_sso_bkp'
    elsif !oauth_bkp.nil?
      page.uncheck 'user_preference_notify_sso_bkp'
    end

    if sec_2fa_pri
      page.check 'user_preference_notify_2fa_pri'
    elsif !sec_2fa_pri.nil?
      page.uncheck 'user_preference_notify_2fa_pri'
    end

    if sec_2fa_bkp
      page.check 'user_preference_notify_2fa_bkp'
    elsif !sec_2fa_bkp.nil?
      page.uncheck 'user_preference_notify_2fa_bkp'
    end

    if admin_pri
      page.check 'user_preference_notify_admin_pri'
    elsif !admin_pri.nil?
      page.uncheck 'user_preference_notify_admin_pri'
    end

    if admin_bkp
      page.check 'user_preference_notify_admin_bkp'
    elsif !admin_bkp.nil?
      page.uncheck 'user_preference_notify_admin_bkp'
    end

    if submit_form
      click_button "Save Settings"
    end
  end
end
