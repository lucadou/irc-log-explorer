module AuthenticationHelpers
  def sign_up_with(name, email, password, password_confirmation, navigate_to_page: true)
    if navigate_to_page
      visit new_user_registration_path
    end
    expect(page).to have_content("Create a traditional account")
    fill_in "user_name", with: name
    fill_in "user_email", with: email
    fill_in "user_password", with: password
    fill_in "user_password_confirmation", with: password_confirmation
    within(".actions") do
      click_button "Register"
    end
  end

  def sign_in_with(email, password, totp: nil, navigate_to_page: true, accept_tos: true, expecting_success: true, expecting_before: nil, expecting_after: nil)
    if navigate_to_page
      visit new_user_session_path
    end

    if expecting_before
      expecting_before.each do |item|
        expect(page).to have_content(item)
      end
    end

    expect(page).to have_content("Use a traditional account")
    fill_in "user_email", with: email
    fill_in "user_password", with: password
    within(".actions") do
      click_button "Sign in"
    end
    
    if totp # Supply TOTP code
      if page.has_content?("Two-Factor Authentication") && page.has_content?("Authenticate via One-Time Password (TOTP) or Recovery Code")
        if totp.is_a? ROTP::TOTP
          fill_in "user_otp_attempt", with: totp.now
        else
          fill_in "user_otp_attempt", with: totp.to_s
        end
        click_on "Submit"

        if expecting_success
          expect(page).to have_content("Signed in with One-Time Password. If you used a recovery code, make sure to generate new codes so you don't run out!")
        else
          expect(page).to have_content("Invalid 2FA or recovery code, please try again")
        end
      end
    end

    if accept_tos # Accept the TOS if it is not already accepted
      if page.has_content?("You must accept the Terms of Service before continuing.")
        # First time logging in to the user
        expect(page).to have_content("Terms of Service")
        click_on "Continue"
        expect(page).to have_content("Thank you for accepting the terms of service!")
      elsif totp && expecting_success
        expect(page).to have_content("Signed in with One-Time Password. If you used a recovery code, make sure to generate new codes so you don't run out!")
      elsif expecting_success
        expect(page).to have_content("Signed in successfully")
      end
    end

    if expecting_after
      expecting_after.each do |item|
        expect(page).to have_content(item)
      end
    end
  end

  def switch_to_user(email, password, totp: nil, accept_tos: true, expecting_success: true, expecting_before: nil, expecting_after: nil)
    if expecting_before
      expecting_before.each do |item|
        expect(page).to have_content(item)
      end
    end

    sign_out_with
    sign_in_with(email, password, totp: totp, accept_tos: accept_tos, expecting_success: expecting_success)

    if expecting_after
      expecting_after.each do |item|
        expect(page).to have_content(item)
      end
    end
  end

  def sign_out_with(expecting_before: nil, expecting_after: nil)
    if expecting_before
      expecting_before.each do |item|
        expect(page).to have_content(item)
      end
    end

    expect(page).to have_content("Sign out")
    click_on("navbar-signout-link")
    expect(current_path).to eq(root_path)
    expect(page).to have_content("Signed out successfully")

    if expecting_after
      expecting_after.each do |item|
        expect(page).to have_content(item)
      end
    end
  end
end
